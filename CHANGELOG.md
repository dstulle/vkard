# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added
 - vCard (v. 4.0) with VERSION and FN-Property
 - Add NOTE-Property
 - CRLF linebreak to export
 - unfolding of line during parsing
 - case-insensitive parsing of property names
 - property values are escaped on parsing and exporting
 - folding of lines in export
 - property groups to parsing and exporting
 - N-Property
 - Cardinality of Properties
 - ADR-Property
 - TITLE-Property
 - ROLE-Property
 - ORG-Property
 - SOURCE-Property
 - NICKNAME-Property
 - PHOTO-Property
 - TEL-Property
 - EMAIL-Property
 - IMPP-Property
 - CATEGORIES-Property
 - vCard-DSL
 - RELATED-Property

[Unreleased]: https://gitlab.com/dstulle/vkard/-/commits/master/
