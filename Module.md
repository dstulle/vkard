# Module vKard

This library provides parsing and exporting of vCard as described in [RFC 6350](https://datatracker.ietf.org/doc/html/rfc6350) for the JVM and JavaScript written entirely in Kotlin.

## Goals

The aim of this project is to completely implement everything specified in the [RFC 6350](https://datatracker.ietf.org/doc/html/rfc6350) standard.
The parsing of vCards is trying to be as flexible and forgiving as possible.

## Getting Started

This Library can be used to handle vCards.
vCards can be created through the constructor of [VCard][de.dstulle.vkard.VCard], created through the [vCard-DSL][de.dstulle.vkard.dsl.vCard] or [parsed][de.dstulle.vkard.parse] from a given vCard String.
the vCard can be manipulated and exported as String.

To learn more about how to programmatically create and modify a vCard read the [VCard][de.dstulle.vkard.VCard] documentation.

To use the vCard-DSL to create a vCard read the [vCard-DSL][de.dstulle.vkard.dsl.vCard] documentation.

Further information about the parsing of Strings to vCard Objects can be found at the [parse][de.dstulle.vkard.parse] documentation.

The export of a vCard is described in the [export][de.dstulle.vkard.export] documentation.

# Package de.dstulle.vkard

Base package containing the [VCard] itself as well as the [parse] function to parse vCard Strings into vCard Objects and the [export] function to export a vCard Object as a vCard String.

# Package de.dstulle.vkard.properties

Package containing all the properties a vCard can have.

# Package de.dstulle.vkard.dsl

Package containing the [vCard]-DSL to create a [VCard] in a clean and easy way.

# Package de.dstulle.vkard.properties.addressing

Package containing all the delivery addressing properties of a vCard as described in [Section 6.3. of RFC 6350](https://datatracker.ietf.org/doc/html/rfc6350#section-6.3)

# Package de.dstulle.vkard.properties.communications

Package containing all the communications properties of a vCard as described in [Section 6.4. of RFC 6350](https://datatracker.ietf.org/doc/html/rfc6350#section-6.4)

# Package de.dstulle.vkard.properties.explanatory

Package containing all the explanatory properties of a vCard as described in [Section 6.7. of RFC 6350](https://datatracker.ietf.org/doc/html/rfc6350#section-6.7)

# Package de.dstulle.vkard.properties.general

Package containing all the general properties of a vCard as described in [Section 6.1. of RFC 6350](https://datatracker.ietf.org/doc/html/rfc6350#section-6.1)

# Package de.dstulle.vkard.properties.geographical

Package containing all the geographical properties of a vCard as described in [Section 6.5. of RFC 6350](https://datatracker.ietf.org/doc/html/rfc6350#section-6.5)

# Package de.dstulle.vkard.properties.identification

Package containing all the identification properties of a vCard as described in [Section 6.2. of RFC 6350](https://datatracker.ietf.org/doc/html/rfc6350#section-6.2)

# Package de.dstulle.vkard.properties.organizational

Package containing all the organizational properties of a vCard as described in [Section 6.6. of RFC 6350](https://datatracker.ietf.org/doc/html/rfc6350#section-6.6)
