# vKard

![vKard Logo](https://gitlab.com/dstulle/vkard/-/raw/master/src/commonMain/resources/vKard-logo-128.png)

This library provides parsing and exporting of vCard as described in [RFC 6350](https://datatracker.ietf.org/doc/html/rfc6350) for the JVM and JavaScript written in Kotlin.

The aim of this project is to completely implement everything specified in the [RFC 6350](https://datatracker.ietf.org/doc/html/rfc6350) standard. The parsing of vCards is trying to be as flexible and forgiving as possible.

## Getting Started

This Library can be used to handle vCards.
vCards can be created directly or parsed from a given vCard String.
the vCard can be manipulated and exported as String.

The documentation can be found here: https://dstulle.gitlab.io/vkard/

## Dependencies

This library is written in plain Kotlin with no special dependencies to other libraries.

## Versioning

We use [Semantic Versioning](https://semver.org/spec/v2.0.0.html) for versioning. For the versions available, see the [tags](https://gitlab.com/dstulle/vkard/-/tags) on this repository.

## Authors

* Daniel Sturm - initial work

## License

This project is licensed under the LGPL v3 License - see the [LICENSE](LICENSE) file for details.
