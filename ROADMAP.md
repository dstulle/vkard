# Roadmap

## Version 1.0
### Parsing and exporting vCard v. 4.0 - [RFC 6350](https://datatracker.ietf.org/doc/html/rfc6350)
![51%](https://geps.dev/progress/51)
<!-- 31 of 61 -->
#### Property Value Data Types
![25%](https://geps.dev/progress/25)
<!-- 3 of 12 -->
  - [x] [TEXT](https://datatracker.ietf.org/doc/html/rfc6350#section-4.1)
  - [x] [URI](https://datatracker.ietf.org/doc/html/rfc6350#section-4.2)
  - [ ] [DATE](https://datatracker.ietf.org/doc/html/rfc6350#section-4.3.1)
  - [ ] [TIME](https://datatracker.ietf.org/doc/html/rfc6350#section-4.3.2)
  - [ ] [DATE-TIME](https://datatracker.ietf.org/doc/html/rfc6350#section-4.3.3)
  - [ ] [DATE-AND-OR-TIME](https://datatracker.ietf.org/doc/html/rfc6350#section-4.3.4)
  - [ ] [TIMESTAMP](https://datatracker.ietf.org/doc/html/rfc6350#section-4.3.5)
  - [x] [BOOLEAN](https://datatracker.ietf.org/doc/html/rfc6350#section-4.4)
  - [ ] [INTEGER](https://datatracker.ietf.org/doc/html/rfc6350#section-4.5)
  - [ ] [FLOAT](https://datatracker.ietf.org/doc/html/rfc6350#section-4.6)
  - [ ] [UTC-OFFSET](https://datatracker.ietf.org/doc/html/rfc6350#section-4.7)
  - [ ] [LANGUAGE-TAG](https://datatracker.ietf.org/doc/html/rfc6350#section-4.8)

#### Property Parameters
![54%](https://geps.dev/progress/54)
<!-- 6 of 11 -->
  - [x] [LANGUAGE](https://datatracker.ietf.org/doc/html/rfc6350#section-5.1)
  - [x] [VALUE](https://datatracker.ietf.org/doc/html/rfc6350#section-5.2)
  - [x] [PREF](https://datatracker.ietf.org/doc/html/rfc6350#section-5.3)
  - [x] [ALTID](https://datatracker.ietf.org/doc/html/rfc6350#section-5.4)
  - [x] [PID](https://datatracker.ietf.org/doc/html/rfc6350#section-5.5)
  - [x] [TYPE](https://datatracker.ietf.org/doc/html/rfc6350#section-5.6)
  - [ ] [MEDIATYPE](https://datatracker.ietf.org/doc/html/rfc6350#section-5.7)
  - [ ] [CALSCALE](https://datatracker.ietf.org/doc/html/rfc6350#section-5.8)
  - [ ] [SORT-AS](https://datatracker.ietf.org/doc/html/rfc6350#section-5.9)
  - [ ] [GEO](https://datatracker.ietf.org/doc/html/rfc6350#section-5.10)
  - [ ] [TZ](https://datatracker.ietf.org/doc/html/rfc6350#section-5.11)

#### Properties
![58%](https://geps.dev/progress/58)
<!-- 22 of 38 -->
  - [x] [BEGIN](https://datatracker.ietf.org/doc/html/rfc6350#section-6.1.1)
  - [x] [END](https://datatracker.ietf.org/doc/html/rfc6350#section-6.1.2)
  - [x] [SOURCE](https://datatracker.ietf.org/doc/html/rfc6350#section-6.1.3)
  - [ ] [KIND](https://datatracker.ietf.org/doc/html/rfc6350#section-6.1.4)
  - [ ] [XML](https://datatracker.ietf.org/doc/html/rfc6350#section-6.1.5)
  - [x] [FN](https://datatracker.ietf.org/doc/html/rfc6350#section-6.2.1)
  - [x] [N](https://datatracker.ietf.org/doc/html/rfc6350#section-6.2.2)
  - [x] [NICKNAME](https://datatracker.ietf.org/doc/html/rfc6350#section-6.2.3)
  - [x] [PHOTO](https://datatracker.ietf.org/doc/html/rfc6350#section-6.2.4)
  - [ ] [BDAY](https://datatracker.ietf.org/doc/html/rfc6350#section-6.2.5)
  - [ ] [ANIVERSARY](https://datatracker.ietf.org/doc/html/rfc6350#section-6.2.6)
  - [ ] [GENDER](https://datatracker.ietf.org/doc/html/rfc6350#section-6.2.7)
  - [x] [ADR](https://datatracker.ietf.org/doc/html/rfc6350#section-6.3.1)
  - [x] [TEL](https://datatracker.ietf.org/doc/html/rfc6350#section-6.4.1)
  - [x] [EMAIL](https://datatracker.ietf.org/doc/html/rfc6350#section-6.4.2)
  - [x] [IMPP](https://datatracker.ietf.org/doc/html/rfc6350#section-6.4.3)
  - [ ] [LANG](https://datatracker.ietf.org/doc/html/rfc6350#section-6.4.4)
  - [ ] [TZ](https://datatracker.ietf.org/doc/html/rfc6350#section-6.5.1)
  - [x] [GEO](https://datatracker.ietf.org/doc/html/rfc6350#section-6.5.2)
  - [x] [TITLE](https://datatracker.ietf.org/doc/html/rfc6350#section-6.6.1)
  - [x] [ROLE](https://datatracker.ietf.org/doc/html/rfc6350#section-6.6.2)
  - [x] [LOGO](https://datatracker.ietf.org/doc/html/rfc6350#section-6.6.3)
  - [x] [ORG](https://datatracker.ietf.org/doc/html/rfc6350#section-6.6.4)
  - [ ] [MEMBER](https://datatracker.ietf.org/doc/html/rfc6350#section-6.6.5)
  - [x] [RELATED](https://datatracker.ietf.org/doc/html/rfc6350#section-6.6.6)
  - [x] [CATEGORIES](https://datatracker.ietf.org/doc/html/rfc6350#section-6.7.1)
  - [x] [NOTE](https://datatracker.ietf.org/doc/html/rfc6350#section-6.7.2)
  - [ ] [PRODID](https://datatracker.ietf.org/doc/html/rfc6350#section-6.7.3)
  - [ ] [REV](https://datatracker.ietf.org/doc/html/rfc6350#section-6.7.4)
  - [x] [SOUND](https://datatracker.ietf.org/doc/html/rfc6350#section-6.7.5)
  - [ ] [UID](https://datatracker.ietf.org/doc/html/rfc6350#section-6.7.6)
  - [ ] [CLIENTPIDMAP](https://datatracker.ietf.org/doc/html/rfc6350#section-6.7.7)
  - [x] [URL](https://datatracker.ietf.org/doc/html/rfc6350#section-6.7.8)
  - [x] [VERSION](https://datatracker.ietf.org/doc/html/rfc6350#section-6.7.9)
  - [ ] [KEY](https://datatracker.ietf.org/doc/html/rfc6350#section-6.8.1)
  - [ ] [FBURL](https://datatracker.ietf.org/doc/html/rfc6350#section-6.9.1)
  - [ ] [CALARDURI](https://datatracker.ietf.org/doc/html/rfc6350#section-6.9.2)
  - [ ] [CALURI](https://datatracker.ietf.org/doc/html/rfc6350#section-6.9.3)
  - [ ] [Extended Properties and Parameters](https://datatracker.ietf.org/doc/html/rfc6350#section-6.10)

## Version 1.1

- Parsing and exporting extensions: Place of Birth, Place and Date of Death - [RFC 6474](https://datatracker.ietf.org/doc/html/rfc6474)

## Version 1.2

- Parsing and exporting extensions: Representing vCard Extensions Defined by the Open Mobile Alliance (OMA) Converged Address Book (CAB) Group - [RFC 6715](https://datatracker.ietf.org/doc/html/rfc6715)

## Version 1.3

- Parsing and exporting extensions for Instant Messaging (IM) - [RFC 4770](https://datatracker.ietf.org/doc/html/rfc4770)

## Version 1.4

- Parsing vCard v. 3.0 - [RFC 2425](https://datatracker.ietf.org/doc/html/rfc2425), [RFC 2426](https://datatracker.ietf.org/doc/html/rfc2426)


## Maybe sometime in the future

* xCard - [RFC 6351](https://datatracker.ietf.org/doc/html/rfc6351)

* jCard - [RFC 7095](https://datatracker.ietf.org/doc/html/rfc7095)

* Generation of [QR-Code](https://en.wikipedia.org/wiki/QR_code) based on the vCard

* Exporting to vCard v. 3.0 - [RFC 2425](https://datatracker.ietf.org/doc/html/rfc2425), [RFC 2426](https://datatracker.ietf.org/doc/html/rfc2426)

* Parsing vCard v. 2.1 - [vCard](http://jpim.sourceforge.net/contacts/specifications/vcard-21.pdf)

* Export to vCard v. 2.1 - [vCard](http://jpim.sourceforge.net/contacts/specifications/vcard-21.pdf)
