
plugins {
    kotlin("multiplatform") version "2.0.0"
    id("java-library")
    jacoco
    id("org.sonarqube") version "5.0.0.4638"
    java
    id("maven-publish")
}

group = "de.dstulle.vkard"
version = "0.1-SNAPSHOT"

repositories {
    mavenCentral()
}

kotlin {
    jvm {
        testRuns["test"].executionTask.configure {
            useJUnitPlatform()
        }
    }
    js {
        browser {
            testTask {
                useKarma {
                    useChromeHeadless()
                }
            }
        }
        binaries.executable()
    }

    sourceSets {
        val commonMain by getting
        val commonTest by getting {
            dependencies {
                implementation(kotlin("test-common"))
                implementation(kotlin("test-annotations-common"))
            }
        }
        val jvmMain by getting
        val jvmTest by getting {
            dependencies {
                implementation(kotlin("test-junit5"))
                implementation("org.junit.jupiter:junit-jupiter-api:5.10.2")
                runtimeOnly("org.junit.jupiter:junit-jupiter-engine:5.10.2")
            }
        }
        val jsMain by getting
        val jsTest by getting {
            dependencies {
                implementation(kotlin("test-js"))
            }
        }
    }
}

tasks.test {
    enabled = true
    dependsOn(getTasksByName("commonTest", false))
    dependsOn(getTasksByName("jvmTest", false))
    dependsOn(getTasksByName("jsTest", false))
}

jacoco {
    toolVersion = "0.8.11"
}

tasks.jacocoTestReport {
    dependsOn(tasks.test)

    val coverageSourceDirs = arrayOf(
        "src/commonMain",
        "src/jvmMain"
    )

    val classFiles = layout.buildDirectory.get().dir("classes/kotlin/jvm/").asFile
        .walkBottomUp()
        .toSet()

    classDirectories.setFrom(classFiles)
    sourceDirectories.setFrom(files(coverageSourceDirs))

    executionData.setFrom(layout.buildDirectory.get().file("jacoco/jvmTest.exec"))

    reports {
        xml.required.set(true)
        html.required.set(true)
    }
}

sonarqube {
    properties {
        property("sonar.projectKey", "dstulle_vkard")
        property("sonar.organization", "dstulle")

        property("sonar.projectName", "vKard")

        property("sonar.sources", "./src/commonMain/kotlin")
        property("sonar.tests", "./src/commonTest/kotlin")
        property("sonar.java.binaries", "./build/classes/kotlin/jvm/main")
        property("sonar.java.test.binaries", "./build/classes/kotlin/jvm/test")

        property("sonar.java.coveragePlugin", "jacoco")
        property("sonar.junit.reportsPath", "./build/jacoco/jvmTest.exec")
        property("sonar.coverage.jacoco.xmlReportPaths", "./build/reports/jacoco/test/jacocoTestReport.xml")
    }
}
