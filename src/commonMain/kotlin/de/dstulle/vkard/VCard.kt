/*
 * GNU LESSER GENERAL PUBLIC LICENSE
 * Copyright (C) 2021 - 2024 Daniel Sturm
 * mail@DanielSturm.de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.dstulle.vkard

import de.dstulle.vkard.properties.identification.Fn
import de.dstulle.vkard.properties.identification.N
import de.dstulle.vkard.property.Property
import de.dstulle.vkard.property.value.TextListValue
import kotlin.reflect.KClass

/**
 * Object that represents a vCard
 *
 * Each vCard is required to have at least one
 * [FN][de.dstulle.vkard.properties.identification.Fn] property so the
 * value for the first FN property can be directly passed through this
 * constructor.
 *
 * ```kotlin
 * val arthur = VCard("Arthur Dent")
 * ```
 *
 * If no value is passed then no FN property is added to the vCard. In
 * this case the FN property is added during the [export] to be
 * a valid vCard.
 *
 * ```kotlin
 * val emptyCard = VCard()
 * ```
 *
 * The FN property could also be added later.
 *
 * ```kotlin
 * val arthur = VCard()
 *     .property(Fn("Arthur Dent"))
 * ```
 *
 * To simplify the creation of vCard objects with [properties][de.dstulle.vkard.property.Property] the
 * [property][VCard.property] method can also be chained:
 *
 * ```kotlin
 * val arthur = VCard()
 *     .property(Fn("Arthur Dent"))
 *     .property(N("Dent", "Arthur", "Philip")
 *     .property(Categories("human", "male"))
 * ```
 *
 * @constructor If given a value, automatically creates an FN property.
 * @param formattedName the value for the first FN property
 */
class VCard(formattedName: String? = null) {

    private val properties = mutableListOf<Property<*>>()

    init {
        if(formattedName != null) {
            properties.add(Fn(formattedName))
        }
    }


    /**
     * Convenience method to get have a quick and easy method to get a
     * human-readable name of this card. If the Property "N" is present a Name
     * is build based on the values. The Order of the given and family names
     * can be chosen by the parameter ```easternNameOrder``. If this parameter
     * is set to true, the family names are put before the given names.
     *
     * @param easternNameOrder to specify the ordering of given and family
     *    names.
     */
    fun getName(easternNameOrder: Boolean = false): String {
        if(has(N::class)){
            val nValue = get(N::class).first().value
            val nComponents = mutableListOf<TextListValue>()
            nComponents.add(nValue.get()[3])
            if (easternNameOrder) {
                nComponents.add(nValue.get()[0])
                nComponents.add(nValue.get()[1])
                nComponents.add(nValue.get()[2])
            } else {
                nComponents.add(nValue.get()[1])
                nComponents.add(nValue.get()[2])
                nComponents.add(nValue.get()[0])
            }
            nComponents.add(nValue.get()[4])
            return nComponents.filter { it.isNotEmpty() }.joinToString(" ") { it.get().joinToString(" ") }
        }
        if(has(Fn::class)){
            return get(Fn::class).first().value.get()
        }
        return ""
    }

    /**
     * Returns a formatted string of this vCard according
     * to [RFC 6350](https://datatracker.ietf.org/doc/html/rfc6350)
     * with all mandatory values. If no
     * [FN][de.dstulle.vkard.properties.identification.Fn] property
     * is present an empty FN be added to the string because it is a
     * mandatory field.
     *
     * @return [RFC 6350](https://datatracker.ietf.org/doc/html/rfc6350) conform
     *     representation of this vCard
     */
    fun toVCardString(): String {
        return "BEGIN:VCARD\r\nVERSION:4.0\r\n" +
                if(properties.any { it is Fn}) {
                    ""
                } else {
                    "FN:\r\n"
                } +
                if(properties.isNotEmpty()) {
                    properties.joinToString("\r\n") { it.toVCardString() } + "\r\n"
                } else {
                    ""
                } +
                "END:VCARD"
    }

    /**
     * Indicates whether some other object is "equal to" this vCard.
     *
     * All properties have to have an equal property in the other vCard
     * and all properties of the other vCard have to have and equal
     * property in this vCard.
     *
     * ```kotlin
     * val vCard1 = VCard().property(Note("A")).property(Note("B"))
     * val vCard2 = VCard().property(Note("B")).property(Note("A"))
     *
     * vCard1 == vCard1
     * vCard1.toVCardString() != vCard1.toVCardString()
     * ```
     *
     * The order in which the properties appear in the vCard is not
     * taken into account.
     */
    override fun equals(other: Any?): Boolean {
        return when {
            this === other -> true
            other is VCard -> (
                        toVCardString() == other.toVCardString() || (
                            properties.all { other.has(it) } &&
                            other.properties.all { this.has(it) }
                        )
                    )
            else -> false
        }
    }

    /**
     * Returns the hash code value for the vCard.
     */
    override fun hashCode(): Int {
        return if(properties.isEmpty()) {
            VCard("").hashCode()
        } else {
            properties.sumOf { it.hashCode() }
        }
    }

    /**
     * Adds the given property to the vCard.
     *
     * A [Property] has a [cardinality][de.dstulle.vkard.property.Cardinality] which
     * indicates the number of properties allowed per vCard. The
     * majority of the Properties may be present one or more times.
     *
     * The Property [N][de.dstulle.vkard.properties.identification.N]
     * may be present only one time and the Property [FN][Fn]
     * must be present, but is allowed more than once.
     *
     * If the Property already exists the given property is merged into
     * the already existing property of the vCard.
     *
     * the [method][VCard.property] can also be chained
     *
     * ```kotlin
     * val zaphod = VCard("Zaphod Beeblebrox").property(N("Beeblebrox", "Zaphod"))
     * zaphod.set(N(honorificPrefix = "President of the Galaxy"))
     * zaphod.set(N(honorificPrefix = "President of the Galaxy (formerly)"))
     * ```
     *
     * @param property the property to be added or merged if the vCard
     *     already has such a property
     */
    fun property(property: Property<*>): VCard {
        val ownProperty = properties.find { it.name == property.name }
        if (property.cardinality.exactlyOne &&
            ownProperty != null
        ) {
            if(ownProperty.altId != null && ownProperty.altId == property.altId) {
                properties.add(property)
            } else {
                ownProperty.mergeWith(property)
            }
        } else {
            properties.add(property)
        }
        return this
    }

    /**
     * Retuns a list of properties of the given type/class in
     * this vCard.
     *
     * @param propertyClass the class of the property you are looking
     *     for.
     * @return a list of all properties matching.
     */
    @Suppress("UNCHECKED_CAST")
    fun <P: Property<*>> get(propertyClass: KClass<P>): List<P> {
        return properties.filter { it::class == propertyClass } as List<P>
    }

    /**
     * Checks weather there is a property of the given type/class in
     * this vCard.
     *
     * @param propertyClass the class of the property you are looking
     *     for.
     * @return true if a fitting property exists, otherwise false.
     */
    fun <P: Property<*>> has(propertyClass: KClass<P>): Boolean {
        return properties.any { it::class == propertyClass }
    }

    /**
     * checks if the given property with the same value is present at
     * this vCard.
     *
     * @param property the property you are looking for.
     * @return true if a property equal to the given is present
     *     otherwise false.
     */
    private fun has(property: Property<*>): Boolean {
        return properties.any { it == property }
    }

}