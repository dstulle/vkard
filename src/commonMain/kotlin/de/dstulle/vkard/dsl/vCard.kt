/*
 * GNU LESSER GENERAL PUBLIC LICENSE
 * Copyright (C) 2022 - 2024 Daniel Sturm
 * mail@DanielSturm.de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.dstulle.vkard.dsl

import de.dstulle.vkard.VCard
import de.dstulle.vkard.export
import de.dstulle.vkard.properties.addressing.Adr
import de.dstulle.vkard.properties.communications.Email
import de.dstulle.vkard.properties.communications.Impp
import de.dstulle.vkard.properties.communications.Tel
import de.dstulle.vkard.properties.explanatory.Categories
import de.dstulle.vkard.properties.explanatory.Note
import de.dstulle.vkard.properties.explanatory.Sound
import de.dstulle.vkard.properties.explanatory.Url
import de.dstulle.vkard.properties.general.Source
import de.dstulle.vkard.properties.geographical.Geo
import de.dstulle.vkard.properties.identification.Fn
import de.dstulle.vkard.properties.identification.N
import de.dstulle.vkard.properties.identification.Nickname
import de.dstulle.vkard.properties.identification.Photo
import de.dstulle.vkard.properties.organizational.*
import de.dstulle.vkard.property.Property
import de.dstulle.vkard.property.parameter.*
import de.dstulle.vkard.property.value.UriValue

/**
 * property that returns an empty vCard.
 *
 * Each vCard is required to have at least one
 * [FN][de.dstulle.vkard.properties.identification.Fn] property
 * so FN property can be added afterwards or is added during the
 * [export] to be a valid vCard.
 *
 * ```kotlin
 * val emptyCard = vCard
 * ```
 *
 * @author Daniel Sturm <mail@danielsturm.de>
 */
val vCard: VCard
    get() {
        return VCard()
    }

/**
 * Function that creates a vCard.
 *
 * Each vCard is required to have at least one
 * [FN][de.dstulle.vkard.properties.identification.Fn] property so the
 * value for the first FN property can be directly passed through this
 * constructor.
 *
 * ```kotlin
 * val arthur = vCard("Arthur Dent")
 * ```
 *
 * If no value is passed then no FN property is added to the vCard. In
 * this case the FN property is added during the [export] to be
 * a valid vCard.
 *
 * ```kotlin
 * val emptyCard = vCard()
 * ```
 *
 * The FN property could also be added during creation.
 *
 * ```kotlin
 * val arthur = vCard {
 *     fn("Arthur Dent")
 * }
 * ```
 *
 * To create vCard objects with [properties][de.dstulle.vkard.property.Property] the
 * DSL syntax can be used:
 *
 * ```kotlin
 * val arthur = vCard {
 *     fn("Arthur Dent")
 *     n("Dent", "Arthur", "Philip")
 *     categories("human", "male")
 * }
 * ```
 *
 * @param formattedName the value for the first FN property
 * @param init function that is called to initialize the vCard
 * @author Daniel Sturm &lt;mail@danielsturm.de&gt;
 */
fun vCard(formattedName: String? = null, init: VCARD.() -> Unit = {}): VCard {
    val vCard = VCARD(formattedName)
    vCard.init()
    return vCard.vCard
}

class VCARD(formattedName: String? = null) {

    val vCard = VCard(formattedName)

    private fun <P : PROPERTY<*>> property(property: P, init: P.() -> Unit = {}): P {
        property.init()
        vCard.property(property.property)
        return property
    }

    fun source(value: String, init: PROPERTY<Source>.() -> Unit = {}) = property(PROPERTY(Source(value)), init)

    fun fn(value: String, init: PROPERTY<Fn>.() -> Unit = {}) = property(PROPERTY(Fn(value)), init)

    fun n(value: String, init: PROPERTY<N>.() -> Unit = {}) = property(PROPERTY(N(value)), init)

    fun n(
        familyName: String? = null,
        givenName: String? = null,
        additionalName: String? = null,
        honorificPrefix: String? = null,
        honorificSuffix: String? = null,
        init: PROPERTY<N>.() -> Unit = {}
    ) = property(PROPERTY(
        N(
                familyName,
                givenName,
                additionalName,
                honorificPrefix,
                honorificSuffix
            )
    ), init)

    fun nickname(vararg value: String, init: PROPERTY<Nickname>.() -> Unit = {}) = property(PROPERTY(Nickname(*value)), init)

    fun photo(value: String, init: PROPERTY<Photo>.() -> Unit = {}) = property(PROPERTY(Photo(value)), init)

    fun adr(
        postOfficeBox: String? = null,
        extendedAddress: String? = null,
        streetAddress: String? = null,
        locality: String? = null,
        region: String? = null,
        postalCode: String? = null,
        countryName: String? = null,
        init: PROPERTY<Adr>.() -> Unit = {}
    ) = property(PROPERTY(
        Adr(
                postOfficeBox,
                extendedAddress,
                streetAddress,
                locality,
                region,
                postalCode,
                countryName
            )
    ), init)

    fun tel(value: String, init: PROPERTY<Tel>.() -> Unit = {}) = property(PROPERTY(Tel(value)), init)

    fun email(value: String, init: PROPERTY<Email>.() -> Unit = {}) = property(PROPERTY(Email(value)), init)

    fun impp(value: String, init: PROPERTY<Impp>.() -> Unit = {}) = property(PROPERTY(Impp(value)), init)

    fun geo(value: String, init: PROPERTY<Geo>.() -> Unit = {}) = property(PROPERTY(Geo(value)), init)

    fun geo(
        scheme: String,
        userinfo: String = "",
        host: String = "",
        port: String = "",
        path: String,
        query: String = "",
        fragment: String = "",
        init: PROPERTY<Geo>.() -> Unit = {}
    ) = property(PROPERTY(
        Geo(UriValue(
            scheme,
            userinfo,
            host,
            port,
            path,
            query,
            fragment
        ))
    ), init)

    fun title(value: String, init: PROPERTY<Title>.() -> Unit = {}) = property(PROPERTY(Title(value)), init)

    fun role(value: String, init: PROPERTY<Role>.() -> Unit = {}) = property(PROPERTY(Role(value)), init)

    fun logo(value: String, init: PROPERTY<Logo>.() -> Unit = {}) = property(PROPERTY(Logo(value)), init)

    fun org(value: String, init: PROPERTY<Org>.() -> Unit = {}) = property(PROPERTY(Org(value)), init)

    fun related(value: String, init: PROPERTY<Related>.() -> Unit = {}) = property(PROPERTY(Related(value)), init)

    fun related(
        scheme: String,
        userinfo: String = "",
        host: String = "",
        port: String = "",
        path: String,
        query: String = "",
        fragment: String = "",
        init: PROPERTY<Related>.() -> Unit = {}
    ) = property(PROPERTY(
        Related(UriValue(
            scheme,
            userinfo,
            host,
            port,
            path,
            query,
            fragment
        ))
    ), init)

    fun categories(vararg value: String, init: PROPERTY<Categories>.() -> Unit = {}) = property(PROPERTY(Categories(*value)), init)

    fun note(value: String, init: PROPERTY<Note>.() -> Unit = {}) = property(PROPERTY(Note(value)), init)

    fun sound(value: String, init: PROPERTY<Sound>.() -> Unit = {}) = property(PROPERTY(Sound(value)), init)

    fun url(value: String, init: PROPERTY<Url>.() -> Unit = {}) = property(PROPERTY(Url(value)), init)

}

class PROPERTY<P : Property<*>>(val property: P) {

    private fun <P : Parameter<*>> parameter(parameter: P, init: P.() -> Unit = {}): P {
        parameter.init()
        property.parameter(parameter)
        return parameter
    }

    fun language(languageTag: String, init: Language.() -> Unit = {}) = parameter(Language(languageTag), init)

    fun value(valueType: String, init: Value.() -> Unit = {}) = parameter(Value(valueType), init)

    fun value(valueType: Value.Type, init: Value.() -> Unit = {}) = parameter(Value(valueType), init)

    fun pref(value: Int, init: Pref.() -> Unit = {}) = parameter(Pref(value), init)

    fun altid(id: String, init: AltId.() -> Unit = {}) = parameter(AltId(id), init)

    fun pid(id: Int, init: PId.() -> Unit = {}) = parameter(PId(id), init)

    fun pid(firstId: Int, secondId: Int?, init: PId.() -> Unit = {}) = parameter(PId(firstId, secondId), init)

    fun pid(vararg ids : Pair<Int, Int?>, init: PId.() -> Unit = {}) = parameter(PId(*ids), init)

    fun type(value: String) = parameter(Type(value))

    fun type(vararg values: String) = parameter(Type(*values))

}
