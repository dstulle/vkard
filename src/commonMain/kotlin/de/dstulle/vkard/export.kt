/*
 * GNU LESSER GENERAL PUBLIC LICENSE
 * Copyright (C) 2022 - 2024 Daniel Sturm
 * mail@DanielSturm.de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.dstulle.vkard

/**
 * Exports the given [VCard] as
 * [RFC 6350](https://datatracker.ietf.org/doc/html/rfc6350) conform
 * string.
 *
 * Lines in the export are wrapped when exceeding the given width
 * limit without taking into account the linebreak characters.
 *
 * @param vCard the vCard to be exported.
 * @param lineWidthLimit the limit of line with after which the line
 *     is folded
 * @return the string that represents the given vCard.
 */
fun export(vCard: VCard, lineWidthLimit: Int = 75): String {
    require(lineWidthLimit >= 2){"lineWidthLimit must be 2 or greater but was $lineWidthLimit"}
    return wrapLongLines(vCard.toVCardString(), lineWidthLimit)

}

private fun wrapLongLines(vCardString: String, lineFoldOffset: Int): String {
    var result = ""

    for (line in vCardString.split("\n")) {
        var nextLine = line
        var lineToAdd = ""
        while (nextLine.encodeToByteArray().size > lineFoldOffset) {
            while (lineToAdd.encodeToByteArray().size + nextLine.substring(0, 0)
                    .encodeToByteArray().size < lineFoldOffset
            ) {
                lineToAdd += nextLine[0]
                nextLine = nextLine.substring(1)
            }
            result += lineToAdd + "\r\n"
            lineToAdd = " "
        }
        if (nextLine != "\r") {
            result += lineToAdd + nextLine + "\n"
        }
    }

    return result.substring(0, result.length - 1)
}
