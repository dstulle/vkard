/*
 * GNU LESSER GENERAL PUBLIC LICENSE
 * Copyright (C) 2022 - 2024 Daniel Sturm
 * mail@DanielSturm.de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.dstulle.vkard

import de.dstulle.vkard.properties.addressing.Adr
import de.dstulle.vkard.properties.communications.Email
import de.dstulle.vkard.properties.communications.Impp
import de.dstulle.vkard.properties.communications.Tel
import de.dstulle.vkard.properties.explanatory.Categories
import de.dstulle.vkard.properties.explanatory.Note
import de.dstulle.vkard.properties.explanatory.Sound
import de.dstulle.vkard.properties.explanatory.Url
import de.dstulle.vkard.properties.general.Source
import de.dstulle.vkard.properties.geographical.Geo
import de.dstulle.vkard.properties.identification.Fn
import de.dstulle.vkard.properties.identification.N
import de.dstulle.vkard.properties.identification.Nickname
import de.dstulle.vkard.properties.identification.Photo
import de.dstulle.vkard.properties.organizational.*
import de.dstulle.vkard.property.Property
import de.dstulle.vkard.property.parameter.*
import de.dstulle.vkard.property.value.StructuredTextValue
import de.dstulle.vkard.property.value.TextListValue
import de.dstulle.vkard.property.value.TextValue
import de.dstulle.vkard.property.value.UriValue

/**
 * Parses the given string into a [VCard] object.
 *
 * Though according to
 * [RFC 6350](https://datatracker.ietf.org/doc/html/rfc6350) there are
 * some requirements and restrictions on how a vCard String should be
 * structured, this parsing method tries to be as forgiving as possible
 * with the given input.
 *
 * If the `BEGIN:VCARD` and/or `END:VCARD` property is missing the
 * card will still be parsed. Currently even the `VERSION` property
 * is ignored for the time being because the parser currently is only
 * supporting version `4.0`. E The parser even doesn't care if the
 * `VERSION` is there or not. And if a property line has no "`:`" to
 * separate the property name from the property value it just assumes
 * that it is the `FN` property.
 *
 * So the following vCard-String
 *
 * ```vcard
 * BEGIN:VCARD
 * VERSION:4.0
 * FN:Arthur Dent
 * END:VCARD
 * ```
 *
 * would lead to the same result if the parser would have just parsed
 * the String `"Arthur Dent"` and parse to the same card as:
 *
 * ```kotlin
 * VCard("Arthur Dent")
 * ```
 *
 * ### Linebreaks
 *
 * The parser does not actually care about which of the commonly used
 * linebreak characters and combinations are used. Either `\r\n`,
 * `\n` or `\r` will be accepted. Even if one vCard uses different
 * line breaks.
 *
 * ### Case-Sensitivity
 *
 * As described in
 * [RFC 6350](https://datatracker.ietf.org/doc/html/rfc6350)
 * some parts are case-insensitive, so for example the property
 * [groups][de.dstulle.vkard.property.Property.group] or property
 * [names][de.dstulle.vkard.property.Property.name] will be recognized
 * no matter if they ar lower or upper case or a combination of both.
 *
 * ```vcard
 * bEgIn:VCARD
 * versION:4.0
 * fn:Arthur Dent
 * END:VCARD
 * ```
 *
 * will parse to the same card as:
 *
 * ```kotlin
 * VCard("Arthur Dent")
 * ```
 *
 * ### Value Escaping
 *
 * Because some characters might have special meaning and others are
 * used for text formatting and some lines might get long but should be
 * displayed wrapped without actually wrapping the content at the vCards
 * wrapping position there are some escape and wrapping techniques that
 * will be taken into account while parsing the input.
 *
 * ```vcard
 * BEGIN:VCARD
 * VERSION:4.0
 * FN:Arthur Dent
 * NOTE:Along with Ford Prefect\, Arthur Dent barely escapes the Earth's
 *   destruction as it is demolished to make way for a hyperspace bypass.
 * END:VCARD
 * ```
 *
 * will parse to the same card as:
 *
 * ```kotlin
 * VCard("Arthur Dent")
 *     .property(Note("Along with Ford Prefect, Arthur Dent barely escapes the Earth's destruction" +
 *             " as it is demolished to make way for a hyperspace bypass."))
 * ```
 *
 * ### Invalid Content
 *
 * Invalid Properties will be ignored.
 *
 * ```vcard
 * BEGIN:VCARD
 * VERSION:4.0
 * FN:Arthur Dent
 * INVALID:property
 * NON:existing
 * END:VCARD
 * ```
 *
 * will parse to the same card as:
 *
 * ```kotlin
 * VCard("Arthur Dent")
 * ```
 *
 * Invalid Parameters will be ignored.
 *
 * ```vcard
 * BEGIN:VCARD
 * VERSION:4.0
 * FN;INVALID=param;NON=existing:Arthur Dent
 * END:VCARD
 * ```
 *
 * will parse to the same card as:
 *
 * ```kotlin
 * VCard("Arthur Dent")
 * ```
 *
 * @param input the string to be parsed.
 * @return a VCard that represents the given string.
 */
fun parse(input: String): VCard {

    val inputLines = unfold(input).lines()

    val vCard = VCard()

    inputLines.mapNotNull { getProperty(it) }.forEach { property ->
        vCard.property(property)
    }

    return vCard
}

/**
 * removes all linebreaks and following whitespaces from the given string.
 *
 * @param input the string to be purged of linebreaks
 * @return a string without any linebreaks and whitespaces following the linebreak
 */
private fun unfold(input: String) = input.replace(Regex("(?:\\r|\\n|\\r\\n)[ \\t]"), "")

/**
 * Factory method that creates a [Property] from a given string if
 * possible.
 *
 * @param line as found in a vCard to represent a property
 * @return the property according to the input, null if not
 *     applicable
 */
private fun getProperty(line: String): Property<*>? {

    val nameAndValue = line.split(":", limit = 2)

    val group = if(nameAndValue.size == 2) {
        extractPropertyGroup(nameAndValue[0])
    } else { "" }

    val name = if(nameAndValue.size == 2) {
        extractPropertyName(nameAndValue[0])
    } else { "FN" }

    val parameters = if(nameAndValue.size == 2) {
        extractPropertyParameters(nameAndValue[0])
    } else { listOf() }

    val value = if(nameAndValue.size == 2) {
        nameAndValue[1]
    } else { nameAndValue[0] }

    val property = createProperty(name, value)

    if(property != null) {
        addGroupToProperty(group, property)
        addParameterToProperty(parameters, property)
    }

    return property
}

/**
 * creates a [Property] with the given name and value. The name
 * determines the actual Type of the Property.
 *
 * @param name of the Property to be created.
 * @param value of the Property to be created.
 * @return a Property depending on the name with the given value.
 *     null if the name can't be identified.
 */
private fun createProperty(name: String, value: String) = when (name.uppercase()) {
    "SOURCE" -> Source(value)
    "FN" -> Fn(processValue(value))
    "N" -> N(processStructuredValueList(value))
    "NICKNAME" -> Nickname(processValueList(value))
    "PHOTO" -> Photo(value)
    "ADR" -> Adr(processStructuredValueList(value))
    "TEL" -> Tel(processTelValue(value))
    "EMAIL" -> Email(processValue(value))
    "IMPP" -> Impp(value)
    "GEO" -> Geo(value)
    "TITLE" -> Title(processValue(value))
    "ROLE" -> Role(processValue(value))
    "LOGO" -> Logo(value)
    "ORG" -> Org(processStructuredValue(value))
    "RELATED" -> Related(value)
    "CATEGORIES" -> Categories(processValueList(value))
    "NOTE" -> Note(processValue(value))
    "SOUND" -> Sound(value)
    "URL" -> Url(value)
    else -> null
}

/**
 * adds the group to the given property.
 *
 * @param group the name of the group to be added.
 * @param property the property where the group should be added to.
 */
private fun addGroupToProperty(group: String, property: Property<out de.dstulle.vkard.property.value.Value<*>>) {
    property.group(group)
}

/**
 * adds parameters according to the given list to the given property.
 *
 * @param parameters a list of strings representing the parameters.
 * @param property the property where the parameters should be added to.
 */
private fun addParameterToProperty(parameters: List<String>, property: Property<out de.dstulle.vkard.property.value.Value<*>>) {
    for (parameterString in parameters) {
        val parameter = getParameter(parameterString)
        if (parameter != null) {
            property.parameter(parameter)
        }
    }
}

private fun extractPropertyGroup(nameString: String): String {
    val matchResult = Regex("(?:([a-zA-Z0-9-]+)\\.)?.*").matchEntire(nameString)

    return if(matchResult != null && matchResult.groups[1] != null) {
        matchResult.groups[1]!!.value
    } else {
        ""
    }
}

private fun extractPropertyName(nameString: String): String {
    val matchResult = Regex("(?:[a-zA-Z0-9-]+\\.)?([a-zA-Z0-9-]+).*").matchEntire(nameString)

    return if(matchResult != null) {
        matchResult.groups[1]!!.value
    } else {
        nameString
    }
}

private fun extractPropertyParameters(nameString: String): List<String> {
    val matchResult = Regex("[a-zA-Z0-9-.]+((?:;[^;]+)*)").matchEntire(nameString)

    return if(matchResult != null && matchResult.groups[1] != null) {
        matchResult.groups[1]!!.value.drop(1).split(";").toMutableList()
    } else {
        listOf()
    }
}

/**
 * creates a [Parameter] from the given string. The name
 * determines the actual Type of the Parameter.
 *
 * @param parameter String representation of the Parameter to be created.
 * @return a Parameter depending on the given value.
 *     null if the name can't be identified.
 */
private fun getParameter(parameter: String): Parameter<*>? {
    val name = extractParameterName(parameter)
    val value = extractParameterValue(parameter)

    return createParameter(name, value)
}

private fun extractParameterName(parameter: String): String {
    val matchResult = Regex("([a-zA-Z0-9-]+)=?.+").matchEntire(parameter)

    return if(matchResult != null && matchResult.groups[1] != null) {
        matchResult.groups[1]!!.value
    } else {
        ""
    }
}

private fun extractParameterValue(parameter: String): String {
    val matchResult = Regex("[a-zA-Z0-9-]+=?(.+)").matchEntire(parameter)

    return if(matchResult != null && matchResult.groups[1] != null) {
        matchResult.groups[1]!!.value
    } else {
        ""
    }
}

/**
 * creates a [Parameter] with the given name and value. The name
 * determines the actual Type of the Property.
 *
 * @param name of the Parameter to be created.
 * @param value of the Parameter to be created.
 * @return a Parameter depending on the name with the given value.
 *     null if the name can't be identified.
 */
private fun createParameter(name: String, value: String) = when (name.uppercase()) {
    "LANGUAGE" -> Language(value)
    "VALUE" -> Value(value)
    "PREF" -> Pref(value.toIntOrNull() ?: 0)
    "ALTID" -> AltId(value)
    "PID" -> PId(processPidValue(value))
    "TYPE" -> Type(value.split(",").toMutableSet())
    else -> null
}

private fun processPidValue(value: String): PropertyIdValue {
    return PropertyIdValue(
        value.split(",").map { ids ->
            if(ids.contains(".")) {
                @Suppress("UNCHECKED_CAST")
                ids.split(".").map{ it.toInt() }.zipWithNext() as Pair<Int, Int?>
            } else {
                Pair<Int, Int?>(ids.toInt(), null)
            }
        }.toMutableList()
    )
}

private fun processValue(value: String): String {
    return value
        .replace("\\n", "\n", true)
        .replace(Regex("\\\\([,;\\\\])")) {
            it.groups[1]!!.value
        }
}

private fun processValueList(value: String): TextListValue {
    return TextListValue(Regex("((?:\\\\[,;\\\\nN]|[^,\\\\])+),?")
        .findAll(value)
        .map { TextValue(processValue(it.groups[1]!!.value)) }.toMutableList())
}

private fun processStructuredValueList(value: String): List<TextListValue> {
    return Regex("((?:\\\\[,;\\\\nN]|[^;\\\\])*);?")
        .findAll(value)
        .map { processValueList(it.groups[1]!!.value) }.toList()
}

private fun processStructuredValue(value: String): StructuredTextValue {
    return StructuredTextValue(
        Regex("((?:\\\\[,;\\\\nN]|[^;\\\\])+)(?:;|$)")
            .findAll(value)
            .map { it.groups[1]!!.value }
            .toList()
            .map { TextListValue(it) }
            .toMutableList()
    )
}

private fun processTelValue(value: String): UriValue {
    return if(value.startsWith("tel:", true)) {
        UriValue.parseUri(value)
    } else {
        UriValue.parseUri("tel:$value")
    }
}
