/*
 * GNU LESSER GENERAL PUBLIC LICENSE
 * Copyright (C) 2021 - 2024 Daniel Sturm
 * mail@DanielSturm.de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.dstulle.vkard.properties.addressing

import de.dstulle.vkard.property.Property
import de.dstulle.vkard.property.value.StructuredTextValue
import de.dstulle.vkard.property.value.TextListValue

class Adr(
    value: StructuredTextValue
): Property<StructuredTextValue>(
    value
) {

    constructor(
        postOfficeBox: TextListValue = TextListValue(),
        extendedAddress: TextListValue = TextListValue(),
        streetAddress: TextListValue = TextListValue(),
        locality: TextListValue = TextListValue(),
        region: TextListValue = TextListValue(),
        postalCode: TextListValue = TextListValue(),
        countryName: TextListValue = TextListValue()
    ): this(
        StructuredTextValue(mutableListOf(
            postOfficeBox,
            extendedAddress,
            streetAddress,
            locality,
            region,
            postalCode,
            countryName
        ))
    )

    constructor(
        postOfficeBox: String? = null,
        extendedAddress: String? = null,
        streetAddress: String? = null,
        locality: String? = null,
        region: String? = null,
        postalCode: String? = null,
        countryName: String? = null
    ): this(
        TextListValue(postOfficeBox?: ""),
        TextListValue(extendedAddress ?: ""),
        TextListValue(streetAddress ?: ""),
        TextListValue(locality ?: ""),
        TextListValue(region ?: ""),
        TextListValue(postalCode ?: ""),
        TextListValue(countryName ?: "")
    )

    constructor(values: List<TextListValue>): this(
        values.getOrNull(0)?: TextListValue(),
        values.getOrNull(1)?: TextListValue(),
        values.getOrNull(2)?: TextListValue(),
        values.getOrNull(3)?: TextListValue(),
        values.getOrNull(4)?: TextListValue(),
        values.getOrNull(5)?: TextListValue(),
        values.getOrNull(6)?: TextListValue()
    )

    override val name = "ADR"

    override val typeParameterIsAllowed = true

}