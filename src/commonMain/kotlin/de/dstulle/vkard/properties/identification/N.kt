/*
 * GNU LESSER GENERAL PUBLIC LICENSE
 * Copyright (C) 2021 - 2024 Daniel Sturm
 * mail@DanielSturm.de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.dstulle.vkard.properties.identification

import de.dstulle.vkard.property.Cardinality
import de.dstulle.vkard.property.Property
import de.dstulle.vkard.property.value.StructuredTextValue
import de.dstulle.vkard.property.value.TextListValue

class N(
    value: StructuredTextValue
): Property<StructuredTextValue>(
    value
) {

    constructor(
        familyNames: TextListValue = TextListValue(),
        givenNames: TextListValue = TextListValue(),
        additionalNames: TextListValue = TextListValue(),
        honorificPrefixes: TextListValue = TextListValue(),
        honorificSuffixes: TextListValue = TextListValue()
    ): this(
        StructuredTextValue(mutableListOf(
            familyNames,
            givenNames,
            additionalNames,
            honorificPrefixes,
            honorificSuffixes
        ))
    )

    constructor(
        familyName: String? = null,
        givenName: String? = null,
        additionalName: String? = null,
        honorificPrefix: String? = null,
        honorificSuffix: String? = null
    ): this(
        TextListValue(familyName?: ""),
        TextListValue(givenName?: ""),
        TextListValue(additionalName?: ""),
        TextListValue(honorificPrefix?: ""),
        TextListValue(honorificSuffix?: "")
    )

    constructor(values: List<TextListValue>): this(
        values.getOrNull(0)?: TextListValue(),
        values.getOrNull(1)?: TextListValue(),
        values.getOrNull(2)?: TextListValue(),
        values.getOrNull(3)?: TextListValue(),
        values.getOrNull(4)?: TextListValue()
    )

    override val cardinality = Cardinality.EXACTLY_ONE_MAY

    override val name = "N"

    override fun mergeWith(other: Property<*>) {
        super.mergeWith(other)
        if(other is N) {
            value.mergeWith(other.value)
        }
    }

}