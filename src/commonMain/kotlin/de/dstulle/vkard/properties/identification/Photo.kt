/*
 * GNU LESSER GENERAL PUBLIC LICENSE
 * Copyright (C) 2021 - 2024 Daniel Sturm
 * mail@DanielSturm.de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.dstulle.vkard.properties.identification

import de.dstulle.vkard.property.Property
import de.dstulle.vkard.property.parameter.Value.Type.URI
import de.dstulle.vkard.property.value.UriValue

class Photo(
    value: UriValue
): Property<UriValue>(
    value
) {

    constructor(value: String): this(UriValue(value))

    override val name = "PHOTO"

    override val valueParameterRestriction = listOf(URI)

    override val typeParameterIsAllowed = true

}
