/*
 * GNU LESSER GENERAL PUBLIC LICENSE
 * Copyright (C) 2021 - 2024 Daniel Sturm
 * mail@DanielSturm.de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.dstulle.vkard.properties.organizational

import de.dstulle.vkard.property.Property
import de.dstulle.vkard.property.value.StructuredTextValue
import de.dstulle.vkard.property.value.TextListValue

class Org(
    value: StructuredTextValue
): Property<StructuredTextValue>(
    value
) {

    constructor(
        values: List<String>
    ): this(
        StructuredTextValue(values.map { TextListValue(it) }.toMutableList())
    )

    constructor(
        vararg name: String
    ): this(
        name.toList()
    )

    override val name = "ORG"

    override val typeParameterIsAllowed = true

}