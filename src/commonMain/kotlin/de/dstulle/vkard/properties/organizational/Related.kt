/*
 * GNU LESSER GENERAL PUBLIC LICENSE
 * Copyright (C) 2024 Daniel Sturm
 * mail@DanielSturm.de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.dstulle.vkard.properties.organizational

import de.dstulle.vkard.property.Property
import de.dstulle.vkard.property.parameter.Type
import de.dstulle.vkard.property.parameter.Type.Value.*
import de.dstulle.vkard.property.parameter.Value.Type.TEXT
import de.dstulle.vkard.property.parameter.Value.Type.URI
import de.dstulle.vkard.property.value.AnyBasicValue
import de.dstulle.vkard.property.value.TextValue
import de.dstulle.vkard.property.value.UriValue

class Related private constructor(
    value: AnyBasicValue<*>
) : Property<AnyBasicValue<*>>(
    value
) {

    constructor(value: UriValue) : this(AnyBasicValue(value))
    constructor(value: TextValue) : this(AnyBasicValue(value))
    constructor(
        value: String
    ) : this(
        if (isUri(value)) {
            AnyBasicValue(UriValue(value))
        } else {
            AnyBasicValue(TextValue(value))
        }
    )

    override val name = "RELATED"

    override val valueParameterRestriction = listOf(TEXT, URI)

    override val typeParameterIsAllowed = true

    override val typeParameterAllowedValues: Collection<Type.Value> = listOf(
        CONTACT,
        ACQUAINTANCE,
        FRIEND,
        MET,
        CO_WORKER,
        COLLEAGUE,
        CO_RESIDENT,
        NEIGHBOR,
        CHILD,
        PARENT,
        SIBLING,
        SPOUSE,
        KIN,
        MUSE,
        CRUSH,
        DATE,
        SWEETHEART,
        ME,
        AGENT,
        EMERGENCY
    )

    companion object {
        private fun isUri(value: String): Boolean {
            return Regex("^[a-zA-Z][a-zA-Z0-9+.-]*:[^ ]*\$").matches(value)
        }
    }

}
