/*
 * GNU LESSER GENERAL PUBLIC LICENSE
 * Copyright (C) 2021 - 2024 Daniel Sturm
 * mail@DanielSturm.de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.dstulle.vkard.property

/**
 * Describes the Cardinality of a property. see:
 * [RFC 6350 3.3](https://datatracker.ietf.org/doc/html/rfc6350#section-3.3).
 */
enum class Cardinality(val exactlyOne: Boolean, val must: Boolean) {
    /** __1__  | Exactly one instance per vCard MUST be present. */
    EXACTLY_ONE_MUST(true, true),
    /** __*1__ | Exactly one instance per vCard MAY be present. */
    EXACTLY_ONE_MAY(true, false),
    /** __1*__ | One or more instances per vCard MUST be present. */
    ONE_OR_MORE_MUST(false, true),
    /** __*__ | One or more instances per vCard MAY be present. */
    ONE_OR_MORE_MAY(false, false),

}
