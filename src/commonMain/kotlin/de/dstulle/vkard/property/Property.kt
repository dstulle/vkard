/*
 * GNU LESSER GENERAL PUBLIC LICENSE
 * Copyright (C) 2021 - 2024 Daniel Sturm
 * mail@DanielSturm.de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.dstulle.vkard.property

import de.dstulle.vkard.property.parameter.Parameter
import de.dstulle.vkard.property.parameter.ParameterType
import de.dstulle.vkard.property.parameter.PropertyIdValue
import de.dstulle.vkard.property.parameter.Type
import de.dstulle.vkard.property.parameter.Type.Value.HOME
import de.dstulle.vkard.property.parameter.Type.Value.WORK
import de.dstulle.vkard.property.parameter.Value.Type.TEXT
import de.dstulle.vkard.property.value.Value
import de.dstulle.vkard.property.parameter.Type as TypeParameter
import de.dstulle.vkard.property.parameter.Value as ValueParameter

/**
 * This class serves as Parent for all
 * Properties of a vCard as described in
 * [RFC 6350 Section 6](https://datatracker.ietf.org/doc/html/rfc6350#section-6)
 *
 * The Properties
 * [BEGIN](https://datatracker.ietf.org/doc/html/rfc6350#section-6.1.1)
 * and
 * [END](https://datatracker.ietf.org/doc/html/rfc6350#section-6.1.2)
 * are not represented by a Property class, because each of them has to
 * appear exactly once per card. Therefore, the possibility to add, alter
 * or remove those properties would not be necessary.
 *
 * All Properties have a name, a value and a group. The name is bound to
 * the property class itself and the value stores the actual information
 * in various formats.
 *
 * The group is optional prefix of the property to connect
 * related properties. The only significance of the group
 * is a convention that all properties of the same group
 * should be shown together when displayed as described in
 * [RFC 6350 3.3](https://datatracker.ietf.org/doc/html/rfc6350#section-3.3).
 */
abstract class Property<V : Value<*>>(val value: V, var group: String = "") {

    abstract val name: String

    private val parameters: LinkedHashMap<ParameterType, Parameter<*>> = linkedMapOf()

    val language: String?
        get() = parameters[ParameterType.LANGUAGE]?.value as String?

    val valueType: String?
        get() = parameters[ParameterType.VALUE]?.value as String?

    val pref: Int?
        get() = parameters[ParameterType.PREF]?.value as Int?

    val altId: String?
        get() = parameters[ParameterType.ALTID]?.value as String?

    val pId: PropertyIdValue?
        get() = parameters[ParameterType.PID]?.value as PropertyIdValue?

    @Suppress("UNCHECKED_CAST")
    val types: Collection<String>
        get() {
            val typeParameter = parameters[ParameterType.TYPE]
            return if(typeParameter != null) {
                typeParameter.value as Collection<String>
            } else {
                listOf()
            }
        }

    open val cardinality: Cardinality = Cardinality.ONE_OR_MORE_MAY

    open val valueParameterRestriction: Collection<ValueParameter.Type> = listOf(TEXT)

    open val typeParameterIsAllowed: Boolean = false

    open val typeParameterAllowedValues: Collection<TypeParameter.Value> = listOf(WORK, HOME)

    fun group(name: String): Property<V> {
        group = name
        return this
    }

    fun parameter(parameter: Parameter<*>): Property<V> {
        when(parameter.name) {
            ParameterType.VALUE -> {
                if (valueParameterRestriction.isEmpty() || valueParameterRestriction.map { it.toVCardString() }.contains(parameter.value)) {
                    parameters[ParameterType.VALUE] = parameter
                }
            }
            ParameterType.PID -> {
                if (cardinality == Cardinality.ONE_OR_MORE_MUST || cardinality == Cardinality.ONE_OR_MORE_MAY) {
                    parameters[ParameterType.PID] = parameter
                }
            }
            ParameterType.TYPE -> {
                if(typeParameterIsAllowed) {
                    val newTypeParameter = Type(
                        @Suppress("UNCHECKED_CAST")
                        (parameter.value as Collection<String>).filter {
                            typeParameterAllowedValues.map { allowed ->
                                allowed.toVCardString().lowercase()
                            }.contains(it.lowercase())
                        }.toMutableSet()
                    )
                    if(parameters[ParameterType.TYPE] == null) {
                        parameters[ParameterType.TYPE] = newTypeParameter
                    } else {
                        (parameters[ParameterType.TYPE] as Type).mergeWith(newTypeParameter)
                    }
                }
            }
            else -> {
                parameters[parameter.name] = parameter
            }
        }
        return this
    }

    fun removeParameter(parameter: Parameter<*>) {
        removeParameter(parameter.name)
    }

    fun removeParameter(name: ParameterType) {
        parameters.remove(name)
    }

    /**
     * Returns a formatted string of this Property according to
     * [RFC 6350](https://datatracker.ietf.org/doc/html/rfc6350) with group name
     * and values.
     *
     * @return [RFC 6350](https://datatracker.ietf.org/doc/html/rfc6350) conform
     *     representation of this Property
     */
    open fun toVCardString(): String {
        val stringBuilder = StringBuilder()
        if(group.isNotBlank()) {
            stringBuilder.append("${group.uppercase()}.")
        }
        stringBuilder.append(name.uppercase())
        for(parameter in parameters.values) {
            stringBuilder.append(";${parameter.toVCardString()}")
        }
        stringBuilder.append(":${value.toVCardString()}")

        return stringBuilder.toString()
    }

    /**
     * Indicates whether some other object is "equal to" this Property.
     *
     * The other Property has to have the same group as the other. Case
     * does not matter for the group. The name of the property has to
     * be the same and the value string has to be the same as the value
     * string of the other property.
     */
    override fun equals(other: Any?): Boolean {
        return when {
            this === other -> true
            other is Property<*> -> group.equals(other.group, ignoreCase = true) &&
                    name == other.name && (
                            parameters.values.all { other.has(it) } &&
                            other.parameters.values.all { this.has(it) }
                    ) &&
                    value.toVCardString() == other.value.toVCardString()
            else -> false
        }
    }

    /**
     * Returns a hash code value for the Property.
     */
    override fun hashCode(): Int {
        return group.uppercase().hashCode() + name.hashCode() + parameters.hashCode() + value.toVCardString().hashCode()
    }


    /**
     * checks if the given parameter with the same value is present at
     * this property.
     *
     * @return true if a parameter equal to the given is present otherwise false.
     */
    private fun has(parameter: Parameter<*>): Boolean {
        return parameters.values.any { it == parameter }
    }

    open fun mergeWith(other: Property<*>) {
        other.parameters.values.forEach { this.parameter(it) }
    }

}