/*
 * GNU LESSER GENERAL PUBLIC LICENSE
 * Copyright (C) 2021 - 2024 Daniel Sturm
 * mail@DanielSturm.de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.dstulle.vkard.property.parameter

enum class ParameterType {

    /**
     * See: [RFC 6350 5.1](https://datatracker.ietf.org/doc/html/rfc6350#section-5.1)
     */
    LANGUAGE,

    /**
     * See: [RFC 6350 5.2](https://datatracker.ietf.org/doc/html/rfc6350#section-5.2)
     */
    VALUE,

    /**
     * See: [RFC 6350 5.3](https://datatracker.ietf.org/doc/html/rfc6350#section-5.3)
     */
    PREF,

    /**
     * See: [RFC 6350 5.4](https://datatracker.ietf.org/doc/html/rfc6350#section-5.4)
     */
    ALTID,

    /**
     * See: [RFC 6350 5.5](https://datatracker.ietf.org/doc/html/rfc6350#section-5.5)
     */
    PID,

    /**
     * See: [RFC 6350 5.6](https://datatracker.ietf.org/doc/html/rfc6350#section-5.6)
     */
    TYPE,

}
