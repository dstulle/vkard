/*
 * GNU LESSER GENERAL PUBLIC LICENSE
 * Copyright (C) 2022 - 2024 Daniel Sturm
 * mail@DanielSturm.de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.dstulle.vkard.property.parameter

import kotlin.math.absoluteValue

/**
 * This value is either a single small positive integer or a pair of small positive integers separated by a dot.
 * Multiple values may be encoded by separating the values with a comma ","
 *
 * See also:
 * [RFC 6350 5.5](https://datatracker.ietf.org/doc/html/rfc6350#section-5.5)
 */
data class PropertyIdValue(val value: MutableList<Pair<Int,Int?>>) {

    constructor(id: Int): this(id, null)
    constructor(firstId: Int, secondId: Int?): this(Pair(firstId, secondId))
    constructor(vararg ids : Pair<Int, Int?>): this(mutableListOf(*ids))

    fun toVCardString(): String {
        return value.joinToString(",") {
            if(it.second == null) {
                it.first.toString()
            } else {
                "${it.first.absoluteValue}.${it.second?.absoluteValue}"
            }
        }
    }

}