/*
 * GNU LESSER GENERAL PUBLIC LICENSE
 * Copyright (C) 2022 - 2024 Daniel Sturm
 * mail@DanielSturm.de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.dstulle.vkard.property.parameter

/**
 * See: [RFC 6350 5.6](https://datatracker.ietf.org/doc/html/rfc6350#section-5.6)
 */
class Type(types: MutableSet<String>): Parameter<MutableSet<String>>(ParameterType.TYPE, types) {

    constructor(type: String): this(mutableSetOf(type))

    constructor(type: Value): this(mutableSetOf(type.toVCardString()))

    constructor(vararg types : String): this(mutableSetOf(*types))

    constructor(vararg types : Value): this(setOf(*types).map { it.toVCardString() }.toMutableSet())

    enum class Value {
        WORK,
        HOME,
        TEXT,
        VOICE,
        FAX,
        CELL,
        VIDEO,
        PAGER,
        TEXTPHONE,
        CONTACT,
        ACQUAINTANCE,
        FRIEND,
        MET,
        CO_WORKER,
        COLLEAGUE,
        CO_RESIDENT,
        NEIGHBOR,
        CHILD,
        PARENT,
        SIBLING,
        SPOUSE,
        KIN,
        MUSE,
        CRUSH,
        DATE,
        SWEETHEART,
        ME,
        AGENT,
        EMERGENCY;

        fun toVCardString(): String {
            return name.lowercase().replace("_","-")
        }
    }

    fun mergeWith(other: Type): Type {
        this.value.addAll(other.value)
        return this
    }

    override fun toVCardString(): String {
        return "$name=${value.joinToString(",")}"
    }

}