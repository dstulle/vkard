/*
 * GNU LESSER GENERAL PUBLIC LICENSE
 * Copyright (C) 2024 Daniel Sturm
 * mail@DanielSturm.de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.dstulle.vkard.property.value

/**
 * Wraps any [BasicValue]. To be used in situations when
 * a Property can have multiple possible Value Types. For
 * example [de.dstulle.vkard.properties.organizational.Related]
 */
data class AnyBasicValue<T>(
    private val value: BasicValue<T>
) : BasicValue<T>() {

    override fun get(): T = value.get()

    override fun isEmpty(): Boolean {
        return value.isEmpty()
    }

    override fun toVCardString(): String {
        return value.toVCardString()
    }

    override fun equals(other: Any?): Boolean {
        if(this === other) return true
        if(other !is AnyBasicValue<*>) return false

        if(this.value != other.value) return false

        return true
    }

    override fun hashCode(): Int {
        return value.hashCode()
    }

}