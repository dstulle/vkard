/*
 * GNU LESSER GENERAL PUBLIC LICENSE
 * Copyright (C) 2021 - 2024 Daniel Sturm
 * mail@DanielSturm.de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.dstulle.vkard.property.value

/**
 * A [Value] that stores a boolean value, that can either be `TRUE` or
 * `FALSE`. This value is not used in any property defined the RFC 6350
 * vCard.
 *
 * See also:
 * [RFC 6350 4.4](https://datatracker.ietf.org/doc/html/rfc6350#section-4.4)
 */
data class BooleanValue(private val value: Boolean): BasicValue<Boolean>() {

    constructor(booleanValue: BooleanValue): this(booleanValue.value)

    constructor(boolean: String): this(parseBoolean(boolean))

    override fun get(): Boolean = value

    override fun isEmpty(): Boolean {
        return false
    }

    override fun toVCardString(): String {
        return if (value) "TRUE" else "FALSE"
    }

    companion object {
        fun parseBoolean(boolean: String): Boolean {
            return boolean.toBoolean()
        }
    }

}