/*
 * GNU LESSER GENERAL PUBLIC LICENSE
 * Copyright (C) 2021 - 2024 Daniel Sturm
 * mail@DanielSturm.de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.dstulle.vkard.property.value

/**
 * A Text [Value] that consists of multiple values, with distinct meanings
 * like the Value of [N][de.dstulle.vkard.properties.identification.N],
 * [ADR][de.dstulle.vkard.properties.addressing.Adr] and
 * [ORG][de.dstulle.vkard.properties.organizational.Org]
 *
 * See also:
 * [RFC 6350 6.2.2](https://datatracker.ietf.org/doc/html/rfc6350#section-6.2.2)
 * [RFC 6350 6.3.1](https://datatracker.ietf.org/doc/html/rfc6350#section-6.3.1)
 * [RFC 6350 6.6.4](https://datatracker.ietf.org/doc/html/rfc6350#section-6.6.4)
 */
class StructuredTextValue(value: MutableList<TextListValue>): StructuredValue<TextListValue>(value) {

    constructor(structuredTextValue: StructuredTextValue): this(structuredTextValue.value)
    constructor(): this(mutableListOf())

    override fun isEmpty(): Boolean {
        return value.map{ it.isEmpty()}.reduceOrNull { acc, isEmpty -> acc && isEmpty }?:true
    }

    override fun equals(other: Any?): Boolean {
        if(this === other) return true
        if(other !is StructuredTextValue) return false
        if(value.size != other.value.size) return false

        for(i in 0 until value.size) {
            if(this.value[i] != other.value[i]) return false
        }

        return true
    }

    override fun hashCode(): Int {
        return value.hashCode()
    }

    override fun toVCardString(): String {
        return value.joinToString(";") { it.toVCardString() }
    }

}