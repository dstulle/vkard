/*
 * GNU LESSER GENERAL PUBLIC LICENSE
 * Copyright (C) 2021 - 2024 Daniel Sturm
 * mail@DanielSturm.de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.dstulle.vkard.property.value

/**
 * A Value that consists of multiple values, with distinct meanings
 * like the Value of GENDER, CLIENTPIDMAP or [StructuredTextValue]
 *
 * See also:
 * [RFC 6350 6.2.7](https://datatracker.ietf.org/doc/html/rfc6350#section-6.2.7)
 * [RFC 6350 6.7.7](https://datatracker.ietf.org/doc/html/rfc6350#section-6.7.7)
 */
abstract class StructuredValue<T: BasicValue<*>>(protected val value: MutableList<T>): Value<List<T>>() {

    override fun toVCardString(): String {
        return value.joinToString(";") { it.toVCardString() }
    }

    override fun get(): List<T> = value

    fun mergeWith(other: StructuredValue<T>) {
        if(value.size == other.value.size) {
            for(i in value.indices) {
                if (other.value[i]::class.isInstance(value[i]) && other.value[i].isNotEmpty()) {
                    value[i] = other.value[i]
                }
            }
        }
    }

}