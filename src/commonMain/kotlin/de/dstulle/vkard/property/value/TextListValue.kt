/*
 * GNU LESSER GENERAL PUBLIC LICENSE
 * Copyright (C) 2021 - 2024 Daniel Sturm
 * mail@DanielSturm.de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.dstulle.vkard.property.value

/**
 * A Text list [Value] that consists of one or more values.
 * For example as used in the Properties
 * [NICKNAME][de.dstulle.vkard.properties.identification.Nickname] and
 * CATEGORIES
 *
 * See also:
 * [RFC 6350 4](https://datatracker.ietf.org/doc/html/rfc6350#section-4)
 * [RFC 6350 4.1](https://datatracker.ietf.org/doc/html/rfc6350#section-4.1)
 * [RFC 6350 6.2.3](https://datatracker.ietf.org/doc/html/rfc6350#section-6.2.3)
 * [RFC 6350 6.7.1](https://datatracker.ietf.org/doc/html/rfc6350#section-6.7.1)
 */
data class TextListValue(private val value: MutableList<TextValue>): BasicValue<List<String>>() {

    constructor(structuredTextValue: TextListValue): this(structuredTextValue.value)

    constructor(textValue: TextValue): this(mutableListOf(textValue))
    constructor(vararg texts: String): this(texts.map { TextValue(it) }.toMutableList())
    constructor(text: String): this(TextValue(text))
    constructor(): this(mutableListOf())

    override fun get(): List<String> = value.map { it.get() }

    override fun isEmpty(): Boolean {
        return value.map{ it.isEmpty()}.reduceOrNull { acc, isEmpty -> acc && isEmpty }?:true
    }

    override fun toVCardString(): String {
        return value.joinToString(",") { it.toVCardString() }
    }

}