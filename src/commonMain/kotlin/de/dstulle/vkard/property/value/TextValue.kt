/*
 * GNU LESSER GENERAL PUBLIC LICENSE
 * Copyright (C) 2021 - 2024 Daniel Sturm
 * mail@DanielSturm.de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.dstulle.vkard.property.value

/**
 * A Text [Value] that consists of only one single
 * value. For example as used in the Properties XML,
 * [FN][de.dstulle.vkard.properties.identification.Fn],
 * [TEL][de.dstulle.vkard.properties.communications.Tel],
 * [EMAIL][de.dstulle.vkard.properties.communications.Email], TZ
 * [TITLE][de.dstulle.vkard.properties.organizational.Title],
 * [ROLE][de.dstulle.vkard.properties.organizational.Role],
 * [NOTE][de.dstulle.vkard.properties.explanatory.Note], PRODID, UID and
 * KEY
 *
 * See also:
 * [RFC 6350 4](https://datatracker.ietf.org/doc/html/rfc6350#section-4)
 * [RFC 6350 4.1](https://datatracker.ietf.org/doc/html/rfc6350#section-4.1)
 * [RFC 6350 6.1.5](https://datatracker.ietf.org/doc/html/rfc6350#section-6.1.5)
 * [RFC 6350 6.2.1](https://datatracker.ietf.org/doc/html/rfc6350#section-6.2.1)
 * [RFC 6350 6.4.1](https://datatracker.ietf.org/doc/html/rfc6350#section-6.4.1)
 * [RFC 6350 6.4.2](https://datatracker.ietf.org/doc/html/rfc6350#section-6.4.2)
 * [RFC 6350 6.5.1](https://datatracker.ietf.org/doc/html/rfc6350#section-6.5.1)
 * [RFC 6350 6.6.1](https://datatracker.ietf.org/doc/html/rfc6350#section-6.6.1)
 * [RFC 6350 6.6.2](https://datatracker.ietf.org/doc/html/rfc6350#section-6.6.2)
 * [RFC 6350 6.7.2](https://datatracker.ietf.org/doc/html/rfc6350#section-6.7.2)
 * [RFC 6350 6.7.3](https://datatracker.ietf.org/doc/html/rfc6350#section-6.7.3)
 * [RFC 6350 6.7.6](https://datatracker.ietf.org/doc/html/rfc6350#section-6.7.6)
 * [RFC 6350 6.8.1](https://datatracker.ietf.org/doc/html/rfc6350#section-6.8.1)
 */
data class TextValue(private val value: String = ""): BasicValue<String>() {

    constructor(textValue: TextValue): this(textValue.value)

    override fun get(): String = value

    override fun isEmpty(): Boolean {
        return value.isEmpty()
    }

    override fun toVCardString(): String {
        return escapeValue(value)
    }

}