/*
 * GNU LESSER GENERAL PUBLIC LICENSE
 * Copyright (C) 2021 - 2024 Daniel Sturm
 * mail@DanielSturm.de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.dstulle.vkard.property.value

/**
 * A Uri [Value] that consists of only one single
 * value. For example as used in the Properties
 * [SOURCE][de.dstulle.vkard.properties.general.Source],
 * [PHOTO][de.dstulle.vkard.properties.identification.Photo],
 * [TEL][de.dstulle.vkard.properties.communications.Tel],
 * [IMPP][de.dstulle.vkard.properties.communications.Impp],
 * [GEO][de.dstulle.vkard.properties.geographical.Geo],
 * [LOGO][de.dstulle.vkard.properties.organizational.Logo], MEMBER,
 * [RELATED][de.dstulle.vkard.properties.organizational.Related],
 * [SOUND][de.dstulle.vkard.properties.explanatory.Sound],
 * UID, [URL][de.dstulle.vkard.properties.explanatory.Url], KEY, FBURL,
 * CALADRURI and CALURI
 *
 * See also:
 * [RFC 6350 4](https://datatracker.ietf.org/doc/html/rfc6350#section-4)
 * [RFC 6350 4.2](https://datatracker.ietf.org/doc/html/rfc6350#section-4.2)
 * [RFC 6350 6.1.3](https://datatracker.ietf.org/doc/html/rfc6350#section-6.1.3)
 * [RFC 6350 6.4.1](https://datatracker.ietf.org/doc/html/rfc6350#section-6.4.1)
 * [RFC 6350 6.4.3](https://datatracker.ietf.org/doc/html/rfc6350#section-6.4.3)
 * [RFC 6350 6.5.2](https://datatracker.ietf.org/doc/html/rfc6350#section-6.5.2)
 * [RFC 6350 6.6.3](https://datatracker.ietf.org/doc/html/rfc6350#section-6.6.3)
 * [RFC 6350 6.6.5](https://datatracker.ietf.org/doc/html/rfc6350#section-6.6.5)
 * [RFC 6350 6.6.6](https://datatracker.ietf.org/doc/html/rfc6350#section-6.6.6)
 * [RFC 6350 6.7.5](https://datatracker.ietf.org/doc/html/rfc6350#section-6.7.5)
 * [RFC 6350 6.7.6](https://datatracker.ietf.org/doc/html/rfc6350#section-6.7.6)
 * [RFC 6350 6.7.8](https://datatracker.ietf.org/doc/html/rfc6350#section-6.7.8)
 * [RFC 6350 6.8.1](https://datatracker.ietf.org/doc/html/rfc6350#section-6.8.1)
 * [RFC 6350 6.9.1](https://datatracker.ietf.org/doc/html/rfc6350#section-6.9.1)
 * [RFC 6350 6.9.2](https://datatracker.ietf.org/doc/html/rfc6350#section-6.9.2)
 * [RFC 6350 6.9.3](https://datatracker.ietf.org/doc/html/rfc6350#section-6.9.3)
 */
data class UriValue(
    private val scheme: String,
    private val userinfo: String = "",
    private val host: String = "",
    private val port: String = "",
    private val path: String,
    private val query: String = "",
    private val fragment: String = ""
): BasicValue<String>() {

    constructor(uriValue: UriValue): this(
        uriValue.scheme,
        uriValue.userinfo,
        uriValue.host,
        uriValue.port,
        uriValue.path,
        uriValue.query,
        uriValue.fragment
    )

    constructor(uri: String): this(parseUri(uri))

    override fun get(): String = toUriString()

    override fun isEmpty(): Boolean {
        return scheme.isEmpty() &&
                userinfo.isEmpty() &&
                host.isEmpty() &&
                port.isEmpty() &&
                path.isEmpty() &&
                query.isEmpty() &&
                fragment.isEmpty()
    }

    override fun toVCardString() = toUriString()

    fun toUriString(): String {
        return  if(scheme.isNotEmpty()) {
                       "$scheme:"
                    } else{
                        ""
                    } +
                if(host.isNotEmpty()) {
                    "//" +
                    if(userinfo.isNotEmpty()) {
                        "$userinfo@"
                    } else {
                        ""
                    } +
                    host +
                    if(port.isNotEmpty()) {
                        ":$port"
                    } else {
                        ""
                    }
                } else {
                    ""
                } +
                path +
                if(query.isNotEmpty()) {
                    "?$query"
                } else {
                    ""
                } +
                if(fragment.isNotEmpty()) {
                    "#$fragment"
                } else {
                    ""
                }
    }

    companion object {
        fun parseUri(value: String): UriValue {
            val matchResult = Regex("^([^:]+):(?://(?:([^@]+)@)?([^@/]+)(?::([0-9]+))?((?:/[^?]*)?)|([^/][^?]*))(?:\\?([^#]+))?(?:#(.+))?\$")
                .find(value)
            return UriValue(
                matchResult!!.groups[1]?.value ?: "",
                matchResult.groups[2]?.value ?: "",
                matchResult.groups[3]?.value ?: "",
                matchResult.groups[4]?.value ?: "",
                matchResult.groups[5]?.value ?: matchResult.groups[6]?.value ?: "",
                matchResult.groups[7]?.value ?: "",
                matchResult.groups[8]?.value ?: "",
            )
        }
    }

}