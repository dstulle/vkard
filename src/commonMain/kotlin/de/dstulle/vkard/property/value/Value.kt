/*
 * GNU LESSER GENERAL PUBLIC LICENSE
 * Copyright (C) 2021 - 2024 Daniel Sturm
 * mail@DanielSturm.de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.dstulle.vkard.property.value

/**
 * Base class for all Values a [Property][de.dstulle.vkard.property.Property] can have.
 */
abstract class Value<V> {

    protected fun escapeValue(value: String): String {
        return value.replace(Regex("([,;\\\\])")) { "\\" + it.groups[1]!!.value }.replace("\n", "\\n")
    }

    abstract fun get(): V

    abstract fun isEmpty(): Boolean

    fun isNotEmpty(): Boolean = !isEmpty()

    abstract fun toVCardString(): String

    abstract override fun equals(other: Any?): Boolean

    abstract override fun hashCode(): Int

}