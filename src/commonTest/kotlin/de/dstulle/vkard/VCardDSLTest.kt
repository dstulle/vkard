/*
 * GNU LESSER GENERAL PUBLIC LICENSE
 * Copyright (C) 2022 - 2024 Daniel Sturm
 * mail@DanielSturm.de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.dstulle.vkard

import de.dstulle.vkard.dsl.vCard
import de.dstulle.vkard.properties.explanatory.Note
import de.dstulle.vkard.properties.identification.Fn
import de.dstulle.vkard.property.parameter.Value
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals

internal class VCardDSLTest {

    @Test
    fun fromDslBasicCard() {
        val vCardEmpty = vCard

        val vCardArthur =
            vCard("Arthur Dent")

        val vCardSlartibartfast =
            vCard {
                fn("Slartibartfast")
            }

        assertEquals(VCard(), vCardEmpty)
        assertEquals(vCardEmpty, vCard)
        assertEquals(vCardEmpty, vCard())
        assertEquals(vCardEmpty, vCard(null))
        assertEquals(vCardEmpty, vCard(""))

        assertEquals(VCard("Arthur Dent"), vCardArthur)
        assertEquals(VCard().property(Fn("Arthur Dent")), vCardArthur)
        assertEquals(VCard("Slartibartfast"), vCardSlartibartfast)

        assertNotEquals(VCard("Arthur Dent"), vCardEmpty)
        assertNotEquals(VCard("Slartibartfast"), vCardEmpty)
        assertNotEquals(VCard(), vCardArthur)
        assertNotEquals(VCard("Slartibartfast"), vCardArthur)
        assertNotEquals(VCard(), vCardSlartibartfast)
        assertNotEquals(VCard("Arthur Dent"), vCardSlartibartfast)

        val vCardWithNote =
            vCard {
                note("Some Note")
            }

        assertEquals(VCard().property(Note("Some Note")), vCardWithNote)

    }

    @Test
    fun fromDslFullCard() {

        val vCardStringArthur =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN;LANGUAGE=en;VALUE=text:Arthur Dent
                SOURCE:ldap://ldap.example.com/cn=Arthur%20Dent,%20o=BBC,%20c=UK
                SOURCE;VALUE=uri:https://directory.example.com/addressbooks/earth/Arthur%20Dent.vcf
                N;LANGUAGE=en:Dent;Arthur;Philip;;
                NICKNAME;LANGUAGE=en:Ape Man,Chimp Man,Dumb Dumb,Earthman,Monkey,Monkey Man,Primate,Semi-Evolved Simian
                RELATED;VALUE=uri:https://directory.example.com/addressbooks/earth/Tricia%20McMillan.vcf
                CATEGORIES:human,male
                PHOTO;PREF=42;ALTID=profile:https://www.example.com/pub/photos/arthur_dent.png
                PHOTO;PREF=23;ALTID=profile:data:image/jpeg;base64,MIICajCCAdOgAwIBAgICBEUwDQYJKoZIhvAQEEBQAwdzELMAkGA1UEBhMCVVMxLDAqBgNVBAoTI05ldHNjYXBlIENvbW11bmljYXRpb25zIENvcnBvcm
                LOGO:https://www.example.com/pub/logos/hitchhiker.png
                SOUND;PID=47.11,8,15:https://www.example.com/pub/sound/arthur_dent.ogg
                URL:https://www.example.com/
                ADR:;;155 Country Lane;Cottington;Cottingshire County;;United Kingdom
                GEO:geo:51.935163,0.033072
                EMAIL;TYPE=home:Arthur Dent <arthur.dent@example.com>
                IMPP:xmpp:arthur.dent@example.com
                TITLE:The Hitchhiker
                ROLE:Main Protagonist
                ORG:BBC
                NOTE:Along with Ford Prefect\, Arthur Dent barely escapes the Earth's destruction as it is demolished to make way for a hyperspace bypass.
                END:VCARD
            """.trimIndent().replace("\n", "\r\n")

        val vCardArthur = vCard {
            fn("Arthur Dent") {
                language("en")
                value(Value.Type.TEXT)
            }
            source("ldap://ldap.example.com/cn=Arthur%20Dent,%20o=BBC,%20c=UK")
            source("https://directory.example.com/addressbooks/earth/Arthur%20Dent.vcf") {
                value("uri")
            }
            n("Dent", "Arthur", "Philip") {
                language("en")
            }
            nickname("Ape Man", "Chimp Man", "Dumb Dumb", "Earthman", "Monkey", "Monkey Man", "Primate", "Semi-Evolved Simian") {
                language("en")
            }
            related("https://directory.example.com/addressbooks/earth/Tricia%20McMillan.vcf") {
                value(Value.Type.URI)
            }
            categories("human", "male")
            photo("https://www.example.com/pub/photos/arthur_dent.png") {
                pref(42)
                altid("profile")
            }
            photo("data:image/jpeg;base64,MIICajCCAdOgAwIBAgICBEUwDQYJKoZIhvAQEEBQAwdzELMAkGA1UEBhMCVVMxLDAqBgNVBAoTI05ldHNjYXBlIENvbW11bmljYXRpb25zIENvcnBvcm") {
                pref(23)
                altid("profile")
            }
            logo("https://www.example.com/pub/logos/hitchhiker.png")
            sound("https://www.example.com/pub/sound/arthur_dent.ogg") {
                pid( Pair(47, 11), Pair(8, null), Pair(15, null) ) //there might be a more elegant way to do this.
            }
            url("https://www.example.com/")
            adr(streetAddress = "155 Country Lane", locality = "Cottington", region = "Cottingshire County", countryName = "United Kingdom")
            geo("geo", path = "51.935163,0.033072")
            email("Arthur Dent <arthur.dent@example.com>") {
                type("home")
            }
            impp("xmpp:arthur.dent@example.com")
            title("The Hitchhiker")
            role("Main Protagonist")
            org("BBC")
            note("Along with Ford Prefect, Arthur Dent barely escapes the Earth's destruction as it is demolished to make way for a hyperspace bypass.")
        }

        val vCardStringTrillian =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Tricia McMillan
                SOURCE:https://directory.example.com/addressbooks/earth/Tricia%20McMillan.vcf
                N:McMillan;Tricia;;Dr.;
                NICKNAME:Trillian,Trillian Astra
                RELATED:urn:uuid:f81d4fae-7dec-11d0-a765-00a0c91e6bf6
                PHOTO:https://www.example.com/pub/photos/tricia_mcmillan.png
                GEO:geo:51.935163,0.033072
                TEL;TYPE=voice:tel:2079460347
                EMAIL:"Tricia McMillan" tricia.mcmillan@example.com
                IMPP;PID=42:xmpp:tricia.mcmillan@example.com
                IMPP;PID=3:xmpp:trillian@example.com
                TITLE:The Girlfriend
                ROLE:Wife of the President of the Algolian Chapter of the Galactic Rotary Club
                NOTE:Tricia McMillan\, usually known as Trillian and sometimes Trillian Astra\, was a human astrophysicist and mathematician whom Arthur Dent completely failed to chat up at a party at a flat in Islington.
                END:VCARD
            """.trimIndent().replace("\n", "\r\n")


        val vCardTrillian = vCard("Tricia McMillan") {
            source("https://directory.example.com/addressbooks/earth/Tricia%20McMillan.vcf")
            n("McMillan", "Tricia", honorificPrefix = "Dr.")
            nickname("Trillian", "Trillian Astra")
            related(scheme = "urn", path = "uuid:f81d4fae-7dec-11d0-a765-00a0c91e6bf6")
            photo("https://www.example.com/pub/photos/tricia_mcmillan.png")
            geo("geo:51.935163,0.033072")
            tel("tel:2079460347") {
                type("voice")
            }
            email("\"Tricia McMillan\" tricia.mcmillan@example.com")
            impp("xmpp:tricia.mcmillan@example.com") {
                pid(42)
            }
            impp("xmpp:trillian@example.com") {
                pid(3)
            }
            title("The Girlfriend")
            role("Wife of the President of the Algolian Chapter of the Galactic Rotary Club")
            note("Tricia McMillan, usually known as Trillian and sometimes Trillian Astra, was a human astrophysicist and mathematician whom Arthur Dent completely failed to chat up at a party at a flat in Islington.")
        }

        assertEquals(vCardArthur.toVCardString(), vCardStringArthur)
        assertEquals(vCardTrillian.toVCardString(), vCardStringTrillian)

        assertNotEquals(vCardStringArthur, vCardTrillian.toVCardString(), vCardStringArthur)
        assertNotEquals(vCardArthur.toVCardString(), vCardStringTrillian)
    }

}