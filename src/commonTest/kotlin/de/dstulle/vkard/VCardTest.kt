/*
 * GNU LESSER GENERAL PUBLIC LICENSE
 * Copyright (C) 2021 - 2024 Daniel Sturm
 * mail@DanielSturm.de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.dstulle.vkard

import de.dstulle.vkard.properties.addressing.Adr
import de.dstulle.vkard.properties.communications.Email
import de.dstulle.vkard.properties.communications.Impp
import de.dstulle.vkard.properties.communications.Tel
import de.dstulle.vkard.properties.explanatory.Categories
import de.dstulle.vkard.properties.explanatory.Note
import de.dstulle.vkard.properties.explanatory.Sound
import de.dstulle.vkard.properties.explanatory.Url
import de.dstulle.vkard.properties.general.Source
import de.dstulle.vkard.properties.geographical.Geo
import de.dstulle.vkard.properties.identification.Fn
import de.dstulle.vkard.properties.identification.N
import de.dstulle.vkard.properties.identification.Nickname
import de.dstulle.vkard.properties.identification.Photo
import de.dstulle.vkard.properties.organizational.*
import de.dstulle.vkard.property.parameter.*
import de.dstulle.vkard.property.value.TextListValue
import de.dstulle.vkard.property.value.UriValue
import kotlin.test.*

internal class VCardTest {

    @Test
    fun toStringBasicCard() {
        val vCardEmpty =
            listOf("BEGIN:VCARD",
                "VERSION:4.0",
                "FN:",
                "END:VCARD").joinToString("\r\n")

        val vCardArthur =
            listOf("BEGIN:VCARD",
                "VERSION:4.0",
                "FN:Arthur Dent",
                "END:VCARD").joinToString("\r\n")

        val vCardSlartibartfast =
            listOf("BEGIN:VCARD",
                "VERSION:4.0",
                "FN:Slartibartfast",
                "END:VCARD").joinToString("\r\n")

        assertEquals(vCardEmpty, VCard().toVCardString())
        assertEquals(vCardArthur, VCard("Arthur Dent").toVCardString())
        assertEquals(vCardArthur, VCard().property(Fn("Arthur Dent")).toVCardString())
        assertEquals(vCardSlartibartfast, VCard("Slartibartfast").toVCardString())

        assertNotEquals(vCardEmpty, VCard("Arthur Dent").toVCardString())
        assertNotEquals(vCardEmpty, VCard("Slartibartfast").toVCardString())
        assertNotEquals(vCardArthur, VCard().toVCardString())
        assertNotEquals(vCardArthur, VCard("Slartibartfast").toVCardString())
        assertNotEquals(vCardSlartibartfast, VCard().toVCardString())
        assertNotEquals(vCardSlartibartfast, VCard("Arthur Dent").toVCardString())


        val vCardNoFn =
            listOf("BEGIN:VCARD",
                "VERSION:4.0",
                "FN:",
                "NOTE:Card Object created without an FN-Property",
                "END:VCARD").joinToString("\r\n")

        assertEquals(
            vCardNoFn,
            VCard().property(Note("Card Object created without an FN-Property")).toVCardString()
        )

        assertNotEquals(
            VCard().property(Note("X")).property(Note("Y")).toVCardString(),
            VCard().property(Note("Y")).property(Note("X")).toVCardString()
        )

    }

    @Test
    fun toVCardStringFullCard() {

        val vCardStringArthur =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN;LANGUAGE=en;VALUE=text:Arthur Dent
                SOURCE:ldap://ldap.example.com/cn=Arthur%20Dent,%20o=BBC,%20c=UK
                SOURCE;VALUE=uri:https://directory.example.com/addressbooks/earth/Arthur%20Dent.vcf
                N;LANGUAGE=en:Dent;Arthur;Philip;;
                NICKNAME;LANGUAGE=en:Ape Man,Chimp Man,Dumb Dumb,Earthman,Monkey,Monkey Man,Primate,Semi-Evolved Simian
                RELATED;VALUE=uri:https://directory.example.com/addressbooks/earth/Tricia%20McMillan.vcf
                CATEGORIES:human,male
                PHOTO;PREF=42;ALTID=profile:https://www.example.com/pub/photos/arthur_dent.png
                PHOTO;PREF=23;ALTID=profile:data:image/jpeg;base64,MIICajCCAdOgAwIBAgICBEUwDQYJKoZIhvAQEEBQAwdzELMAkGA1UEBhMCVVMxLDAqBgNVBAoTI05ldHNjYXBlIENvbW11bmljYXRpb25zIENvcnBvcm
                LOGO:https://www.example.com/pub/logos/hitchhiker.png
                SOUND;PID=47.11,8,15:https://www.example.com/pub/sound/arthur_dent.ogg
                URL:https://www.example.com/
                ADR:;;155 Country Lane;Cottington;Cottingshire County;;United Kingdom
                GEO:geo:51.935163,0.033072
                EMAIL;TYPE=home:Arthur Dent <arthur.dent@example.com>
                IMPP:xmpp:arthur.dent@example.com
                TITLE:The Hitchhiker
                ROLE:Main Protagonist
                ORG:BBC
                NOTE:Along with Ford Prefect\, Arthur Dent barely escapes the Earth's destruction as it is demolished to make way for a hyperspace bypass.
                END:VCARD
            """.trimIndent().replace("\n", "\r\n")

        val vCardArthur = VCard()
            .property(Fn("Arthur Dent")
                .parameter(Language("en"))
                .parameter(Value(Value.Type.TEXT)))
            .property(Source("ldap://ldap.example.com/cn=Arthur%20Dent,%20o=BBC,%20c=UK"))
            .property(Source("https://directory.example.com/addressbooks/earth/Arthur%20Dent.vcf")
                .parameter(Value("uri")))
            .property(N("Dent", "Arthur", "Philip")
                .parameter(Language("en")))
            .property(Nickname("Ape Man", "Chimp Man", "Dumb Dumb", "Earthman", "Monkey", "Monkey Man", "Primate", "Semi-Evolved Simian")
                .parameter(Language("en")))
            .property(
                Related("https://directory.example.com/addressbooks/earth/Tricia%20McMillan.vcf")
                .parameter(Value("uri")))
            .property(Categories("human", "male"))
            .property(Photo("https://www.example.com/pub/photos/arthur_dent.png")
                .parameter(Pref(42))
                .parameter(AltId("profile")))
            .property(Photo("data:image/jpeg;base64,MIICajCCAdOgAwIBAgICBEUwDQYJKoZIhvAQEEBQAwdzELMAkGA1UEBhMCVVMxLDAqBgNVBAoTI05ldHNjYXBlIENvbW11bmljYXRpb25zIENvcnBvcm")
                .parameter(Pref(23))
                .parameter(AltId("profile")))
            .property(Logo("https://www.example.com/pub/logos/hitchhiker.png"))
            .property(Sound("https://www.example.com/pub/sound/arthur_dent.ogg")
                .parameter(PId(Pair(47,11), Pair(8, null), Pair(15, null))))
            .property(Url("https://www.example.com/"))
            .property(Adr(streetAddress = "155 Country Lane", locality = "Cottington", region = "Cottingshire County", countryName = "United Kingdom"))
            .property(Geo(UriValue("geo", path = "51.935163,0.033072")))
            .property(Email("Arthur Dent <arthur.dent@example.com>")
                .parameter(Type("home")))
            .property(Impp("xmpp:arthur.dent@example.com"))
            .property(Title("The Hitchhiker"))
            .property(Role("Main Protagonist"))
            .property(Org("BBC"))
            .property(Note("Along with Ford Prefect, Arthur Dent barely escapes the Earth's destruction as it is demolished to make way for a hyperspace bypass."))

        val vCardStringTrillian =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Tricia McMillan
                SOURCE:https://directory.example.com/addressbooks/earth/Tricia%20McMillan.vcf
                N:McMillan;Tricia;;Dr.;
                NICKNAME:Trillian,Trillian Astra
                PHOTO:https://www.example.com/pub/photos/tricia_mcmillan.png
                TEL;TYPE=voice:tel:2079460347
                EMAIL:"Tricia McMillan" tricia.mcmillan@example.com
                IMPP;PID=42:xmpp:tricia.mcmillan@example.com
                IMPP;PID=3:xmpp:trillian@example.com
                TITLE:The Girlfriend
                ROLE:Wife of the President of the Algolian Chapter of the Galactic Rotary Club
                NOTE:Tricia McMillan\, usually known as Trillian and sometimes Trillian Astra\, was a human astrophysicist and mathematician whom Arthur Dent completely failed to chat up at a party at a flat in Islington.
                END:VCARD
            """.trimIndent().replace("\n", "\r\n")


        val vCardTrillian = VCard("Tricia McMillan")
            .property(Source("https://directory.example.com/addressbooks/earth/Tricia%20McMillan.vcf"))
            .property(N("McMillan", "Tricia", honorificPrefix = "Dr."))
            .property(Nickname("Trillian", "Trillian Astra"))
            .property(Photo("https://www.example.com/pub/photos/tricia_mcmillan.png"))
            .property(Tel("tel:2079460347")
                .parameter(Type(Type.Value.VOICE)))
            .property(Email("\"Tricia McMillan\" tricia.mcmillan@example.com"))
            .property(Impp("xmpp:tricia.mcmillan@example.com")
                .parameter(PId(42)))
            .property(Impp("xmpp:trillian@example.com")
                .parameter(PId(3)))
            .property(Title("The Girlfriend"))
            .property(Role("Wife of the President of the Algolian Chapter of the Galactic Rotary Club"))
            .property(Note("Tricia McMillan, usually known as Trillian and sometimes Trillian Astra, was a human astrophysicist and mathematician whom Arthur Dent completely failed to chat up at a party at a flat in Islington."))

        assertEquals(vCardStringArthur, vCardArthur.toVCardString())
        assertEquals(vCardStringTrillian, vCardTrillian.toVCardString())

        assertNotEquals(vCardStringArthur, vCardTrillian.toVCardString())
        assertNotEquals(vCardStringTrillian, vCardArthur.toVCardString())
    }

    @Test
    fun toVCardStringCardWithNote() {

        val vCardString1 =
            listOf("BEGIN:VCARD",
                "VERSION:4.0",
                "FN:Slartibartfast",
                "NOTE:Slartibartfast is a designer of planets.",
                "END:VCARD").joinToString("\r\n")

        val vCard1 = VCard("Slartibartfast")
        vCard1.property(Note("Slartibartfast is a designer of planets."))

        assertEquals(vCardString1, vCard1.toVCardString())
    }

    @Test
    fun toVCardStringPropertyValueEscaping() {

        val vCardString =
            listOf("BEGIN:VCARD",
                "VERSION:4.0",
                "FN:Sample with Backslash and newlines",
                "NOTE:The backslash: \\\\ and some newline:\\nand here\\, a semicolon: \\;",
                "END:VCARD").joinToString("\r\n")

        val vCard = VCard("Sample with Backslash and newlines")
        vCard.property(Note("The backslash: \\ and some newline:\nand here, a semicolon: ;"))

        assertEquals(vCardString, vCard.toVCardString())

    }

    @Test
    fun testEquals() {

        val vCardA1 = VCard("A")
        val vCardA2 = VCard("A")
        val vCardA3 = VCard("A")
        val vCardB = VCard("B")

        assertFalse(vCardA1.equals("A"))
        assertFalse(vCardA1.equals(null))
        assertNotEquals(vCardB, vCardA1)

        assertEquals(vCardA2, vCardA1)

        vCardA1.property(Note("sample note"))
        vCardA2.property(Note("sample note"))
        vCardA3.property(Note("something completely different"))

        assertEquals(vCardA1, vCardA1)
        assertEquals(vCardA2, vCardA1)
        assertNotEquals(vCardA3, vCardA1)

        assertEquals(VCard(""), VCard())
        assertEquals(VCard().property(Note("X")).property(Note("Y")), VCard().property(Note("Y")).property(Note("X")))

    }

    @Test
    fun testHashCode() {
        val vCard1 = VCard("test")
        val vCard2 = VCard("test 2")
        val vCard3 = VCard("test")
        vCard1.property(Note("some text"))
        vCard2.property(Note("some text"))
        vCard3.property(Note("some text"))

        assertEquals(vCard1.hashCode(), vCard1.hashCode())
        assertEquals(vCard1.hashCode(), vCard3.hashCode())
        assertEquals(vCard3.hashCode(), vCard1.hashCode())

        assertNotEquals(vCard1.hashCode(), vCard2.hashCode())
        assertNotEquals(vCard2.hashCode(), vCard1.hashCode())

        vCard3.property(Note("some other text"))

        assertNotEquals(vCard1.hashCode(), vCard2.hashCode())
        assertNotEquals(vCard1.hashCode(), vCard3.hashCode())
        assertNotEquals(vCard2.hashCode(), vCard1.hashCode())
        assertNotEquals(vCard2.hashCode(), vCard3.hashCode())
        assertNotEquals(vCard3.hashCode(), vCard1.hashCode())
        assertNotEquals(vCard3.hashCode(), vCard2.hashCode())

        assertEquals(VCard("").hashCode(), VCard().hashCode())
        assertEquals(
            VCard().property(Note("X")).property(Note("Y")).hashCode(),
            VCard().property(Note("Y")).property(Note("X")).hashCode()
        )

    }

    @Test
    fun add() {
        val vCard = VCard("test")

        vCard.property(Note("some text"))
        assertTrue(vCard.toVCardString().contains("NOTE:some text"))

        vCard.property(Note("some other text"))
        assertTrue(vCard.toVCardString().contains("NOTE:some other text"))

        val vCardN = VCard("Zaphod Beeblebrox")
            .property(N("Beeblebrox", "Zaphod", honorificPrefix = "President of the Galaxy (formerly)"))

        val vCardDoubleN = VCard("Zaphod Beeblebrox")
            .property(N("Beeblebrox", honorificPrefix = "President of the Galaxy"))
            .property(N(givenName = "Zaphod", honorificPrefix = "President of the Galaxy (formerly)"))

        assertEquals(vCardN, vCardDoubleN)

    }

    @Test
    fun get() {

        val vCard = VCard("Tricia McMillan")

        assertTrue(vCard.get(Impp::class).isEmpty())

        vCard.property(Impp("xmpp:tricia.mcmillan@example.com"))

        assertFalse(vCard.get(Impp::class).isEmpty())
        assertEquals(1, vCard.get(Impp::class).size)

        vCard.property(Impp("xmpp:trillian@example.com"))

        assertFalse(vCard.get(Impp::class).isEmpty())
        assertEquals(2, vCard.get(Impp::class).size)
        assertEquals(Impp("xmpp:tricia.mcmillan@example.com"), vCard.get(Impp::class).first())
        assertEquals(Impp("xmpp:trillian@example.com"), vCard.get(Impp::class)[1])

    }

    @Test
    fun has() {

        val vCard = VCard("Zaphod Beeblebrox")

        assertTrue(vCard.has(Fn::class))
        assertFalse(vCard.has(N::class))

        vCard.property(N("Beeblebrox", "Zaphod", honorificPrefix = "President of the Galaxy"))

        assertTrue(vCard.has(Fn::class))
        assertTrue(vCard.has(N::class))

    }

    @Test
    fun getName() {

        val vCardDan = VCard("Dan Streetmentioner")
        vCardDan.property(N("Streetmentioner", "Dan", honorificPrefix = "Dr."))
        val vCardArthur = VCard("Arthur Dent")
        vCardArthur.property(N("Dent", "Arthur", "Philip"))
        val vCardFord = VCard("Ford Prefect")
        vCardFord.property(N(givenName = "福特", familyName = "派法特"))
        val vCardTricia = VCard("Tricia McMillan")
        val vCardLig = VCard("Lig")
        vCardLig.property(N("Lury", "Lig", honorificSuffix = "Jr."))
        val vCardPaula = VCard("Paula")
        vCardPaula.property(N(TextListValue("Millstone", "Jennings"), TextListValue("Paula", "Nancy")))
        val vCardEmpty = VCard(null)

        assertEquals("", VCard().getName())
        assertEquals("", VCard("").getName())
        assertEquals("", VCard("").property(N("")).getName())

        assertEquals("Dr. Dan Streetmentioner", vCardDan.getName())
        assertEquals("Arthur Philip Dent", vCardArthur.getName(false))
        assertEquals("派法特 福特", vCardFord.getName(true))
        assertEquals("Tricia McMillan", vCardTricia.getName())
        assertEquals("Lig Lury Jr.", vCardLig.getName())
        assertEquals("Paula Nancy Millstone Jennings", vCardPaula.getName())
        assertEquals("", vCardEmpty.getName())

    }

}