/*
 * GNU LESSER GENERAL PUBLIC LICENSE
 * Copyright (C) 2021 - 2024 Daniel Sturm
 * mail@DanielSturm.de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.dstulle.vkard.exporter

import de.dstulle.vkard.VCard
import de.dstulle.vkard.export
import de.dstulle.vkard.properties.explanatory.Note
import de.dstulle.vkard.properties.identification.Fn
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertNotEquals

internal class R300vCardFormatSpecificationExportTest {

    @Test
    fun exportBasicCard() {

        val vCardString1 =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Arthur Dent
                END:VCARD
            """.trimIndent().replace("\n", "\r\n")

        val vCardString2 =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Slartibartfast
                END:VCARD
            """.trimIndent().replace("\n", "\r\n")

        val vCard1 = VCard("Arthur Dent")
        val vCard2 = VCard("Slartibartfast")

        assertEquals(vCardString1, export(vCard1))
        assertEquals(vCardString2, export(vCard2))

        assertNotEquals(vCardString2, export(vCard1))
        assertNotEquals(vCardString1, export(vCard2))

    }

    @Test
    fun exportLinebreaks() {

        val vCardStringCRLF =
            listOf("BEGIN:VCARD",
                "VERSION:4.0",
                "FN:Arthur Dent",
                "END:VCARD").joinToString("\r\n")

        val vCardStringLF =
            listOf("BEGIN:VCARD",
                "VERSION:4.0",
                "FN:Arthur Dent",
                "END:VCARD").joinToString("\n")

        val vCardStringCR =
            listOf("BEGIN:VCARD",
                "VERSION:4.0",
                "FN:Arthur Dent",
                "END:VCARD").joinToString("\r")

        val vCardStringMixedLinebreaks =
            "BEGIN:VCARD\r\n" +
                    "VERSION:4.0\r" +
                    "FN:Arthur Dent\n" +
                    "END:VCARD"

        val vCard = VCard("Arthur Dent")

        assertEquals(vCardStringCRLF, export(vCard))
        assertNotEquals(vCardStringLF, export(vCard))
        assertNotEquals(vCardStringCR, export(vCard))
        assertNotEquals(vCardStringMixedLinebreaks, export(vCard))

    }

    @Test
    fun exportCardWithAttributeGroup() {

        val vCardString1 =
            """
                BEGIN:VCARD
                VERSION:4.0
                HU-MAN.FN:Arthur Dent
                END:VCARD
            """.trimIndent().replace("\n", "\r\n")

        val vCardString2 =
            """
                BEGIN:VCARD
                VERSION:4.0
                MAGRATHEAN.FN:Slartibartfast
                END:VCARD
            """.trimIndent().replace("\n", "\r\n")

        val vCard1 = VCard()
        vCard1.property(Fn("Arthur Dent").group("Hu-man"))
        val vCard2 = VCard()
        vCard2.property(Fn("Slartibartfast").group("MaGraTheAn"))

        assertEquals(vCardString1, export(vCard1))
        assertEquals(vCardString2, export(vCard2))

    }

    @Test
    fun exportCardPropertyCase() {

        val vCardStringUppercase =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Arthur Dent
                END:VCARD
            """.trimIndent().replace("\n", "\r\n")

        val vCard = VCard("Arthur Dent")

        assertEquals(vCardStringUppercase, export(vCard))

    }

    @Test
    fun exportCardPropertyValueEscaping() {

        val vCardString1 =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Slartibartfast
                NOTE:Slartibartfast is a Magrathean\, and a designer of planets.
                END:VCARD
            """.trimIndent().replace("\n", "\r\n")

        val vCardString2 =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Arthur Dent
                NOTE:Along with Ford Prefect\, Arthur Dent barely escapes the Earth's destr
                 uction as it is demolished to make way for a hyperspace bypass.
                END:VCARD
            """.trimIndent().replace("\n", "\r\n")

        val vCardString3 =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Ship of the Vogon Constructor Fleet
                NOTE:They are not crewed exclusively by Vogons\; a species known as the Den
                 trassi are responsible for on-board catering.
                END:VCARD
            """.trimIndent().replace("\n", "\r\n")

        val vCardString4 =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Sample with Backslash and newlines
                NOTE:The backslash: \\ and some newline:\nand here\, a semicolon: \;
                END:VCARD
            """.trimIndent().replace("\n", "\r\n")

        val vCard1 = VCard("Slartibartfast")
        vCard1.property(Note("Slartibartfast is a Magrathean, and a designer of planets."))
        val vCard2 = VCard("Arthur Dent")
        vCard2.property(Note("Along with Ford Prefect, Arthur Dent barely escapes the Earth's destruction as it is demolished to make way for a hyperspace bypass."))
        val vCard3 = VCard("Ship of the Vogon Constructor Fleet")
        vCard3.property(Note("They are not crewed exclusively by Vogons; a species known as the Dentrassi are responsible for on-board catering."))
        val vCard4 = VCard("Sample with Backslash and newlines")
        vCard4.property(Note("The backslash: \\ and some newline:\nand here, a semicolon: ;"))

        assertEquals(vCardString1, export(vCard1))
        assertEquals(vCardString2, export(vCard2))
        assertEquals(vCardString3, export(vCard3))
        assertEquals(vCardString4, export(vCard4))

    }

    @Test
    fun exportCardLineFolding() {

        val vCardString75 =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Arthur Dent
                NOTE:Along with Ford Prefect\, Arthur Dent barely escapes the Earth's destr
                 uction as it is demolished to make way for a hyperspace bypass.
                END:VCARD
            """.trimIndent().replace("\n", "\r\n")

        val vCardString10 =
            """
                BEGIN:VC
                 ARD
                VERSION:
                 4.0
                FN:Slart
                 ibartfa
                 st
                NOTE:Sla
                 rtibart
                 fast is
                  a Magr
                 athean\
                 , and a
                  design
                 er of p
                 lanets.
                END:VCAR
                 D
            """.trimIndent().replace("\n", "\r\n")

        val vCardString75UTF8 =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:福特·派法特
                NOTE:福特·派法特是一位宇宙漫游员，来自参宿，受雇于
                 银河系漫游指南，负责从银河系四处搜集资料，编辑银
                 河系漫游指南，但他已经被困于地球15年，直到一天，渥
                 罡人受命负责摧毁地球，用作兴建一条超空间快速通道
                 ，他借机搭上便车，同时救走了他的地球朋友亚瑟·丹特
                END:VCARD
            """.trimIndent().replace("\n", "\r\n")

        val vCardArthur = VCard("Arthur Dent")
        vCardArthur.property(Note("Along with Ford Prefect, Arthur Dent barely escapes the Earth's destruction as it is demolished to make way for a hyperspace bypass."))
        val vCardSlartibartfast = VCard("Slartibartfast")
        vCardSlartibartfast.property(Note("Slartibartfast is a Magrathean, and a designer of planets."))
        val vCardFordUTF8 = VCard("福特·派法特")
        vCardFordUTF8.property(Note("福特·派法特是一位宇宙漫游员，来自参宿，受雇于银河系漫游指南，负责从银河系四处搜集资料，编辑银河系漫游指南，但他已经被困于地球15年，直到一天，渥罡人受命负责摧毁地球，用作兴建一条超空间快速通道，他借机搭上便车，同时救走了他的地球朋友亚瑟·丹特"))

        assertEquals(vCardString75, export(vCardArthur))
        assertEquals(vCardString10, export(vCardSlartibartfast, 8))
        assertEquals(vCardString75UTF8, export(vCardFordUTF8, 73))

        assertFailsWith<IllegalArgumentException> {
            export(vCardArthur, 1)
        }
        assertFailsWith<IllegalArgumentException> {
            export(vCardSlartibartfast, 0)
        }
        assertFailsWith<IllegalArgumentException> {
            export(vCardFordUTF8, -1)
        }
    }

}