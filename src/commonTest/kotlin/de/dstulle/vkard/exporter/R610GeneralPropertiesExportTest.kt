/*
 * GNU LESSER GENERAL PUBLIC LICENSE
 * Copyright (C) 2021 - 2024 Daniel Sturm
 * mail@DanielSturm.de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.dstulle.vkard.exporter

import de.dstulle.vkard.VCard
import de.dstulle.vkard.export
import de.dstulle.vkard.properties.general.Source
import de.dstulle.vkard.property.value.UriValue
import kotlin.test.Test
import kotlin.test.assertEquals

internal class R610GeneralPropertiesExportTest {

    @Test
    fun exportBegin() {

        val vCardStringWithBegin =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Arthur Dent
                END:VCARD
            """.trimIndent().replace("\n", "\r\n")

        val vCard = VCard("Arthur Dent")

        assertEquals(vCardStringWithBegin, export(vCard))

    }

    @Test
    fun exportEnd() {

        val vCardStringWithEnd =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Arthur Dent
                END:VCARD
            """.trimIndent().replace("\n", "\r\n")

        val vCard = VCard("Arthur Dent")

        assertEquals(vCardStringWithEnd, export(vCard))

    }

    @Test
    fun exportSource() {

        val vCardStringLdapSource =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Arthur Dent
                SOURCE:ldap://ldap.example.com/cn=Arthur%20Dent,%20o=BBC,%20c=UK
                END:VCARD
            """.trimIndent().replace("\n", "\r\n")

        val vCardStringHttpSource =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Arthur Dent
                SOURCE:https://directory.example.com/addressbooks/earth/Arthur%20Dent.vcf
                END:VCARD
            """.trimIndent().replace("\n", "\r\n")

        val vCardStringLdapAndHttpSource =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Arthur Dent
                SOURCE:ldap://ldap.example.com/cn=Arthur%20Dent,%20o=BBC,%20c=UK
                SOURCE:https://directory.example.com/addressbooks/earth/Arthur%20Dent.vcf
                END:VCARD
            """.trimIndent().replace("\n", "\r\n")

        val vCardStringMultipleUriVariants =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Arthur Dent
                SOURCE:https://john.doe@www.example.com:123/forum/questions/?tag=networking
                 &order=newest#top
                SOURCE:ldap://[2001:db8::7]/c=GB?objectClass?one
                SOURCE:mailto:John.Doe@example.com
                SOURCE:news:comp.infosystems.www.servers.unix
                SOURCE:tel:+1-816-555-1212
                SOURCE:telnet://192.0.2.16:80/
                SOURCE:urn:oasis:names:specification:docbook:dtd:xml:4.1.2
                END:VCARD
            """.trimIndent().replace("\n", "\r\n")

        val vCardLdapSource = VCard("Arthur Dent")
        vCardLdapSource.property(Source(UriValue("ldap", host = "ldap.example.com", path = "/cn=Arthur%20Dent,%20o=BBC,%20c=UK")))
        val vCardHttpSource = VCard("Arthur Dent")
        vCardHttpSource.property(Source(UriValue("https", host = "directory.example.com", path = "/addressbooks/earth/Arthur%20Dent.vcf")))
        val vCardLdapAndHttpSource = VCard("Arthur Dent")
        vCardLdapAndHttpSource.property(Source(UriValue("ldap", host = "ldap.example.com", path = "/cn=Arthur%20Dent,%20o=BBC,%20c=UK")))
        vCardLdapAndHttpSource.property(Source(UriValue("https", host = "directory.example.com", path = "/addressbooks/earth/Arthur%20Dent.vcf")))
        val vCardMultipleUriVariants = VCard("Arthur Dent")
        vCardMultipleUriVariants.property(Source(UriValue(scheme = "https", userinfo = "john.doe", host = "www.example.com", port = "123", path = "/forum/questions/", query = "tag=networking&order=newest", fragment = "top")))
        vCardMultipleUriVariants.property(Source(UriValue(scheme = "ldap", host = "[2001:db8::7]", path = "/c=GB", query = "objectClass?one")))
        vCardMultipleUriVariants.property(Source(UriValue(scheme = "mailto", path = "John.Doe@example.com")))
        vCardMultipleUriVariants.property(Source(UriValue(scheme = "news", path = "comp.infosystems.www.servers.unix")))
        vCardMultipleUriVariants.property(Source(UriValue(scheme = "tel", path = "+1-816-555-1212")))
        vCardMultipleUriVariants.property(Source(UriValue(scheme = "telnet", host = "192.0.2.16", port = "80", path = "/")))
        vCardMultipleUriVariants.property(Source(UriValue(scheme = "urn", path = "oasis:names:specification:docbook:dtd:xml:4.1.2")))

        assertEquals(vCardStringLdapSource, export(vCardLdapSource))
        assertEquals(vCardStringHttpSource, export(vCardHttpSource))
        assertEquals(vCardStringLdapAndHttpSource, export(vCardLdapAndHttpSource))
        assertEquals(vCardStringMultipleUriVariants, export(vCardMultipleUriVariants))

    }

}