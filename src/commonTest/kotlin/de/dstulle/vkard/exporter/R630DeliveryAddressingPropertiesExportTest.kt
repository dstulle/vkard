/*
 * GNU LESSER GENERAL PUBLIC LICENSE
 * Copyright (C) 2021 - 2024 Daniel Sturm
 * mail@DanielSturm.de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.dstulle.vkard.exporter

import de.dstulle.vkard.VCard
import de.dstulle.vkard.export
import de.dstulle.vkard.properties.addressing.Adr
import de.dstulle.vkard.property.value.TextListValue
import kotlin.test.Test
import kotlin.test.assertEquals

internal class R630DeliveryAddressingPropertiesExportTest {

    @Test
    fun exportAdr() {

        val vCardStringArthur =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Arthur Dent
                ADR:;;155 Country Lane;Cottington;Cottingshire County;;United Kingdom
                END:VCARD
            """.trimIndent().replace("\n", "\r\n")

        val vCardStringFord =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Ford Prefect
                ADR:;;;Guildford;South East England;GU1;United Kingdom
                ADR:;;;;Betelgeuse VII,Betelgeuse V;;Betelgeuse
                END:VCARD
            """.trimIndent().replace("\n", "\r\n")

        val vCardStringExotic =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Something completely different
                ADR:\;;\,;;;;an\nna;
                END:VCARD
            """.trimIndent().replace("\n", "\r\n")

        val vCardStringEmptyAdr =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Zaphod Beeblebrox
                ADR:;;;;;;
                END:VCARD
            """.trimIndent().replace("\n", "\r\n")

        val vCardArthur = VCard("Arthur Dent")
        vCardArthur.property(Adr(
            streetAddress = "155 Country Lane",
            locality = "Cottington",
            region = "Cottingshire County",
            countryName = "United Kingdom"
        ))
        val vCardFord = VCard("Ford Prefect")
        vCardFord.property(Adr(
            locality = "Guildford",
            region = "South East England",
            postalCode = "GU1",
            countryName = "United Kingdom"
        ))
        vCardFord.property(Adr(
            region = TextListValue("Betelgeuse VII", "Betelgeuse V"),
            countryName = TextListValue("Betelgeuse")
        ))
        val vCardExotic = VCard("Something completely different")
        vCardExotic.property(Adr(";", ",", postalCode = "an\nna"))
        val vCardEmptyAdr = VCard("Zaphod Beeblebrox")
        vCardEmptyAdr.property(Adr(listOf()))

        assertEquals(vCardStringArthur, export(vCardArthur))
        assertEquals(vCardStringFord, export(vCardFord))
        assertEquals(vCardStringExotic, export(vCardExotic))
        assertEquals(vCardStringEmptyAdr, export(vCardEmptyAdr))

    }

}