/*
 * GNU LESSER GENERAL PUBLIC LICENSE
 * Copyright (C) 2021 - 2024 Daniel Sturm
 * mail@DanielSturm.de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.dstulle.vkard.exporter

import de.dstulle.vkard.VCard
import de.dstulle.vkard.export
import de.dstulle.vkard.properties.organizational.Logo
import de.dstulle.vkard.properties.organizational.Org
import de.dstulle.vkard.properties.organizational.Role
import de.dstulle.vkard.properties.organizational.Title
import de.dstulle.vkard.property.value.UriValue
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals

internal class R660OrganizationalPropertiesExportTest {

    @Test
    fun exportTitle() {

        val vCardStringZaphod =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Zaphod Beeblebrox
                TITLE:President of the Galaxy
                END:VCARD
            """.trimIndent().replace("\n", "\r\n")

        val vCardStringProstetnic =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Prostetnic Vogon Jeltz
                TITLE:Civil Servant
                TITLE:Commander of the Vogon Constructor Fleet
                END:VCARD
            """.trimIndent().replace("\n", "\r\n")

        val vCardZaphod = VCard("Zaphod Beeblebrox")
        vCardZaphod.property(Title("President of the Galaxy"))
        val vCardProstetnic = VCard("Prostetnic Vogon Jeltz")
        vCardProstetnic.property(Title("Civil Servant"))
        vCardProstetnic.property(Title("Commander of the Vogon Constructor Fleet"))

        assertEquals(vCardStringZaphod, export(vCardZaphod))
        assertEquals(vCardStringProstetnic, export(vCardProstetnic))

        assertNotEquals(vCardStringZaphod, export(vCardProstetnic))
        assertNotEquals(vCardStringProstetnic, export(vCardZaphod))

    }

    @Test
    fun exportRole() {

        val vCardStringSlartibartfast =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Slartibartfast
                ROLE:designer of planets
                END:VCARD
            """.trimIndent().replace("\n", "\r\n")

        val vCardStringFord =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Ford Prefect
                ROLE:friend of Arthur Dent
                ROLE:semi-cousin of The President of the Galaxy\, Zaphod Beeblebrox
                ROLE:out-of-work actor
                ROLE:researcher for The Hitchhiker's Guide to the Galaxy
                END:VCARD
            """.trimIndent().replace("\n", "\r\n")

        val vCardSlartibartfast = VCard("Slartibartfast")
        vCardSlartibartfast.property(Role("designer of planets"))
        val vCardFord = VCard("Ford Prefect")
        vCardFord.property(Role("friend of Arthur Dent"))
        vCardFord.property(Role("semi-cousin of The President of the Galaxy, Zaphod Beeblebrox"))
        vCardFord.property(Role("out-of-work actor"))
        vCardFord.property(Role("researcher for The Hitchhiker's Guide to the Galaxy"))

        assertEquals(vCardStringSlartibartfast, export(vCardSlartibartfast))
        assertEquals(vCardStringFord, export(vCardFord))

        assertNotEquals(vCardStringSlartibartfast, export(vCardFord))
        assertNotEquals(vCardStringFord, export(vCardSlartibartfast))

    }

    @Test
    fun exportLogo() {

        val vCardStringArthur =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Arthur Dent
                LOGO:https://www.example.com/dont_panic.png
                END:VCARD
            """.trimIndent().replace("\n", "\r\n")

        val vCardStringTrillian =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Tricia McMillan
                LOGO:https://www.example.com/dont_panic.png
                LOGO:data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAdCAYAAADLnm6HAAAE
                 NklEQVRIia1WXWhcRRSe1JqotdA2VYsKilZBQZy5d3+an8qqNRJBtA/74IMiKIVYEVFQnHPvMt
                 g2NQRa3Le+tBSp6D5on3wx58zuZhsUS6FIhLoK/oFCrEqbJrY2Oz7s3jD37r2bTdOBgcvMN+f7
                 7jkz3wxjq2xDE7WNjqf3cklfcUl/CaBFAXiJA/4mgD5xlR42xvSsKqjrU5ZL/arjl3eOFut9sR
                 hPP88lznOJprtOtdxhvWlFcgH4xfIiwMtcohGAlxzQbxpjeoZU7U4OdLF74nAXkk50FBCQ2mqz
                 B6bu4ICfXStpTDYuJGWWCaBfuEQTHXeA3lkrsZB4lQMuBd/5QzM3x5XgRS7RDCi9fZm8QE+vmR
                 zoY4vju1Y5FmI3KJfUEEA/MMbYaLHeZwVZTAMOKmPW5UuzvRzwcDfkrk+v2fGNMT1WTGwT4Ho4
                 1lqYFUDftMDzcWozPj3aidzxyY0rNZfUCDCD+/D+dgDgeS7xnABabG2choDyhxxIppXeZmMH1M
                 yWoLZ2TxXKT8WSQ/lAWultAujLVuxf20A5pTel/IrIKb019s9AT9r4kcmzG+y/4hJNWuldNma0
                 WO8Tnn7FHhOS6lyiefbI6VvaRAip32WMsZRHzyQcp0M2fvjg9OZmyQIh1BiZPLuBMcZ2KH2v61
                 M2ypH29K7WXngrNJEvzfZyiSblVwRjjAmgQpyIJJt1pAbr+P25c7x6WxxOAP3Uwp0LTWTU9INB
                 gJxqmpLw6HjbDgd6Iy4wY4wN7sd7uETDgf6OzuVLs71C4j+WQ14JAVwPR6yj8t+eI6dvbCk+GR
                 WRL832JolwVdXhEk3WqzwQjGWVvptLei/l00tWORuhhakCPRd2MVrIl8wNjDHGJX4eMZnfkwQ0
                 8VTjEo1TKO8WHo6FxVQfDuKEBahqJs7DlTHrGIstx0dJAp4cn+oPcAP78a7ovJB0hQMuhQaHD0
                 5vTjCXeaX1esYYE6D3dbMhl0kkmtwxfZM9HpSIA56PSV2CwwFeDs5tCvCFbgQ0Hyxh03F82hN3
                 V9gCfky2WWoE6XTfrzziejiWRM4YYxzoqL2p7RPAJZodXuWhtkVuofz4SheN4+ETnYitDHzaIa
                 NLiQuD2iX1tJq+rxsBAvD7pBiup99OXJiUBSHx6oCa2dINeSsDjSQBwsMPOquXWI2c+8Xobu7U
                 bFeN9gGltwuguRUfrAJoLvD1wAu6bcEzr/3PmzfjoDp1u5BU7xik+YrRM6shDtrQRG1j9EQ5nt
                 5rY7ikbzPjU/3XEj+xOR4qIelE4A8ZpVNc0gVXVZ0oVgA+Jjw6ft3IXUlF2zMc0K+n/XKaS/yX
                 A5ZySt8awit0BNDcdSHngMesdP/hejgW7BvH1y8LSQuBrQugkxzoKAdcEhJn10yutF6fGf+6v9
                 MVzRhjOaW3cqAJIfFnDnSRA54ZLdb7/gfFKnNHPZRW4AAAAABJRU5ErkJggg==
                END:VCARD
            """.trimIndent().replace("\n", "\r\n")

        val vCardArthur = VCard("Arthur Dent")
        vCardArthur.property(Logo(UriValue("https", host = "www.example.com", path = "/dont_panic.png")))
        val vCardTrillian = VCard("Tricia McMillan")
        vCardTrillian.property(Logo(UriValue("https", host = "www.example.com", path = "/dont_panic.png")))
        vCardTrillian.property(Logo(
            UriValue("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAdCAYAAADLnm6HAAAENklEQVRIia1WXWhcRRSe1JqotdA2VYsKilZBQZy5d3+an8qqNRJBtA/74IMiKIVYEVFQnHPvMtg2NQRa3Le+tBSp6D5on3wx58zuZhsUS6FIhLoK/oFCrEqbJrY2Oz7s3jD37r2bTdOBgcvMN+f77jkz3wxjq2xDE7WNjqf3cklfcUl/CaBFAXiJA/4mgD5xlR42xvSsKqjrU5ZL/arjl3eOFut9sRhPP88lznOJprtOtdxhvWlFcgH4xfIiwMtcohGAlxzQbxpjeoZU7U4OdLF74nAXkk50FBCQ2mqzB6bu4ICfXStpTDYuJGWWCaBfuEQTHXeA3lkrsZB4lQMuBd/5QzM3x5XgRS7RDCi9fZm8QE+vmRzoY4vju1Y5FmI3KJfUEEA/MMbYaLHeZwVZTAMOKmPW5UuzvRzwcDfkrk+v2fGNMT1WTGwT4Ho41lqYFUDftMDzcWozPj3aidzxyY0rNZfUCDCD+/D+dgDgeS7xnABabG2choDyhxxIppXeZmMH1MyWoLZ2TxXKT8WSQ/lAWultAujLVuxf20A5pTel/IrIKb019s9AT9r4kcmzG+y/4hJNWuldNma0WO8Tnn7FHhOS6lyiefbI6VvaRAip32WMsZRHzyQcp0M2fvjg9OZmyQIh1BiZPLuBMcZ2KH2v61M2ypH29K7WXngrNJEvzfZyiSblVwRjjAmgQpyIJJt1pAbr+P25c7x6WxxOAP3Uwp0LTWTU9INBgJxqmpLw6HjbDgd6Iy4wY4wN7sd7uETDgf6OzuVLs71C4j+WQ14JAVwPR6yj8t+eI6dvbCk+GRWRL832JolwVdXhEk3WqzwQjGWVvptLei/l00tWORuhhakCPRd2MVrIl8wNjDHGJX4eMZnfkwQ08VTjEo1TKO8WHo6FxVQfDuKEBahqJs7DlTHrGIstx0dJAp4cn+oPcAP78a7ovJB0hQMuhQaHD05vTjCXeaX1esYYE6D3dbMhl0kkmtwxfZM9HpSIA56PSV2CwwFeDs5tCvCFbgQ0Hyxh03F82hN3V9gCfky2WWoE6XTfrzziejiWRM4YYxzoqL2p7RPAJZodXuWhtkVuofz4SheN4+ETnYitDHzaIaNLiQuD2iX1tJq+rxsBAvD7pBiup99OXJiUBSHx6oCa2dINeSsDjSQBwsMPOquXWI2c+8Xobu7UbFeN9gGltwuguRUfrAJoLvD1wAu6bcEzr/3PmzfjoDp1u5BU7xik+YrRM6shDtrQRG1j9EQ5nt5rY7ikbzPjU/3XEj+xOR4qIelE4A8ZpVNc0gVXVZ0oVgA+Jjw6ft3IXUlF2zMc0K+n/XKaS/yXA5ZySt8awit0BNDcdSHngMesdP/hejgW7BvH1y8LSQuBrQugkxzoKAdcEhJn10yutF6fGf+6v9MVzRhjOaW3cqAJIfFnDnSRA54ZLdb7/gfFKnNHPZRW4AAAAABJRU5ErkJggg==")
        ))

        assertEquals(vCardStringArthur, export(vCardArthur))
        assertEquals(vCardStringTrillian, export(vCardTrillian))

    }

    @Test
    fun exportOrg() {

        val vCardStringArthur =
            """
            BEGIN:VCARD
            VERSION:4.0
            FN:Arthur Dent
            ORG:
            END:VCARD
        """.trimIndent().replace("\n", "\r\n")

        val vCardStringZaphod =
            """
            BEGIN:VCARD
            VERSION:4.0
            FN:Zaphod Beeblebrox
            ORG:;Galactic Empire;
            END:VCARD
        """.trimIndent().replace("\n", "\r\n")

        val vCardStringProstetnic =
            """
            BEGIN:VCARD
            VERSION:4.0
            FN:Prostetnic Vogon Jeltz
            ORG:Vogon Civil Service;Vogon Constructor Fleet;Vogon flagship
            END:VCARD
        """.trimIndent().replace("\n", "\r\n")

        val vCardArthur = VCard("Arthur Dent")
        vCardArthur.property(Org())
        val vCardZaphod = VCard("Zaphod Beeblebrox")
        vCardZaphod.property(Org("", "Galactic Empire", ""))
        val vCardProstetnic = VCard("Prostetnic Vogon Jeltz")
        vCardProstetnic.property(Org("Vogon Civil Service", "Vogon Constructor Fleet", "Vogon flagship"))

        assertEquals(vCardStringArthur, export(vCardArthur))
        assertEquals(vCardStringZaphod, export(vCardZaphod))
        assertEquals(vCardStringProstetnic, export(vCardProstetnic))

        assertNotEquals(vCardStringZaphod, export(vCardProstetnic))
        assertNotEquals(vCardStringProstetnic, export(vCardZaphod))

    }

}