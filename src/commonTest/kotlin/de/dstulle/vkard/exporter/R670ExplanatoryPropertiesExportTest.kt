/*
 * GNU LESSER GENERAL PUBLIC LICENSE
 * Copyright (C) 2021 - 2024 Daniel Sturm
 * mail@DanielSturm.de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.dstulle.vkard.exporter

import de.dstulle.vkard.VCard
import de.dstulle.vkard.export
import de.dstulle.vkard.properties.explanatory.Categories
import de.dstulle.vkard.properties.explanatory.Note
import de.dstulle.vkard.properties.explanatory.Sound
import de.dstulle.vkard.properties.explanatory.Url
import de.dstulle.vkard.property.value.UriValue
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals

internal class R670ExplanatoryPropertiesExportTest {

    @Test
    fun exportCategories() {

        val vCardStringArthur =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Arthur Dent
                CATEGORIES:human,male
                END:VCARD
            """.trimIndent().replace("\n", "\r\n")

        val vCardStringFord =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Ford Prefect
                CATEGORIES:human,male
                CATEGORIES:alien
                END:VCARD
            """.trimIndent().replace("\n", "\r\n")

        val vCardArthur = VCard("Arthur Dent")
        vCardArthur.property(Categories("human", "male"))
        val vCardFord = VCard("Ford Prefect")
        vCardFord.property(Categories("human", "male"))
        vCardFord.property(Categories("alien"))

        assertEquals(vCardStringArthur, export(vCardArthur))
        assertEquals(vCardStringFord, export(vCardFord))

    }

    @Test
    fun exportCardWithNote() {

        val vCardString1 =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Slartibartfast
                NOTE:Slartibartfast is a designer of planets.
                END:VCARD
            """.trimIndent().replace("\n", "\r\n")

        val vCardString2 =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Arthur Dent
                NOTE:Arthur Dent barely escapes the Earth's destruction as it is demolished
                  to make way for a hyperspace bypass.
                END:VCARD
            """.trimIndent().replace("\n", "\r\n")

        val vCard1 = VCard("Slartibartfast")
        vCard1.property(Note("Slartibartfast is a designer of planets."))
        val vCard2 = VCard("Arthur Dent")
        vCard2.property(Note("Arthur Dent barely escapes the Earth's destruction as it is demolished to make way for a hyperspace bypass."))

        assertEquals(vCardString1, export(vCard1))
        assertEquals(vCardString2, export(vCard2))

        assertNotEquals(vCardString2, export(vCard1))
        assertNotEquals(vCardString1, export(vCard2))

    }

    @Test
    fun exportSound() {

        val vCardStringArthur =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Arthur Dent
                SOUND:cid:arthur.dent@example.com
                END:VCARD
            """.trimIndent().replace("\n", "\r\n")

        val vCardStringTrillian =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Tricia McMillan
                SOUND:cid:tricia.mcmillan@example.com
                SOUND:mid:trillian@example.com
                END:VCARD
            """.trimIndent().replace("\n", "\r\n")

        val vCardArthur = VCard("Arthur Dent")
        vCardArthur.property(Sound(UriValue("cid", path = "arthur.dent@example.com")))
        val vCardTrillian = VCard("Tricia McMillan")
        vCardTrillian.property(Sound(UriValue("cid", path = "tricia.mcmillan@example.com")))
        vCardTrillian.property(Sound(UriValue("mid", path = "trillian@example.com")))

        assertEquals(vCardStringArthur, export(vCardArthur))
        assertEquals(vCardStringTrillian, export(vCardTrillian))

    }

    @Test
    fun exportUrl() {

        val vCardStringArthur =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Arthur Dent
                URL:https://arthur.example.com
                END:VCARD
            """.trimIndent().replace("\n", "\r\n")

        val vCardStringTrillian =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Tricia McMillan
                URL:https://tricia.example.com
                URL:https://trillian.example.com
                END:VCARD
            """.trimIndent().replace("\n", "\r\n")

        val vCardArthur = VCard("Arthur Dent")
        vCardArthur.property(Url(UriValue("https", host = "arthur.example.com", path = "")))
        val vCardTrillian = VCard("Tricia McMillan")
        vCardTrillian.property(Url(UriValue("https", host = "tricia.example.com", path = "")))
        vCardTrillian.property(Url (UriValue("https", host = "trillian.example.com", path = "")))

        assertEquals(vCardStringArthur, export(vCardArthur))
        assertEquals(vCardStringTrillian, export(vCardTrillian))

    }

    @Test
    fun exportVersion() {

        val vCardString21 =
            """
                BEGIN:VCARD
                VERSION:2.1
                FN:Arthur Dent
                END:VCARD
            """.trimIndent().replace("\n", "\r\n")

        val vCardString30 =
            """
                BEGIN:VCARD
                VERSION:3.0
                FN:Arthur Dent
                END:VCARD
            """.trimIndent().replace("\n", "\r\n")

        val vCardString40 =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Arthur Dent
                END:VCARD
            """.trimIndent().replace("\n", "\r\n")

        val vCard = VCard("Arthur Dent")

        assertNotEquals(vCardString21, export(vCard))
        assertNotEquals(vCardString30, export(vCard))
        assertEquals(vCardString40, export(vCard))

    }

}