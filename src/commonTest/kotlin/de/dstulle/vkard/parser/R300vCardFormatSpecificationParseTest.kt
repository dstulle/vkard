/*
 * GNU LESSER GENERAL PUBLIC LICENSE
 * Copyright (C) 2021 - 2024 Daniel Sturm
 * mail@DanielSturm.de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.dstulle.vkard.parser

import de.dstulle.vkard.VCard
import de.dstulle.vkard.parse
import de.dstulle.vkard.properties.explanatory.Note
import de.dstulle.vkard.properties.identification.Fn
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals

internal class R300vCardFormatSpecificationParseTest {

    @Test
    fun parseBasicCard() {

        val vCardString1 =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Arthur Dent
                END:VCARD
            """.trimIndent()

        val vCardString2 =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Slartibartfast
                END:VCARD
            """.trimIndent()


        val vCardStringNoBeginOrEnd =
            """
                VERSION:4.0
                FN:Arthur Dent
            """.trimIndent()

        val vCardStringJustFn =
            """
                FN:Arthur Dent
            """.trimIndent()

        val vCardStringName = "Arthur Dent"

        val vCard1 = VCard("Arthur Dent")
        val vCard2 = VCard("Slartibartfast")

        assertEquals(vCard1, parse(vCardString1))
        assertEquals(vCard2, parse(vCardString2))
        assertEquals(vCard1, parse(vCardStringNoBeginOrEnd))
        assertEquals(vCard1, parse(vCardStringJustFn))
        assertEquals(vCard1, parse(vCardStringName))

        assertNotEquals(vCard1, parse(vCardString2))
        assertNotEquals(vCard2, parse(vCardString1))

    }

    @Test
    fun parseLinebreaks() {

        val vCardStringCRLF =
            listOf("BEGIN:VCARD",
                "VERSION:4.0",
                "FN:Arthur Dent",
                "END:VCARD").joinToString("\r\n")

        val vCardStringLF =
            listOf("BEGIN:VCARD",
                "VERSION:4.0",
                "FN:Arthur Dent",
                "END:VCARD").joinToString("\n")

        val vCardStringCR =
            listOf("BEGIN:VCARD",
                "VERSION:4.0",
                "FN:Arthur Dent",
                "END:VCARD").joinToString("\r")

        val vCardStringMixedLinebreaks =
            "BEGIN:VCARD\r\n" +
                "VERSION:4.0\r" +
                "FN:Arthur Dent\n" +
                "END:VCARD"

        val vCard = VCard("Arthur Dent")

        assertEquals(vCard, parse(vCardStringCRLF))
        assertEquals(vCard, parse(vCardStringLF))
        assertEquals(vCard, parse(vCardStringCR))
        assertEquals(vCard, parse(vCardStringMixedLinebreaks))

    }

    @Test
    fun parseCardWithAttributeGroup() {

        val vCardString1 =
            """
                BEGIN:VCARD
                VERSION:4.0
                HU-Man.FN:Arthur Dent
                END:VCARD
            """.trimIndent()

        val vCardString2 =
            """
                BEGIN:VCARD
                VERSION:4.0
                Magrathean.FN:Slartibartfast
                END:VCARD
            """.trimIndent()

        val vCard1 = VCard()
        vCard1.property(Fn("Arthur Dent").group("Hu-man"))
        val vCard2 = VCard()
        vCard2.property(Fn("Slartibartfast").group("MaGraTheAn"))

        assertEquals(vCard1, parse(vCardString1))
        assertEquals(vCard2, parse(vCardString2))

    }

    @Test
    fun parseCardPropertyCase() {

        val vCardStringUppercase =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Arthur Dent
                END:VCARD
            """.trimIndent()

        val vCardStringLowercase =
            """
                begin:VCARD
                version:4.0
                fn:Arthur Dent
                end:VCARD
            """.trimIndent()

        val vCardStringCapitalized =
            """
                Begin:VCARD
                Version:4.0
                Fn:Arthur Dent
                End:VCARD
            """.trimIndent()

        val vCardStringRandomCase =
            """
                bEgIn:VCARD
                versION:4.0
                fn:Arthur Dent
                END:VCARD
            """.trimIndent()

        val vCard = VCard("Arthur Dent")

        assertEquals(vCard, parse(vCardStringUppercase))
        assertEquals(vCard, parse(vCardStringLowercase))
        assertEquals(vCard, parse(vCardStringCapitalized))
        assertEquals(vCard, parse(vCardStringRandomCase))

    }

    @Test
    fun parseCardPropertyValueEscaping() {

        val vCardString1 =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Slartibartfast
                NOTE:Slartibartfast is a Magrathean\, and a designer of planets.
                END:VCARD
            """.trimIndent()

        val vCardString2 =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Arthur Dent
                NOTE:Along with Ford Prefect\, Arthur Dent barely escapes the Earth's 
                 destruction as it is demolished to make way for a hyperspace bypass.
                END:VCARD
            """.trimIndent()

        val vCardString3 =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Ship of the Vogon Constructor Fleet
                NOTE:They are not crewed exclusively by Vogons\; a species known as the Den
                 trassi are responsible for on-board catering.
                END:VCARD
            """.trimIndent()

        val vCardString4 =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Sample with Backslash and newlines
                NOTE:The backslash: \\ and some newline:\neven Uppercase Newlines a
                 re possible:\Nand here\, a semicolon: \;
                END:VCARD
            """.trimIndent()

        val vCard1 = VCard("Slartibartfast")
        vCard1.property(Note("Slartibartfast is a Magrathean, and a designer of planets."))
        val vCard2 = VCard("Arthur Dent")
        vCard2.property(Note("Along with Ford Prefect, Arthur Dent barely escapes the Earth's destruction as it is demolished to make way for a hyperspace bypass."))
        val vCard3 = VCard("Ship of the Vogon Constructor Fleet")
        vCard3.property(Note("They are not crewed exclusively by Vogons; a species known as the Dentrassi are responsible for on-board catering."))
        val vCard4 = VCard("Sample with Backslash and newlines")
        vCard4.property(Note("The backslash: \\ and some newline:\neven Uppercase Newlines are possible:\nand here, a semicolon: ;"))

        assertEquals(vCard1, parse(vCardString1))
        assertEquals(vCard2, parse(vCardString2))
        assertEquals(vCard3, parse(vCardString3))
        assertEquals(vCard4, parse(vCardString4))

    }

    @Test
    fun parseCardPropertyFoldedLines() {

        val vCardStringWhitespace =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Arthur Dent
                NOTE:Along with Ford Prefect\, Arthur Dent barely escapes the Earth's 
                 destruction as it is demolished to make way for a hyperspace bypass.
                END:VCARD
            """.trimIndent()

        val vCardStringTab =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Arthur Dent
                NOTE:Along with Ford Prefect\, Arthur Dent barely escapes the Earth's 
                	destruction as it is demolished to make way for a hyperspace bypass.
                END:VCARD
            """.trimIndent()

        val vCard = VCard("Arthur Dent")
        vCard.property(Note("Along with Ford Prefect, Arthur Dent barely escapes the Earth's destruction as it is demolished to make way for a hyperspace bypass."))

        assertEquals(vCard, parse(vCardStringWhitespace))
        assertEquals(vCard, parse(vCardStringTab))

    }

}