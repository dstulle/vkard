/*
 * GNU LESSER GENERAL PUBLIC LICENSE
 * Copyright (C) 2021 - 2024 Daniel Sturm
 * mail@DanielSturm.de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.dstulle.vkard.parser

import de.dstulle.vkard.VCard
import de.dstulle.vkard.parse
import de.dstulle.vkard.properties.communications.Tel
import de.dstulle.vkard.properties.identification.Fn
import de.dstulle.vkard.properties.identification.N
import de.dstulle.vkard.properties.identification.Photo
import de.dstulle.vkard.properties.organizational.Related
import de.dstulle.vkard.property.parameter.*
import de.dstulle.vkard.property.parameter.Type.Value.*
import de.dstulle.vkard.property.parameter.Value.Type.TEXT
import de.dstulle.vkard.property.parameter.Value.Type.URI
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals

internal class R500PropertyParametersParseTest {

    @Test
    fun parseInvalidParameter() {

        val vCardString1 =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN;INVALID=param;NON=existing:Arthur Dent
                END:VCARD
            """.trimIndent()


        val vCardString2 =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN;:Arthur Dent
                END:VCARD
            """.trimIndent()

        val vCardArthur = VCard("Arthur Dent")

        assertEquals(vCardArthur, parse(vCardString1))
        assertEquals(vCardArthur, parse(vCardString2))

    }

    @Test
    fun parseLanguageParameter() {

        val vCardStringArthurEn =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN;LANGUAGE=en;INVALID=param:Arthur Dent
                END:VCARD
            """.trimIndent()

        val vCardStringFordZh =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN;LANGUAGE=zh:福特·派法特
                END:VCARD
            """.trimIndent()

        val vCardArthurEn = VCard().property(Fn("Arthur Dent").parameter(Language("en")))
        val vCardFordEn = VCard().property(Fn("福特·派法特").parameter(Language("zh")))

        assertEquals(vCardArthurEn, parse(vCardStringArthurEn))
        assertEquals(vCardFordEn, parse(vCardStringFordZh))

    }


    @Test
    fun parseValueParameter() {

        val vCardStringTrillianUri =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Tricia McMillan
                TEL;VALUE=uri:tel:2079460347
                END:VCARD
            """.trimIndent()

        val vCardStringTrillianText =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Tricia McMillan
                TEL;VALUE=text:2079460347
                END:VCARD
            """.trimIndent()

        val vCardTrillianUri = VCard("Tricia McMillan")
            .property(Tel("tel:2079460347")
                .parameter(Value(URI))
            )
        val vCardTrillianText = VCard("Tricia McMillan")
            .property(Tel("tel:2079460347")
                .parameter(Value(TEXT))
            )

        assertEquals(vCardTrillianUri, parse(vCardStringTrillianUri))
        assertEquals(vCardTrillianText, parse(vCardStringTrillianText))

    }


    @Test
    fun parsePrefParameter() {

        val vCardStringArthur =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Arthur Dent
                PHOTO;PREF=42:https://www.example.com/pub/photos/arthur_dent.png
                PHOTO;PREF=23:data:image/jpeg;base64,MIICajCCAdOgAwIBAgICBEUwDQYJKoZIhvAQEEBQAwdzELMAkGA1UEBhMCVVMxLDAqBgNVBAoTI05ldHNjYXBlIENvbW11bmljYXRpb25zIENvcnBvcm
                END:VCARD
            """.trimIndent()

        val vCardArthurPref = VCard("Arthur Dent")
            .property(Photo("https://www.example.com/pub/photos/arthur_dent.png")
                .parameter(Pref(42)))
            .property(Photo("data:image/jpeg;base64,MIICajCCAdOgAwIBAgICBEUwDQYJKoZIhvAQEEBQAwdzELMAkGA1UEBhMCVVMxLDAqBgNVBAoTI05ldHNjYXBlIENvbW11bmljYXRpb25zIENvcnBvcm")
                .parameter(Pref(23)))

        val vCardArthurNoPref = VCard("Arthur Dent")
            .property(Photo("https://www.example.com/pub/photos/arthur_dent.png"))
            .property(Photo("data:image/jpeg;base64,MIICajCCAdOgAwIBAgICBEUwDQYJKoZIhvAQEEBQAwdzELMAkGA1UEBhMCVVMxLDAqBgNVBAoTI05ldHNjYXBlIENvbW11bmljYXRpb25zIENvcnBvcm"))

        assertEquals(vCardArthurPref, parse(vCardStringArthur))

        assertNotEquals(vCardArthurNoPref, parse(vCardStringArthur))

    }


    @Test
    fun parseAltIdParameter() {

        val vCardStringFordAltId =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Fort Prefect
                N;LANGUAGE=en;ALTID=1:Prefect;Fort;;;
                N;LANGUAGE=zh;ALTID=1:派法特;福特;;;
                END:VCARD
            """.trimIndent()

        val vCardStringFordNoAltId =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Fort Prefect
                N;LANGUAGE=en:Prefect;Fort;;;
                N;LANGUAGE=zh:派法特;福特;;;
                END:VCARD
            """.trimIndent()

        val vCardFordAltId = VCard("Fort Prefect")
            .property(N("Prefect","Fort")
                .parameter(Language("en"))
                .parameter(AltId("1")))
            .property(N("派法特","福特")
                .parameter(Language("zh"))
                .parameter(AltId("1")))

        val vCardFordNoAltId = VCard("Fort Prefect")
            .property(N("派法特","福特")
                .parameter(Language("zh")))

        assertEquals(vCardFordAltId, parse(vCardStringFordAltId))
        assertEquals(vCardFordNoAltId, parse(vCardStringFordNoAltId))

        assertNotEquals(vCardFordNoAltId, parse(vCardStringFordAltId))

    }

    @Test
    fun parsePIdParameter() {

        val vCardStringArthur =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Arthur Dent
                PHOTO;PID=42:https://www.example.com/pub/photos/arthur_dent.png
                PHOTO;PID=8,15:data:image/jpeg;base64,MIICajCCAdOgAwIBAgICBEUwDQYJKoZIhvAQE
                 EBQAwdzELMAkGA1UEBhMCVVMxLDAqBgNVBAoTI05ldHNjYXBlIENvbW11bmljYXRpb25zIENvc
                 nBvcm
                END:VCARD
            """.trimIndent()

        val vCardArthurPId = VCard("Arthur Dent")
            .property(Photo("https://www.example.com/pub/photos/arthur_dent.png")
                .parameter(PId(42)))
            .property(Photo("data:image/jpeg;base64,MIICajCCAdOgAwIBAgICBEUwDQYJKoZIhvAQEEBQAwdzELMAkGA1UEBhMCVVMxLDAqBgNVBAoTI05ldHNjYXBlIENvbW11bmljYXRpb25zIENvcnBvcm")
                .parameter(PId(Pair(8, null), Pair(15, null))))

        val vCardArthurNoPId = VCard("Arthur Dent")
            .property(Photo("https://www.example.com/pub/photos/arthur_dent.png"))
            .property(Photo("data:image/jpeg;base64,MIICajCCAdOgAwIBAgICBEUwDQYJKoZIhvAQEEBQAwdzELMAkGA1UEBhMCVVMxLDAqBgNVBAoTI05ldHNjYXBlIENvbW11bmljYXRpb25zIENvcnBvcm"))

        assertEquals(vCardArthurPId, parse(vCardStringArthur))

        assertNotEquals(vCardArthurNoPId, parse(vCardStringArthur))

    }

    @Test
    fun parseTypeParameter() {

        val vCardStringTrillian =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN;TYPE=home,video:Tricia McMillan
                N;TYPE=home:McMillan;Tricia
                TEL;TYPE=voice,me:tel:2079460347
                RELATED;TYPE=cell,contact:urn:uuid:f81d4fae-7dec-11d0-a765-00a0c91e6bf6
                END:VCARD
            """.trimIndent().replace("\n", "\r\n")

        val vCardTrillianType = VCard()
            .property(Fn("Tricia McMillan")
                .parameter(Type(HOME)))
            .property(N("McMillan", "Tricia"))
            .property(Tel("tel:2079460347")
                .parameter(Type(VOICE)))
            .property(Related("urn:uuid:f81d4fae-7dec-11d0-a765-00a0c91e6bf6")
                .parameter(Type(CONTACT)))

        val vCardTrillianNoType = VCard("Tricia McMillan")
            .property(Fn("Tricia McMillan"))
            .property(N("McMillan", "Tricia"))
            .property(Tel("tel:2079460347"))
            .property(Related("urn:uuid:f81d4fae-7dec-11d0-a765-00a0c91e6bf6"))

        assertEquals(vCardTrillianType, parse(vCardStringTrillian))

        assertNotEquals(vCardTrillianNoType, parse(vCardStringTrillian))

    }

}