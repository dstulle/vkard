/*
 * GNU LESSER GENERAL PUBLIC LICENSE
 * Copyright (C) 2021 - 2024 Daniel Sturm
 * mail@DanielSturm.de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.dstulle.vkard.parser

import de.dstulle.vkard.VCard
import de.dstulle.vkard.parse
import de.dstulle.vkard.properties.identification.Fn
import de.dstulle.vkard.properties.identification.N
import de.dstulle.vkard.properties.identification.Nickname
import de.dstulle.vkard.properties.identification.Photo
import de.dstulle.vkard.property.value.TextListValue
import de.dstulle.vkard.property.value.UriValue
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals

internal class R620IdentificationPropertiesParseTest {

    @Test
    fun parseFn() {

        val vCardString1 =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Arthur Dent
                END:VCARD
            """.trimIndent()

        val vCardString2a =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Tricia McMillan
                FN:Trillian
                END:VCARD
            """.trimIndent()

        val vCardString2b =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Trillian
                FN:Tricia McMillan
                END:VCARD
            """.trimIndent()

        val vCard1 = VCard("Arthur Dent")
        val vCard2a = VCard("Tricia McMillan")
        vCard2a.property(Fn("Trillian"))
        val vCard2b = VCard("Trillian")
        vCard2b.property(Fn("Tricia McMillan"))

        assertEquals(vCard1, parse(vCardString1))
        assertEquals(vCard2a, parse(vCardString2a))
        assertEquals(vCard2b, parse(vCardString2b))

        assertEquals(vCard2a, parse(vCardString2b))
        assertEquals(vCard2b, parse(vCardString2a))

        assertNotEquals(vCard1, parse(vCardString2a))
        assertNotEquals(vCard1, parse(vCardString2b))

    }

    @Test
    fun parseN() {

        val vCardStringDan =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Dan Streetmentioner
                N:Streetmentioner;Dan;;Dr.;
                END:VCARD
            """.trimIndent()

        val vCardStringArthur =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Arthur Dent
                N:Dent;Arthur;Philip;
                END:VCARD
            """.trimIndent()

        val vCardStringLig =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Lig Lury Jr.
                N:Lury;Lig;;;Jr.
                END:VCARD
            """.trimIndent()

        val vCardStringZaphod =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Zaphod Beeblebrox
                N:Beeblebrox;Zaphod;
                END:VCARD
            """.trimIndent()

        val vCardStringPaula =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Paula Nancy Millstone Jennings
                N:Millstone,Jennings;Paula,Nancy;;;
                END:VCARD
            """.trimIndent()

        val vCardStringExotic =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Something completely different
                N:;\;;\,;;\;
                END:VCARD
            """.trimIndent()

        val vCardStringDoubleN =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Zaphod Beeblebrox
                N:Beeblebrox;;;President of the Galaxy;
                N:;Zaphod;;President of the Galaxy (formerly);
                END:VCARD
            """.trimIndent()

        val vCardStringEmptyN =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Zaphod Beeblebrox
                N:
                END:VCARD
            """.trimIndent()

        val vCardDan = VCard("Dan Streetmentioner")
        vCardDan.property(N("Streetmentioner", "Dan", honorificPrefix = "Dr."))
        val vCardArthur = VCard("Arthur Dent")
        vCardArthur.property(N("Dent", "Arthur", "Philip"))
        val vCardLig = VCard("Lig Lury Jr.")
        vCardLig.property(N("Lury", "Lig", honorificSuffix = "Jr."))
        val vCardZaphod = VCard("Zaphod Beeblebrox")
        vCardZaphod.property(N("Beeblebrox", "Zaphod"))
        val vCardPaula = VCard("Paula Nancy Millstone Jennings")
        vCardPaula.property(N(TextListValue("Millstone", "Jennings"), TextListValue("Paula", "Nancy")))
        val vCardExotic = VCard("Something completely different")
        vCardExotic.property(N(givenName = ";", additionalName = ",", honorificSuffix = ";"))
        val vCardDoubleN = VCard("Zaphod Beeblebrox")
        vCardDoubleN.property(N("Beeblebrox", "Zaphod", honorificPrefix = "President of the Galaxy (formerly)"))
        val vCardEmptyN = VCard("Zaphod Beeblebrox")
        vCardEmptyN.property(N(listOf()))

        assertEquals(vCardDan, parse(vCardStringDan))
        assertEquals(vCardArthur, parse(vCardStringArthur))
        assertEquals(vCardLig, parse(vCardStringLig))
        assertEquals(vCardZaphod, parse(vCardStringZaphod))
        assertEquals(vCardPaula, parse(vCardStringPaula))
        assertEquals(vCardExotic, parse(vCardStringExotic))
        assertEquals(vCardDoubleN, parse(vCardStringDoubleN))
        assertEquals(vCardEmptyN, parse(vCardStringEmptyN))

    }

    @Test
    fun parseNickname() {

        val vCardStringArthur =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Arthur Dent
                NICKNAME:Ape Man,Chimp Man,Dumb Dumb,Earthman,Monkey,Monkey Man,Primate,S
                 emi-Evolved Simian
                END:VCARD
            """.trimIndent()

        val vCardStringFord =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Ford Prefect
                NICKNAME:Dumb Space Cookie
                NICKNAME:Ix
                END:VCARD
            """.trimIndent()

        val vCardArthur = VCard("Arthur Dent")
        vCardArthur.property(Nickname("Ape Man", "Chimp Man", "Dumb Dumb", "Earthman", "Monkey", "Monkey Man", "Primate", "Semi-Evolved Simian"))
        val vCardFord = VCard("Ford Prefect")
        vCardFord.property(Nickname("Dumb Space Cookie"))
        vCardFord.property(Nickname("Ix"))

        assertEquals(vCardArthur, parse(vCardStringArthur))
        assertEquals(vCardFord, parse(vCardStringFord))

    }

    @Test
    fun parsePhoto() {

        val vCardStringArthur =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Arthur Dent
                PHOTO:https://www.example.com/pub/photos/arthur_dent.png
                END:VCARD
            """.trimIndent()

        val vCardStringFord =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Ford Prefect
                PHOTO:data:image/jpeg;base64,MIICajCCAdOgAwIBAgICBEUwDQYJKoZIhvAQEEBQAwdz
                 ELMAkGA1UEBhMCVVMxLDAqBgNVBAoTI05ldHNjYXBlIENvbW11bmljYXRpb25zIENvcnBvcm
                 F0aW9uMRwwGgYDVQQLExNJbmZvcm1hdGlvbiBTeXN0
                END:VCARD
            """.trimIndent()

        val vCardArthur = VCard("Arthur Dent")
        vCardArthur.property(Photo(UriValue(scheme = "https", host = "www.example.com", path = "/pub/photos/arthur_dent.png")))
        val vCardFord = VCard("Ford Prefect")
        vCardFord.property(Photo(UriValue(scheme = "data", path = "image/jpeg;base64,MIICajCCAdOgAwIBAgICBEUwDQYJKoZIhvAQEEBQAwdzELMAkGA1UEBhMCVVMxLDAqBgNVBAoTI05ldHNjYXBlIENvbW11bmljYXRpb25zIENvcnBvcmF0aW9uMRwwGgYDVQQLExNJbmZvcm1hdGlvbiBTeXN0")))

        assertEquals(vCardArthur, parse(vCardStringArthur))
        assertEquals(vCardFord, parse(vCardStringFord))

    }

}