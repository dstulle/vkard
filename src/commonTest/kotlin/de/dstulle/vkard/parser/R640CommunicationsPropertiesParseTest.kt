/*
 * GNU LESSER GENERAL PUBLIC LICENSE
 * Copyright (C) 2021 - 2024 Daniel Sturm
 * mail@DanielSturm.de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.dstulle.vkard.parser

import de.dstulle.vkard.VCard
import de.dstulle.vkard.parse
import de.dstulle.vkard.properties.communications.Email
import de.dstulle.vkard.properties.communications.Impp
import de.dstulle.vkard.properties.communications.Tel
import de.dstulle.vkard.property.value.UriValue
import kotlin.test.Test
import kotlin.test.assertEquals

internal class R640CommunicationsPropertiesParseTest {

    @Test
    fun parseTel() {

        val vCardStringTrillianUri =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Tricia McMillan
                TEL:tel:2079460347
                END:VCARD
            """.trimIndent()

        val vCardStringTrillianNoUri =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Tricia McMillan
                TEL:2079460347
                END:VCARD
            """.trimIndent()

        val vCardTrillianUri = VCard("Tricia McMillan")
        vCardTrillianUri.property(Tel("tel:2079460347"))
        val vCardTrillianNoUri = VCard("Tricia McMillan")
        vCardTrillianNoUri.property(Tel("2079460347"))

        assertEquals(vCardTrillianUri, vCardTrillianNoUri)

        assertEquals(vCardTrillianUri, parse(vCardStringTrillianUri))
        assertEquals(vCardTrillianNoUri, parse(vCardStringTrillianNoUri))

        assertEquals(vCardTrillianUri, parse(vCardStringTrillianNoUri))
        assertEquals(vCardTrillianNoUri, parse(vCardStringTrillianUri))

    }

    @Test
    fun parseEmail() {

        val vCardStringArthur =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Arthur Dent
                EMAIL:Arthur Dent <arthur.dent@example.com>
                END:VCARD
            """.trimIndent()

        val vCardStringTrillian =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Tricia McMillan
                EMAIL:"Tricia McMillan" tricia.mcmillan@example.com
                END:VCARD
            """.trimIndent()

        val vCardArthur = VCard("Arthur Dent")
        vCardArthur.property(Email("Arthur Dent <arthur.dent@example.com>"))
        val vCardTrillian = VCard("Tricia McMillan")
        vCardTrillian.property(Email("\"Tricia McMillan\" tricia.mcmillan@example.com"))

        assertEquals(vCardArthur, parse(vCardStringArthur))
        assertEquals(vCardTrillian, parse(vCardStringTrillian))

    }

    @Test
    fun parseImpp() {

        val vCardStringArthur =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Arthur Dent
                IMPP:xmpp:arthur.dent@example.com
                END:VCARD
            """.trimIndent()

        val vCardStringTrillian =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Tricia McMillan
                IMPP:xmpp:tricia.mcmillan@example.com
                IMPP:xmpp:trillian@example.com
                END:VCARD
            """.trimIndent()

        val vCardArthur = VCard("Arthur Dent")
        vCardArthur.property(Impp(UriValue("xmpp", path = "arthur.dent@example.com")))
        val vCardTrillian = VCard("Tricia McMillan")
        vCardTrillian.property(Impp(UriValue("xmpp", path = "tricia.mcmillan@example.com")))
        vCardTrillian.property(Impp(UriValue("xmpp", path = "trillian@example.com")))

        assertEquals(vCardArthur, parse(vCardStringArthur))
        assertEquals(vCardTrillian, parse(vCardStringTrillian))

    }

}