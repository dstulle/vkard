/*
 * GNU LESSER GENERAL PUBLIC LICENSE
 * Copyright (C) 2021 - 2024 Daniel Sturm
 * mail@DanielSturm.de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.dstulle.vkard.parser

import de.dstulle.vkard.VCard
import de.dstulle.vkard.parse
import de.dstulle.vkard.properties.organizational.*
import de.dstulle.vkard.property.parameter.Type
import de.dstulle.vkard.property.parameter.Type.Value.*
import de.dstulle.vkard.property.parameter.Value
import de.dstulle.vkard.property.parameter.Value.Type.TEXT
import de.dstulle.vkard.property.value.TextValue
import de.dstulle.vkard.property.value.UriValue
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals

internal class R660OrganizationalPropertiesParseTest {

    @Test
    fun parseTitle() {

        val vCardStringZaphod =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Zaphod Beeblebrox
                TITLE:President of the Galaxy
                END:VCARD
            """.trimIndent()

        val vCardStringProstetnic =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Prostetnic Vogon Jeltz
                TITLE:Civil Servant
                TITLE:Commander of the Vogon Constructor Fleet
                END:VCARD
            """.trimIndent()

        val vCardZaphod = VCard("Zaphod Beeblebrox")
        vCardZaphod.property(Title("President of the Galaxy"))
        val vCardProstetnic = VCard("Prostetnic Vogon Jeltz")
        vCardProstetnic.property(Title("Civil Servant"))
        vCardProstetnic.property(Title("Commander of the Vogon Constructor Fleet"))

        assertEquals(vCardZaphod, parse(vCardStringZaphod))
        assertEquals(vCardProstetnic, parse(vCardStringProstetnic))

        assertNotEquals(vCardZaphod, parse(vCardStringProstetnic))
        assertNotEquals(vCardProstetnic, parse(vCardStringZaphod))

    }

    @Test
    fun parseRole() {

        val vCardStringSlartibartfast =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Slartibartfast
                ROLE:designer of planets
                END:VCARD
            """.trimIndent()

        val vCardStringFord =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Ford Prefect
                ROLE:friend of Arthur Dent
                ROLE:semi-cousin of The President of the Galaxy\, Zaphod Beeblebrox
                ROLE:out-of-work actor
                ROLE:researcher for The Hitchhiker's Guide to the Galaxy
                END:VCARD
            """.trimIndent()

        val vCardSlartibartfast = VCard("Slartibartfast")
        vCardSlartibartfast.property(Role("designer of planets"))
        val vCardFord = VCard("Ford Prefect")
        vCardFord.property(Role("friend of Arthur Dent"))
        vCardFord.property(Role("semi-cousin of The President of the Galaxy, Zaphod Beeblebrox"))
        vCardFord.property(Role("out-of-work actor"))
        vCardFord.property(Role("researcher for The Hitchhiker's Guide to the Galaxy"))

        assertEquals(vCardSlartibartfast, parse(vCardStringSlartibartfast))
        assertEquals(vCardFord, parse(vCardStringFord))

        assertNotEquals(vCardSlartibartfast, parse(vCardStringFord))
        assertNotEquals(vCardFord, parse(vCardStringSlartibartfast))

    }

    @Test
    fun parseLogo() {

        val vCardStringArthur =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Arthur Dent
                LOGO:https://www.example.com/dont_panic.png
                END:VCARD
            """.trimIndent()

        val vCardStringTrillian =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Tricia McMillan
                LOGO:https://www.example.com/dont_panic.png
                LOGO:data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAdCAYAAADLnm6HAAAENklEQVRIia1WXWhcRRSe1JqotdA2VYsKilZBQZy5d3+an8qqNRJBtA/74IMiKIVYEVFQnHPvMtg2NQRa3Le+tBSp6D5on3wx58zuZhsUS6FIhLoK/oFCrEqbJrY2Oz7s3jD37r2bTdOBgcvMN+f77jkz3wxjq2xDE7WNjqf3cklfcUl/CaBFAXiJA/4mgD5xlR42xvSsKqjrU5ZL/arjl3eOFut9sRhPP88lznOJprtOtdxhvWlFcgH4xfIiwMtcohGAlxzQbxpjeoZU7U4OdLF74nAXkk50FBCQ2mqzB6bu4ICfXStpTDYuJGWWCaBfuEQTHXeA3lkrsZB4lQMuBd/5QzM3x5XgRS7RDCi9fZm8QE+vmRzoY4vju1Y5FmI3KJfUEEA/MMbYaLHeZwVZTAMOKmPW5UuzvRzwcDfkrk+v2fGNMT1WTGwT4Ho41lqYFUDftMDzcWozPj3aidzxyY0rNZfUCDCD+/D+dgDgeS7xnABabG2choDyhxxIppXeZmMH1MyWoLZ2TxXKT8WSQ/lAWultAujLVuxf20A5pTel/IrIKb019s9AT9r4kcmzG+y/4hJNWuldNma0WO8Tnn7FHhOS6lyiefbI6VvaRAip32WMsZRHzyQcp0M2fvjg9OZmyQIh1BiZPLuBMcZ2KH2v61M2ypH29K7WXngrNJEvzfZyiSblVwRjjAmgQpyIJJt1pAbr+P25c7x6WxxOAP3Uwp0LTWTU9INBgJxqmpLw6HjbDgd6Iy4wY4wN7sd7uETDgf6OzuVLs71C4j+WQ14JAVwPR6yj8t+eI6dvbCk+GRWRL832JolwVdXhEk3WqzwQjGWVvptLei/l00tWORuhhakCPRd2MVrIl8wNjDHGJX4eMZnfkwQ08VTjEo1TKO8WHo6FxVQfDuKEBahqJs7DlTHrGIstx0dJAp4cn+oPcAP78a7ovJB0hQMuhQaHD05vTjCXeaX1esYYE6D3dbMhl0kkmtwxfZM9HpSIA56PSV2CwwFeDs5tCvCFbgQ0Hyxh03F82hN3V9gCfky2WWoE6XTfrzziejiWRM4YYxzoqL2p7RPAJZodXuWhtkVuofz4SheN4+ETnYitDHzaIaNLiQuD2iX1tJq+rxsBAvD7pBiup99OXJiUBSHx6oCa2dINeSsDjSQBwsMPOquXWI2c+8Xobu7UbFeN9gGltwuguRUfrAJoLvD1wAu6bcEzr/3PmzfjoDp1u5BU7xik+YrRM6shDtrQRG1j9EQ5nt5rY7ikbzPjU/3XEj+xOR4qIelE4A8ZpVNc0gVXVZ0oVgA+Jjw6ft3IXUlF2zMc0K+n/XKaS/yXA5ZySt8awit0BNDcdSHngMesdP/hejgW7BvH1y8LSQuBrQugkxzoKAdcEhJn10yutF6fGf+6v9MVzRhjOaW3cqAJIfFnDnSRA54ZLdb7/gfFKnNHPZRW4AAAAABJRU5ErkJggg==
                END:VCARD
            """.trimIndent()

        val vCardArthur = VCard("Arthur Dent")
        vCardArthur.property(Logo(UriValue("https", host = "www.example.com", path = "/dont_panic.png")))
        val vCardTrillian = VCard("Tricia McMillan")
        vCardTrillian.property(Logo(UriValue("https", host = "www.example.com", path = "/dont_panic.png")))
        vCardTrillian.property(
            Logo(
                UriValue(
                    "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAdCAYAAADLnm6HAAAENklEQVRIia1WXWhcRRSe1JqotdA2VYsKilZBQZy5d3+an8qqNRJBtA/74IMiKIVYEVFQnHPvMtg2NQRa3Le+tBSp6D5on3wx58zuZhsUS6FIhLoK/oFCrEqbJrY2Oz7s3jD37r2bTdOBgcvMN+f77jkz3wxjq2xDE7WNjqf3cklfcUl/CaBFAXiJA/4mgD5xlR42xvSsKqjrU5ZL/arjl3eOFut9sRhPP88lznOJprtOtdxhvWlFcgH4xfIiwMtcohGAlxzQbxpjeoZU7U4OdLF74nAXkk50FBCQ2mqzB6bu4ICfXStpTDYuJGWWCaBfuEQTHXeA3lkrsZB4lQMuBd/5QzM3x5XgRS7RDCi9fZm8QE+vmRzoY4vju1Y5FmI3KJfUEEA/MMbYaLHeZwVZTAMOKmPW5UuzvRzwcDfkrk+v2fGNMT1WTGwT4Ho41lqYFUDftMDzcWozPj3aidzxyY0rNZfUCDCD+/D+dgDgeS7xnABabG2choDyhxxIppXeZmMH1MyWoLZ2TxXKT8WSQ/lAWultAujLVuxf20A5pTel/IrIKb019s9AT9r4kcmzG+y/4hJNWuldNma0WO8Tnn7FHhOS6lyiefbI6VvaRAip32WMsZRHzyQcp0M2fvjg9OZmyQIh1BiZPLuBMcZ2KH2v61M2ypH29K7WXngrNJEvzfZyiSblVwRjjAmgQpyIJJt1pAbr+P25c7x6WxxOAP3Uwp0LTWTU9INBgJxqmpLw6HjbDgd6Iy4wY4wN7sd7uETDgf6OzuVLs71C4j+WQ14JAVwPR6yj8t+eI6dvbCk+GRWRL832JolwVdXhEk3WqzwQjGWVvptLei/l00tWORuhhakCPRd2MVrIl8wNjDHGJX4eMZnfkwQ08VTjEo1TKO8WHo6FxVQfDuKEBahqJs7DlTHrGIstx0dJAp4cn+oPcAP78a7ovJB0hQMuhQaHD05vTjCXeaX1esYYE6D3dbMhl0kkmtwxfZM9HpSIA56PSV2CwwFeDs5tCvCFbgQ0Hyxh03F82hN3V9gCfky2WWoE6XTfrzziejiWRM4YYxzoqL2p7RPAJZodXuWhtkVuofz4SheN4+ETnYitDHzaIaNLiQuD2iX1tJq+rxsBAvD7pBiup99OXJiUBSHx6oCa2dINeSsDjSQBwsMPOquXWI2c+8Xobu7UbFeN9gGltwuguRUfrAJoLvD1wAu6bcEzr/3PmzfjoDp1u5BU7xik+YrRM6shDtrQRG1j9EQ5nt5rY7ikbzPjU/3XEj+xOR4qIelE4A8ZpVNc0gVXVZ0oVgA+Jjw6ft3IXUlF2zMc0K+n/XKaS/yXA5ZySt8awit0BNDcdSHngMesdP/hejgW7BvH1y8LSQuBrQugkxzoKAdcEhJn10yutF6fGf+6v9MVzRhjOaW3cqAJIfFnDnSRA54ZLdb7/gfFKnNHPZRW4AAAAABJRU5ErkJggg=="
                )
            )
        )

        assertEquals(vCardArthur, parse(vCardStringArthur))
        assertEquals(vCardTrillian, parse(vCardStringTrillian))

    }

    @Test
    fun parseOrg() {

        val vCardStringArthur =
            """
            BEGIN:VCARD
            VERSION:4.0
            FN:Arthur Dent
            ORG:
            END:VCARD
        """.trimIndent().replace("\n", "\r\n")

        val vCardStringZaphod =
            """
            BEGIN:VCARD
            VERSION:4.0
            FN:Zaphod Beeblebrox
            ORG:;Galactic Empire;
            END:VCARD
        """.trimIndent().replace("\n", "\r\n")

        val vCardStringProstetnic =
            """
            BEGIN:VCARD
            VERSION:4.0
            FN:Prostetnic Vogon Jeltz
            ORG:Vogon Civil Service;Vogon Constructor Fleet;Vogon flagship
            END:VCARD
        """.trimIndent().replace("\n", "\r\n")

        val vCardArthur = VCard("Arthur Dent")
        vCardArthur.property(Org())
        val vCardZaphod = VCard("Zaphod Beeblebrox")
        vCardZaphod.property(Org("Galactic Empire"))
        val vCardProstetnic = VCard("Prostetnic Vogon Jeltz")
        vCardProstetnic.property(Org("Vogon Civil Service", "Vogon Constructor Fleet", "Vogon flagship"))

        assertEquals(vCardArthur, parse(vCardStringArthur))
        assertEquals(vCardZaphod, parse(vCardStringZaphod))
        assertEquals(vCardProstetnic, parse(vCardStringProstetnic))

        assertNotEquals(vCardZaphod, parse(vCardStringProstetnic))
        assertNotEquals(vCardProstetnic, parse(vCardStringZaphod))

    }

    @Test
    fun parseRelated() {

        val vCardStringArthur =
            """
            BEGIN:VCARD
            VERSION:4.0
            FN:Arthur Dent
            RELATED;TYPE=acquaintance:https://directory.example.com/addressbooks/earth/Tricia%20McMillan.vcf
            END:VCARD
        """.trimIndent().replace("\n", "\r\n")

        val vCardStringZaphod =
            """
            BEGIN:VCARD
            VERSION:4.0
            FN:Zaphod Beeblebrox
            RELATED;TYPE=contact:urn:uuid:f81d4fae-7dec-11d0-a765-00a0c91e6bf6
            END:VCARD
        """.trimIndent().replace("\n", "\r\n")

        val vCardStringProstetnic =
            """
            BEGIN:VCARD
            VERSION:4.0
            FN:Prostetnic Vogon Jeltz
            RELATED;TYPE=co-worker;VALUE=text:Please contact my assistant for any inquiries.
            END:VCARD
        """.trimIndent().replace("\n", "\r\n")

        val vCardArthur = VCard("Arthur Dent")
        vCardArthur.property(
            Related("https://directory.example.com/addressbooks/earth/Tricia%20McMillan.vcf").parameter(
                Type(ACQUAINTANCE)
            )
        )
        val vCardZaphod = VCard("Zaphod Beeblebrox")
        vCardZaphod.property(Related("urn:uuid:f81d4fae-7dec-11d0-a765-00a0c91e6bf6").parameter(Type(CONTACT)))
        val vCardProstetnic = VCard("Prostetnic Vogon Jeltz")
        vCardProstetnic.property(
            Related(TextValue("Please contact my assistant for any inquiries.")).parameter(
                Type(
                    CO_WORKER
                )
            ).parameter(Value(TEXT))
        )

        assertEquals(vCardArthur, parse(vCardStringArthur))
        assertEquals(vCardZaphod, parse(vCardStringZaphod))
        assertEquals(vCardProstetnic, parse(vCardStringProstetnic))

        assertNotEquals(vCardZaphod, parse(vCardStringProstetnic))
        assertNotEquals(vCardProstetnic, parse(vCardStringZaphod))

    }

}