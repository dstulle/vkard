/*
 * GNU LESSER GENERAL PUBLIC LICENSE
 * Copyright (C) 2021 - 2024 Daniel Sturm
 * mail@DanielSturm.de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.dstulle.vkard.parser

import de.dstulle.vkard.VCard
import de.dstulle.vkard.parse
import de.dstulle.vkard.properties.explanatory.Categories
import de.dstulle.vkard.properties.explanatory.Note
import de.dstulle.vkard.properties.explanatory.Sound
import de.dstulle.vkard.properties.explanatory.Url
import de.dstulle.vkard.property.value.UriValue
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals

internal class R670ExplanatoryPropertiesParseTest {

    @Test
    fun parseCategories() {

        val vCardStringArthur =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Arthur Dent
                CATEGORIES:human,male
                END:VCARD
            """.trimIndent()

        val vCardStringFord =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Ford Prefect
                CATEGORIES:human,male
                CATEGORIES:alien
                END:VCARD
            """.trimIndent()

        val vCardArthur = VCard("Arthur Dent")
        vCardArthur.property(Categories("human", "male"))
        val vCardFord = VCard("Ford Prefect")
        vCardFord.property(Categories("human", "male"))
        vCardFord.property(Categories("alien"))

        assertEquals(vCardArthur, parse(vCardStringArthur))
        assertEquals(vCardFord, parse(vCardStringFord))

    }

    @Test
    fun parseCardWithNote() {

        val vCardString1 =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Slartibartfast
                NOTE:Slartibartfast is a designer of planets.
                END:VCARD
            """.trimIndent()

        val vCardString2 =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Arthur Dent
                NOTE:Arthur Dent barely escapes the Earth's destruction as it is demolished
                  to make way for a hyperspace bypass.
                END:VCARD
            """.trimIndent()

        val vCard1 = VCard("Slartibartfast")
        vCard1.property(Note("Slartibartfast is a designer of planets."))
        val vCard2 = VCard("Arthur Dent")
        vCard2.property(Note("Arthur Dent barely escapes the Earth's destruction as it is demolished to make way for a hyperspace bypass."))

        assertEquals(vCard1, parse(vCardString1))
        assertEquals(vCard2, parse(vCardString2))

        assertNotEquals(vCard1, parse(vCardString2))
        assertNotEquals(vCard2, parse(vCardString1))

    }

    @Test
    fun parseSound() {

        val vCardStringArthur =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Arthur Dent
                SOUND:cid:arthur.dent@example.com
                END:VCARD
            """.trimIndent()

        val vCardStringTrillian =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Tricia McMillan
                SOUND:cid:tricia.mcmillan@example.com
                SOUND:mid:trillian@example.com
                END:VCARD
            """.trimIndent()

        val vCardArthur = VCard("Arthur Dent")
        vCardArthur.property(Sound(UriValue("cid", path = "arthur.dent@example.com")))
        val vCardTrillian = VCard("Tricia McMillan")
        vCardTrillian.property(Sound(UriValue("cid", path = "tricia.mcmillan@example.com")))
        vCardTrillian.property(Sound(UriValue("mid", path = "trillian@example.com")))

        assertEquals(vCardArthur, parse(vCardStringArthur))
        assertEquals(vCardTrillian, parse(vCardStringTrillian))

    }

    @Test
    fun parseUrl() {

        val vCardStringArthur =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Arthur Dent
                URL:https://arthur.example.com
                END:VCARD
            """.trimIndent()

        val vCardStringTrillian =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Tricia McMillan
                URL:https://tricia.example.com
                URL:https://trillian.example.com
                END:VCARD
            """.trimIndent()

        val vCardArthur = VCard("Arthur Dent")
        vCardArthur.property(Url(UriValue("https", host = "arthur.example.com", path = "")))
        val vCardTrillian = VCard("Tricia McMillan")
        vCardTrillian.property(Url(UriValue("https", host = "tricia.example.com", path = "")))
        vCardTrillian.property(Url(UriValue("https", host = "trillian.example.com", path = "")))

        assertEquals(vCardArthur, parse(vCardStringArthur))
        assertEquals(vCardTrillian, parse(vCardStringTrillian))

    }

    @Test
    fun parseVersion() {

        val vCardString21 =
            """
                BEGIN:VCARD
                VERSION:2.1
                FN:Arthur Dent
                END:VCARD
            """.trimIndent()

        val vCardString30 =
            """
                BEGIN:VCARD
                VERSION:3.0
                FN:Arthur Dent
                END:VCARD
            """.trimIndent()

        val vCardString40 =
            """
                BEGIN:VCARD
                VERSION:4.0
                FN:Arthur Dent
                END:VCARD
            """.trimIndent()

        val vCardStringWrongVersion =
            """
                BEGIN:VCARD
                VERSION:5.1
                FN:Arthur Dent
                END:VCARD
            """.trimIndent()

        val vCardStringFaultyVersion =
            """
                BEGIN:VCARD
                VERSION:4-0
                FN:Arthur Dent
                END:VCARD
            """.trimIndent()

        val vCardStringNoVersion =
            """
                BEGIN:VCARD
                FN:Arthur Dent
                END:VCARD
            """.trimIndent()

        val vCard = VCard("Arthur Dent")

        assertEquals(vCard, parse(vCardString21))
        assertEquals(vCard, parse(vCardString30))
        assertEquals(vCard, parse(vCardString40))
        assertEquals(vCard, parse(vCardStringWrongVersion))
        assertEquals(vCard, parse(vCardStringFaultyVersion))
        assertEquals(vCard, parse(vCardStringNoVersion))

    }

}