/*
 * GNU LESSER GENERAL PUBLIC LICENSE
 * Copyright (C) 2024 Daniel Sturm
 * mail@DanielSturm.de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.dstulle.vkard.property

import kotlin.test.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class CardinalityTest {

    /** __1__  | Exactly one instance per vCard MUST be present. */
    @Test
    fun testExactlyOneMust() {
        assertTrue(Cardinality.EXACTLY_ONE_MUST.exactlyOne)
        assertTrue(Cardinality.EXACTLY_ONE_MUST.must)
    }

    /** __*1__ | Exactly one instance per vCard MAY be present. */
    @Test
    fun testExactlyOneMay() {
        assertTrue(Cardinality.EXACTLY_ONE_MAY.exactlyOne)
        assertFalse(Cardinality.EXACTLY_ONE_MAY.must)
    }

    /** __1*__ | One or more instances per vCard MUST be present. */
    @Test
    fun testOneOrMoreOneMust() {
        assertFalse(Cardinality.ONE_OR_MORE_MUST.exactlyOne)
        assertTrue(Cardinality.ONE_OR_MORE_MUST.must)
    }

    /** __*__ | One or more instances per vCard MAY be present. */
    @Test
    fun testOneOrMoreOneMay() {
        assertFalse(Cardinality.ONE_OR_MORE_MAY.exactlyOne)
        assertFalse(Cardinality.ONE_OR_MORE_MAY.must)
    }

}