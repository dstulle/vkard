/*
 * GNU LESSER GENERAL PUBLIC LICENSE
 * Copyright (C) 2021 - 2024 Daniel Sturm
 * mail@DanielSturm.de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.dstulle.vkard.property

import de.dstulle.vkard.property.parameter.*
import de.dstulle.vkard.property.parameter.Value.Type.TEXT
import de.dstulle.vkard.property.parameter.Value.Type.URI
import de.dstulle.vkard.property.value.Value
import kotlin.test.*

internal class PropertyTest {

    private class TestValue(val value: String): Value<String>() {

        override fun get() = value
        override fun isEmpty() = value.isEmpty()
        override fun toVCardString() = value

        override fun equals(other: Any?): Boolean {
            return when {
                this === other -> true
                other is TestValue -> value == other.value
                else -> false
            }
        }

        override fun hashCode(): Int {
            return value.hashCode()
        }
    }

    private class TestProperty( override val name: String, value: String): Property<TestValue>(TestValue(value)) {
        override val typeParameterIsAllowed = true
    }

    private class TestParameter(value: String): Parameter<String>(ParameterType.LANGUAGE, value)

    @Test
    fun group() {

        val testProperty = TestProperty("name", "value")
        assertEquals("NAME:value", testProperty.toVCardString())

        testProperty.group("group")
        assertEquals("GROUP.NAME:value", testProperty.toVCardString())

    }

    @Test
    fun parameters() {

        val testProperty = TestProperty("name", "value")
        assertNull(testProperty.language)
        assertNull(testProperty.valueType)
        assertNull(testProperty.pref)
        assertNull(testProperty.altId)
        assertNull(testProperty.pId)
        assertTrue(testProperty.types.isEmpty())

        testProperty.parameter(Language("en"))
        testProperty.parameter(Value(TEXT))
        testProperty.parameter(Pref(42))
        testProperty.parameter(AltId("x"))
        testProperty.parameter(PId(23))
        testProperty.parameter(Type(Type.Value.HOME))

        assertEquals("en", testProperty.language)
        assertEquals("text", testProperty.valueType)
        assertEquals(42, testProperty.pref)
        assertEquals("x", testProperty.altId)
        assertEquals(PropertyIdValue(23), testProperty.pId)
        assertEquals(Type.Value.HOME.toVCardString(), testProperty.types.first())

    }

    @Test
    fun parameter() {

        val testProperty = TestProperty("name", "value")
        assertEquals("NAME:value", testProperty.toVCardString())

        testProperty.parameter(TestParameter("pval"))
        assertEquals("NAME;LANGUAGE=pval:value", testProperty.toVCardString())

        testProperty.parameter(Value(URI))
        assertEquals("NAME;LANGUAGE=pval:value", testProperty.toVCardString())

        testProperty.parameter(Value(TEXT))
        assertEquals("NAME;LANGUAGE=pval;VALUE=text:value", testProperty.toVCardString())

        testProperty.parameter(PId(23))
        assertEquals("NAME;LANGUAGE=pval;VALUE=text;PID=23:value", testProperty.toVCardString())

        testProperty.parameter(Type(Type.Value.HOME))
        assertEquals("NAME;LANGUAGE=pval;VALUE=text;PID=23;TYPE=home:value", testProperty.toVCardString())

        testProperty.parameter(Type(Type.Value.WORK))
        assertEquals("NAME;LANGUAGE=pval;VALUE=text;PID=23;TYPE=home,work:value", testProperty.toVCardString())

    }

    @Test
    fun removeParameter() {

        val testProperty = TestProperty("name", "value").parameter(TestParameter("pval"))
        assertEquals("NAME;LANGUAGE=pval:value", testProperty.toVCardString())

        testProperty.removeParameter(TestParameter("whatever"))
        assertEquals("NAME:value", testProperty.toVCardString())

    }

    @Test
    fun testRemoveParameter(){

        val testProperty = TestProperty("name", "value").parameter(TestParameter("pval"))
        assertEquals("NAME;LANGUAGE=pval:value", testProperty.toVCardString())

        testProperty.removeParameter(ParameterType.LANGUAGE)
        assertEquals("NAME:value", testProperty.toVCardString())

    }

    @Test
    fun toVCardString() {

        assertEquals("NAME:value", TestProperty("name","value").toVCardString())
        assertEquals("NAME:value", TestProperty("name","value").group("").toVCardString())
        assertEquals("GROUP.NAME:value", TestProperty("name","value").group("group").toVCardString())

        assertEquals("NAME;LANGUAGE=pval:value", TestProperty("name","value").parameter(TestParameter("pval")).toVCardString())
        assertEquals("NAME;LANGUAGE=pval:value", TestProperty("name","value").group("").parameter(TestParameter("pval")).toVCardString())
        assertEquals(
            "GROUP.NAME;LANGUAGE=pval:value",
            TestProperty("name","value").group("group").parameter(TestParameter("pval")
            ).toVCardString())

    }

    @Test
    fun testEquals() {

        val property1a = TestProperty("test", "some text").group("group").parameter(TestParameter("pval"))
        val property1b = TestProperty("test", "some text").group("group").parameter(TestParameter("pval"))
        val property1c = TestProperty("test", "some text").group("GROUP").parameter(TestParameter("pval"))
        val property1d = TestProperty("test", "some text").group("Group").parameter(TestParameter("pval"))

        val property2a = TestProperty("test 2", "some text").group("group").parameter(TestParameter("pval"))
        val property2b = TestProperty("test", "some text").group("B-group").parameter(TestParameter("pval"))
        val property2c = TestProperty("test", "some other text").group("group").parameter(TestParameter("pval"))
        val property2d = TestProperty("test", "some text").group("group").parameter(TestParameter("p-val"))
        val property2e = TestProperty("test", "some text")
        val property2f = TestProperty("test", "some text").parameter(TestParameter("pval"))
        val property2g = TestProperty("test", "some text").group("group")

        assertFalse(property1a.equals("test"))
        assertFalse(property1a.equals(null))

        assertEquals(property1a, property1d)
        assertEquals(property1a, property1c)
        assertEquals(property1a, property1b)

        assertEquals(property1a, property1a)

        assertEquals(property1b, property1a)
        assertEquals(property1c, property1a)
        assertEquals(property1d, property1a)

        assertNotEquals(property2a, property1a)
        assertNotEquals(property2b, property1a)
        assertNotEquals(property2c, property1a)
        assertNotEquals(property2d, property1a)
        assertNotEquals(property2e, property1a)
        assertNotEquals(property2f, property1a)
        assertNotEquals(property2g, property1a)

        assertNotEquals(property1a, property2a)
        assertNotEquals(property1a, property2b)
        assertNotEquals(property1a, property2c)
        assertNotEquals(property1a, property2d)
        assertNotEquals(property1a, property2e)
        assertNotEquals(property1a, property2f)
        assertNotEquals(property1a, property2g)

    }

    @Test
    fun testHashCode() {
        val property1a = TestProperty("test", "some text").group("group")
        val property1b = TestProperty("test", "some text").group("group")
        val property1c = TestProperty("test", "some text").group("GROUP")
        val property1d = TestProperty("test", "some text").group("Group")
        val property2a = TestProperty("test 2", "some text").group("group")
        val property2b = TestProperty("test", "some text").group("B-group")
        val property2c = TestProperty("test", "some other text").group("group")
        val property2d = TestProperty("test", "some text")

        assertEquals(property1a.hashCode(), property1d.hashCode())
        assertEquals(property1a.hashCode(), property1c.hashCode())
        assertEquals(property1a.hashCode(), property1b.hashCode())

        assertEquals(property1a.hashCode(), property1a.hashCode())

        assertEquals(property1b.hashCode(), property1a.hashCode())
        assertEquals(property1c.hashCode(), property1a.hashCode())
        assertEquals(property1d.hashCode(), property1a.hashCode())

        assertNotEquals(property1a.hashCode(), property2a.hashCode())
        assertNotEquals(property1a.hashCode(), property2b.hashCode())
        assertNotEquals(property1a.hashCode(), property2c.hashCode())
        assertNotEquals(property1a.hashCode(), property2d.hashCode())

        assertNotEquals(property2a.hashCode(), property1a.hashCode())
        assertNotEquals(property2b.hashCode(), property1a.hashCode())
        assertNotEquals(property2c.hashCode(), property1a.hashCode())
        assertNotEquals(property2d.hashCode(), property1a.hashCode())

    }

}