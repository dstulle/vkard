/*
 * GNU LESSER GENERAL PUBLIC LICENSE
 * Copyright (C) 2022 - 2024 Daniel Sturm
 * mail@DanielSturm.de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.dstulle.vkard.property.parameters

import de.dstulle.vkard.property.Cardinality
import de.dstulle.vkard.property.Property
import de.dstulle.vkard.property.parameter.PId
import de.dstulle.vkard.property.parameter.ParameterType
import de.dstulle.vkard.property.parameter.PropertyIdValue
import de.dstulle.vkard.property.value.TextValue
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals

internal class PIdTest {

    private class ExactlyOneMustTestProperty(value: String) : Property<TextValue>(TextValue(value)) {

        override val cardinality: Cardinality
            get() = Cardinality.EXACTLY_ONE_MUST

        override val name: String
            get() = Cardinality.EXACTLY_ONE_MUST.toString()
    }

    private class ExactlyOneMayTestProperty(value: String) : Property<TextValue>(TextValue(value)) {

        override val cardinality: Cardinality
            get() = Cardinality.EXACTLY_ONE_MAY

        override val name: String
            get() = Cardinality.EXACTLY_ONE_MAY.toString()
    }

    private class OneOrMoreMustTestProperty(value: String) : Property<TextValue>(TextValue(value)) {

        override val cardinality: Cardinality
            get() = Cardinality.ONE_OR_MORE_MUST

        override val name: String
            get() = Cardinality.ONE_OR_MORE_MUST.toString()
    }

    private class OneOrMoreMayTestProperty(value: String) : Property<TextValue>(TextValue(value)) {

        override val cardinality: Cardinality
            get() = Cardinality.ONE_OR_MORE_MAY

        override val name: String
            get() = Cardinality.ONE_OR_MORE_MAY.toString()
    }

    @Test
    fun toVCardString() {

        assertEquals("PID=1", PId(1).toVCardString())
        assertEquals("PID=42", PId(42).toVCardString())
        assertEquals("PID=100", PId(100).toVCardString())

        assertEquals("PID=42,23.5", PId(Pair(42, null), Pair(23, 5)).toVCardString())

    }

    @Test
    fun testEquals() {

        val pref1 = PId(1)
        val pref42a = PId(42)
        val pref42b = PId(42)
        val pref100 = PId(100)

        assertEquals(pref42a, pref42b)
        assertNotEquals(pref1, pref42a)
        assertNotEquals(pref42a, pref100)

    }

    @Test
    fun testHashCode() {

        val pref1 = PId(1)
        val pref42a = PId(42)
        val pref42b = PId(42)
        val pref100 = PId(100)

        assertEquals(pref42a.hashCode(), pref42b.hashCode())
        assertNotEquals(pref1.hashCode(), pref42a.hashCode())
        assertNotEquals(pref42a.hashCode(), pref100.hashCode())

    }

    @Test
    fun getName() {

        assertEquals(ParameterType.PID, PId(42).name)

    }

    @Test
    fun getValue() {

        assertEquals(PropertyIdValue(42), PId(42).value)
        assertEquals(PropertyIdValue(42), PId(42, null).value)
        assertEquals(PropertyIdValue(42), PId(Pair(42, null)).value)

        assertEquals(PropertyIdValue(42), PId(42).value)
        assertEquals(PropertyIdValue(42, null), PId(42).value)
        assertEquals(PropertyIdValue(Pair(42, null)), PId(42).value)

        assertEquals(PropertyIdValue(Pair(42, null), Pair(23, 5)), PId(Pair(42, null), Pair(23, 5)).value)

    }

    @Test
    fun mustNotAppearOnOneInstanceProperties() {

        val exactlyOneMustTestProperty = ExactlyOneMustTestProperty("exactly one")
        val exactlyOneMayTestProperty = ExactlyOneMayTestProperty("maybe one")
        val oneOrMoreMustTestProperty = OneOrMoreMustTestProperty("at least one")
        val oneOrMoreMayTestProperty = OneOrMoreMayTestProperty("anything is possible")

        assertEquals(null, exactlyOneMustTestProperty.pId)
        assertEquals(null, exactlyOneMayTestProperty.pId)
        assertEquals(null, oneOrMoreMustTestProperty.pId)
        assertEquals(null, oneOrMoreMayTestProperty.pId)

        exactlyOneMustTestProperty.parameter(PId(42))
        exactlyOneMayTestProperty.parameter(PId(42))
        oneOrMoreMustTestProperty.parameter(PId(42))
        oneOrMoreMayTestProperty.parameter(PId(42))

        assertEquals(null, exactlyOneMustTestProperty.pId)
        assertEquals(null, exactlyOneMayTestProperty.pId)
        assertEquals(PropertyIdValue(42), oneOrMoreMustTestProperty.pId)
        assertEquals(PropertyIdValue(42), oneOrMoreMayTestProperty.pId)

    }

}