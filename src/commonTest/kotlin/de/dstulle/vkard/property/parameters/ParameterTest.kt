/*
 * GNU LESSER GENERAL PUBLIC LICENSE
 * Copyright (C) 2021 - 2024 Daniel Sturm
 * mail@DanielSturm.de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.dstulle.vkard.property.parameters

import de.dstulle.vkard.property.parameter.Parameter
import de.dstulle.vkard.property.parameter.ParameterType
import kotlin.test.*

internal class ParameterTest {

    private class TestParameter(name: ParameterType, value: String): Parameter<String>(name, value)

    @Test
    fun toVCardString() {

        assertEquals("LANGUAGE=", TestParameter(ParameterType.LANGUAGE, "").toVCardString())
        assertEquals("LANGUAGE=value", TestParameter(ParameterType.LANGUAGE, "value").toVCardString())
        assertEquals("LANGUAGE=en-US", TestParameter(ParameterType.LANGUAGE, "en-US").toVCardString())

    }

    @Test
    fun testEquals() {

        val testParameter1a = TestParameter(ParameterType.LANGUAGE, "value")
        val testParameter1b = TestParameter(ParameterType.LANGUAGE, "value")
        val testParameter2 = TestParameter(ParameterType.LANGUAGE, "other value")

        assertEquals(testParameter1a, testParameter1a)

        assertEquals(testParameter1a, testParameter1b)
        assertNotEquals(testParameter1a, testParameter2)
        assertNotEquals(testParameter1b, testParameter2)

        assertNotEquals(testParameter1a, Any())

    }

    @Test
    fun testHashCode() {

        val testParameter1a = TestParameter(ParameterType.LANGUAGE, "value")
        val testParameter1b = TestParameter(ParameterType.LANGUAGE, "value")
        val testParameter2 = TestParameter(ParameterType.LANGUAGE, "other value")

        assertEquals(testParameter1a.hashCode(), testParameter1a.hashCode())

        assertEquals(testParameter1a.hashCode(), testParameter1b.hashCode())
        assertNotEquals(testParameter1a.hashCode(), testParameter2.hashCode())
        assertNotEquals(testParameter1b.hashCode(), testParameter2.hashCode())

        assertNotEquals(testParameter1a.hashCode(), Any())

    }

    @Test
    fun getName() {

        val testParameter = TestParameter(ParameterType.LANGUAGE, "whatever")

        assertEquals(ParameterType.LANGUAGE, testParameter.name)

    }

    @Test
    fun getValue() {

        val testParameter = TestParameter(ParameterType.LANGUAGE, "something important")

        assertEquals("something important", testParameter.value)

    }
}