/*
 * GNU LESSER GENERAL PUBLIC LICENSE
 * Copyright (C) 2022 - 2024 Daniel Sturm
 * mail@DanielSturm.de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.dstulle.vkard.property.parameters

import de.dstulle.vkard.property.parameter.ParameterType
import de.dstulle.vkard.property.parameter.Pref
import kotlin.test.*

internal class PrefTest {

    @BeforeTest
    fun setUp() {
    }

    @AfterTest
    fun tearDown() {
    }

    @Test
    fun toVCardString() {

        assertEquals("PREF=1", Pref(1).toVCardString())
        assertEquals("PREF=42", Pref(42).toVCardString())
        assertEquals("PREF=100", Pref(100).toVCardString())

    }

    @Test
    fun testEquals() {

        val pref1 = Pref(1)
        val pref42a = Pref(42)
        val pref42b = Pref(42)
        val pref100 = Pref(100)

        assertEquals(pref42a, pref42b)
        assertNotEquals(pref1, pref42a)
        assertNotEquals(pref42a, pref100)

    }

    @Test
    fun testHashCode() {

        val pref1 = Pref(1)
        val pref42a = Pref(42)
        val pref42b = Pref(42)
        val pref100 = Pref(100)

        assertEquals(pref42a.hashCode(), pref42b.hashCode())
        assertNotEquals(pref1.hashCode(), pref42a.hashCode())
        assertNotEquals(pref42a.hashCode(), pref100.hashCode())

    }

    @Test
    fun getName() {

        assertEquals(ParameterType.PREF, Pref(42).name)

    }

    @Test
    fun getValue() {

        assertEquals(42, Pref(42).value)

    }

    @Test
    fun valueBoundaries() {

        assertEquals(1, Pref(-42).value)
        assertEquals(1, Pref(0).value)
        assertEquals(1, Pref(1).value)
        assertEquals(42, Pref(42).value)
        assertEquals(100, Pref(100).value)
        assertEquals(100, Pref(101).value)
        assertEquals(100, Pref(1000000).value)

    }

    @Test
    fun compareTo() {

        val pref1 = Pref(1)
        val pref42a = Pref(42)
        val pref42b = Pref(42)
        val pref100 = Pref(100)

        assertTrue(pref1 < pref42a)
        assertEquals(0, pref42a.compareTo(pref42b))
        assertTrue(pref100 > pref42a)

    }
}