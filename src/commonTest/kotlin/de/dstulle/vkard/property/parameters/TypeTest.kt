/*
 * GNU LESSER GENERAL PUBLIC LICENSE
 * Copyright (C) 2022 - 2024 Daniel Sturm
 * mail@DanielSturm.de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.dstulle.vkard.property.parameters

import de.dstulle.vkard.property.Property
import de.dstulle.vkard.property.parameter.ParameterType
import de.dstulle.vkard.property.parameter.Type
import de.dstulle.vkard.property.parameter.Type.Value.*
import de.dstulle.vkard.property.value.TextValue
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals

internal class TypeTest {

    private class TestPropertyWithTypeParameterAllowed(value: String) : Property<TextValue>(TextValue(value)) {
        override val name = "TypeAllowed"
        override val typeParameterIsAllowed = true
    }

    private class TestPropertyWithAdditionalTypeParameters(value: String) : Property<TextValue>(TextValue(value)) {
        override val name = "MoreTypesAllowed"
        override val typeParameterIsAllowed = true
        override val typeParameterAllowedValues = super.typeParameterAllowedValues + listOf(VOICE)
    }


    private class TestPropertyWithNoTypeParameterAllowed(value: String) : Property<TextValue>(TextValue(value)) {
        override val name = "NoTypeAllowed"
        override val typeParameterIsAllowed = false
    }

    @Test
    fun mergeWith() {

        assertEquals(setOf("home"), Type(HOME).value.toSet())
        assertEquals(setOf("work"), Type(WORK).value.toSet())

        assertEquals(setOf("home"), Type(HOME).mergeWith(Type(HOME)).value.toSet())
        assertEquals(setOf("work"), Type(WORK).mergeWith(Type(WORK)).value.toSet())

        assertEquals(setOf("home", "work"), Type(HOME).mergeWith(Type(WORK)).value.toSet())
        assertEquals(setOf("home", "work"), Type(WORK).mergeWith(Type(HOME)).value.toSet())

        assertEquals(setOf("home", "work"), Type(HOME).mergeWith(Type(WORK)).mergeWith(Type(WORK)).value.toSet())
        assertEquals(setOf("home", "work"), Type(WORK).mergeWith(Type(HOME)).mergeWith(Type(HOME)).value.toSet())

    }

    @Test
    fun toVCardString() {

        assertEquals("TYPE=work", Type("work").toVCardString())
        assertEquals("TYPE=home", Type(HOME).toVCardString())
        assertEquals("TYPE=work,home", Type("work", "home").toVCardString())

    }

    @Test
    fun testEquals() {

        val prefWork = Type(WORK)
        val prefHome = Type("HOME")
        val prefWorkHome = Type(WORK, HOME)

        assertEquals(prefWork, prefWork)
        assertEquals(prefHome, prefHome)
        assertNotEquals(prefWork, prefHome)
        assertNotEquals(prefWork, prefWorkHome)
        assertNotEquals(prefHome, prefWorkHome)

    }

    @Test
    fun testHashCode() {

        val prefWork = Type(WORK)
        val prefHome = Type("HOME")
        val prefWorkHome = Type(WORK, HOME)

        assertEquals(prefWork.hashCode(), prefWork.hashCode())
        assertEquals(prefHome.hashCode(), prefHome.hashCode())
        assertNotEquals(prefWork.hashCode(), prefHome.hashCode())
        assertNotEquals(prefWork.hashCode(), prefWorkHome.hashCode())
        assertNotEquals(prefHome.hashCode(), prefWorkHome.hashCode())

    }

    @Test
    fun getName() {

        assertEquals(ParameterType.TYPE, Type("42").name)

    }

    @Test
    fun getValue() {

        assertEquals(setOf("work"), Type(WORK).value.toSet())
        assertEquals(setOf("work", "home"), Type(WORK, HOME).value.toSet())

    }

    @Test
    fun mustNotAppearSomeProperties() {

        val testPropertyWithTypeParameterAllowed = TestPropertyWithTypeParameterAllowed("Type is allowed")
        val testPropertyWithNoTypeParameterAllowed = TestPropertyWithNoTypeParameterAllowed("No Type is allowed")

        assertEquals(setOf(), testPropertyWithTypeParameterAllowed.types.toSet())
        assertEquals(setOf(), testPropertyWithNoTypeParameterAllowed.types.toSet())

        testPropertyWithTypeParameterAllowed.parameter(Type("home"))
        testPropertyWithNoTypeParameterAllowed.parameter(Type("home"))

        assertEquals(setOf("home"), testPropertyWithTypeParameterAllowed.types.toSet())
        assertEquals(setOf(), testPropertyWithNoTypeParameterAllowed.types.toSet())

    }

    @Test
    fun canBeExtended() {

        val testPropertyWithTypeParameterAllowed = TestPropertyWithTypeParameterAllowed("Only default values are allowed")
        val testPropertyWithAdditionalTypeParameters = TestPropertyWithAdditionalTypeParameters("Other values in Type are allowed")

        assertEquals(listOf(), testPropertyWithTypeParameterAllowed.types)
        assertEquals(listOf(), testPropertyWithAdditionalTypeParameters.types)

        testPropertyWithTypeParameterAllowed.parameter(Type("home"))
        testPropertyWithAdditionalTypeParameters.parameter(Type("home"))

        assertEquals(setOf("home"), testPropertyWithTypeParameterAllowed.types)
        assertEquals(setOf("home"), testPropertyWithAdditionalTypeParameters.types)

        testPropertyWithTypeParameterAllowed.parameter(Type("home", "voice"))
        testPropertyWithAdditionalTypeParameters.parameter(Type("home", "voice"))

        assertEquals(setOf("home"), testPropertyWithTypeParameterAllowed.types)
        assertEquals(setOf("home", "voice"), testPropertyWithAdditionalTypeParameters.types)

    }

}