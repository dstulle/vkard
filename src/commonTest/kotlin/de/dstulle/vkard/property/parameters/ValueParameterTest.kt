/*
 * GNU LESSER GENERAL PUBLIC LICENSE
 * Copyright (C) 2021 - 2024 Daniel Sturm
 * mail@DanielSturm.de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.dstulle.vkard.property.parameters

import de.dstulle.vkard.property.parameter.Value.Type
import kotlin.test.*

internal class ValueParameterTest {

    @Test
    fun typeToVCardString() {

        assertEquals("text", Type.TEXT.toVCardString())
        assertEquals("uri", Type.URI.toVCardString())
        assertEquals("date", Type.DATE.toVCardString())
        assertEquals("time", Type.TIME.toVCardString())
        assertEquals("date-time", Type.DATE_TIME.toVCardString())
        assertEquals("date-and-or-time", Type.DATE_AND_OR_TIME.toVCardString())
        assertEquals("timestamp", Type.TIMESTAMP.toVCardString())
        assertEquals("boolean", Type.BOOLEAN.toVCardString())
        assertEquals("integer", Type.INTEGER.toVCardString())
        assertEquals("float", Type.FLOAT.toVCardString())
        assertEquals("utc-offset", Type.UTC_OFFSET.toVCardString())
        assertEquals("language-tag", Type.LANGUAGE_TAG.toVCardString())

    }

}