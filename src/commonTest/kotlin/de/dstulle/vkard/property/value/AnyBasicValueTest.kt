/*
 * GNU LESSER GENERAL PUBLIC LICENSE
 * Copyright (C) 2024 Daniel Sturm
 * mail@DanielSturm.de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.dstulle.vkard.property.value

import kotlin.test.*

class AnyBasicValueTest {

    data class TestValue(private val value: String = ""): BasicValue<String>() {
        override fun get() = value
        override fun isEmpty() = value.isEmpty()
        override fun toVCardString() = value
    }

    @Test
    fun get() {

        assertEquals("test", AnyBasicValue(TestValue("test")).get())
        assertEquals("value", AnyBasicValue(TestValue("value")).get())

    }

    @Test
    fun isEmpty() {

        assertTrue(AnyBasicValue(TestValue("")).isEmpty())
        assertFalse(AnyBasicValue(TestValue("not Empty")).isEmpty())
        assertFalse(AnyBasicValue(TestValue("also not Empty")).isEmpty())

    }

    @Test
    fun toVCardString() {

        assertEquals(TestValue("string").toVCardString(), AnyBasicValue(TestValue("string")).toVCardString())
        assertEquals(TestValue("value").toVCardString(), AnyBasicValue(TestValue("value")).toVCardString())

    }

    @Test
    fun testEquals() {

        val anyBasicValue = AnyBasicValue(TestValue("equal"))

        assertEquals(anyBasicValue, anyBasicValue)
        assertFalse(anyBasicValue.equals(StructuredTextValue(mutableListOf())))
        assertNotEquals(anyBasicValue, AnyBasicValue(TestValue("not equal")))

        assertEquals(anyBasicValue, AnyBasicValue(TestValue("equal")))

    }

    @Test
    fun testHashCode() {

        assertEquals(TestValue("hash").hashCode(), AnyBasicValue(TestValue("hash")).hashCode())
        assertEquals(TestValue("code").hashCode(), AnyBasicValue(TestValue("code")).hashCode())

    }

}