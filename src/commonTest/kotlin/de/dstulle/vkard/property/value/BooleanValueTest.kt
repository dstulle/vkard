/*
 * GNU LESSER GENERAL PUBLIC LICENSE
 * Copyright (C) 2021 - 2024 Daniel Sturm
 * mail@DanielSturm.de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.dstulle.vkard.property.value

import kotlin.test.*

/**
 * @author Daniel Sturm &lt;daniel.sturm@microwood.games&gt;
 */
internal class BooleanValueTest {

    @Test
    fun testCopyConstructor() {

        val originalTrueValue = BooleanValue(true)
        val originalFalseValue = BooleanValue(false)

        val copiedTrueValue = BooleanValue(originalTrueValue)
        val copiedFalseValue = BooleanValue(originalFalseValue)

        assertEquals(originalTrueValue, copiedTrueValue)
        assertEquals(originalFalseValue, copiedFalseValue)
        assertNotEquals(originalTrueValue, copiedFalseValue)
        assertNotEquals(originalFalseValue, copiedTrueValue)

        assertEquals("TRUE", copiedTrueValue.toVCardString())
        assertEquals("FALSE", copiedFalseValue.toVCardString())

    }

    @Test
    fun testStringConstructor() {

        val trueValue = BooleanValue(true)
        val falseValue = BooleanValue(false)

        val stringTrueValue = BooleanValue("true")
        val stringFalseValue = BooleanValue("false")
        val stringAnyValue = BooleanValue("any")
        val stringEmptyValue = BooleanValue("")

        assertEquals(trueValue, stringTrueValue)
        assertEquals(falseValue, stringFalseValue)
        assertEquals(falseValue, stringAnyValue)
        assertEquals(falseValue, stringEmptyValue)

        assertNotEquals(trueValue, stringFalseValue)
        assertNotEquals(falseValue, stringTrueValue)

        assertEquals("TRUE", stringTrueValue.toVCardString())
        assertEquals("FALSE", stringFalseValue.toVCardString())

    }

    @Test
    fun get() {

        assertEquals(true, BooleanValue(true).get())
        assertEquals(false, BooleanValue(false).get())

    }

    @Test
    fun isEmpty() {

        assertFalse(BooleanValue(true).isEmpty())
        assertFalse(BooleanValue(false).isEmpty())

    }

    @Test
    fun toVCardString() {

        assertEquals("TRUE", BooleanValue(true).toVCardString())
        assertEquals("FALSE", BooleanValue(false).toVCardString())

    }

    @Test
    fun testEquals() {
        val trueValue = BooleanValue(true)
        val falseValue = BooleanValue(false)

        assertFalse(trueValue.equals(true))
        assertFalse(trueValue.equals(false))
        assertFalse(falseValue.equals(true))
        assertFalse(falseValue.equals(false))
        assertFalse(trueValue.equals(null))
        assertFalse(falseValue.equals(null))

        assertEquals(trueValue, trueValue)
        assertEquals(falseValue, falseValue)
        assertNotEquals(trueValue, falseValue)
        assertNotEquals(falseValue, trueValue)
    }

    @Test
    fun testHashCode() {

        val trueValue = BooleanValue(true)
        val falseValue = BooleanValue(false)

        assertEquals(trueValue.hashCode(), trueValue.hashCode())
        assertEquals(falseValue.hashCode(), falseValue.hashCode())
        assertNotEquals(trueValue.hashCode(), falseValue.hashCode())
        assertNotEquals(falseValue.hashCode(), trueValue.hashCode())

    }

    @Test
    fun parseBoolean() {

        assertTrue(BooleanValue.parseBoolean("TRUE"))
        assertTrue(BooleanValue.parseBoolean("True"))
        assertTrue(BooleanValue.parseBoolean("true"))
        assertTrue(BooleanValue.parseBoolean("tRuE"))

        assertFalse(BooleanValue.parseBoolean("FALSE"))
        assertFalse(BooleanValue.parseBoolean("False"))
        assertFalse(BooleanValue.parseBoolean("false"))
        assertFalse(BooleanValue.parseBoolean("fAlSe"))

        assertFalse(BooleanValue.parseBoolean(""))
        assertFalse(BooleanValue.parseBoolean("0"))
        assertFalse(BooleanValue.parseBoolean("1"))
        assertFalse(BooleanValue.parseBoolean("whatever"))
    }
}