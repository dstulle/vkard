/*
 * GNU LESSER GENERAL PUBLIC LICENSE
 * Copyright (C) 2021 - 2024 Daniel Sturm
 * mail@DanielSturm.de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.dstulle.vkard.property.value

import kotlin.test.*

internal class StructuredTextValueTest {

    @Test
    fun testCopyConstructor() {

        val originalValue = StructuredTextValue(mutableListOf(TextListValue("some", "test"), TextListValue("values")))

        val copiedValue = StructuredTextValue(originalValue)

        assertEquals(originalValue, copiedValue)
        assertEquals("some,test;values", copiedValue.toVCardString())

    }

    @Test
    fun toVCardString() {

        assertEquals(
            "",
            StructuredTextValue().toVCardString()
        )

        assertEquals(
            "",
            StructuredTextValue(mutableListOf()).toVCardString()
        )

        assertEquals(
            "",
            StructuredTextValue(mutableListOf(
                TextListValue("")
            )).toVCardString()
        )

        assertEquals(
            ";",
            StructuredTextValue(mutableListOf(
                TextListValue(""),
                TextListValue("")
            )).toVCardString()
        )

        assertEquals(
            "value",
            StructuredTextValue(mutableListOf(
                TextListValue("value")
            )).toVCardString()
        )

        assertEquals(
            "first,second",
            StructuredTextValue(mutableListOf(
                TextListValue("first", "second")
            )).toVCardString()
        )

        assertEquals(
            "first;second",
            StructuredTextValue(mutableListOf(
                TextListValue("first"),
                TextListValue("second")
            )).toVCardString()
        )

        assertEquals(
            "first,second;third",
            StructuredTextValue(mutableListOf(
                TextListValue("first", "second"),
                TextListValue("third")
            )).toVCardString()
        )

    }

    @Test
    fun isEmpty() {

        assertTrue(StructuredTextValue(mutableListOf()).isEmpty())
        assertTrue(StructuredTextValue(mutableListOf(TextListValue(""))).isEmpty())
        assertTrue(StructuredTextValue(mutableListOf(TextListValue(""), TextListValue(""))).isEmpty())
        assertFalse(StructuredTextValue(mutableListOf(TextListValue("not empty"))).isEmpty())

    }

    @Test
    fun testEquals() {

        val structuredTextValue1a = StructuredTextValue(mutableListOf(
            TextListValue("some", "test"),
            TextListValue("values")
        ))
        val structuredTextValue1b = StructuredTextValue(mutableListOf(
            TextListValue("some", "test"),
            TextListValue("values")
        ))
        val structuredTextValue2a = StructuredTextValue(mutableListOf(
            TextListValue("other", "test"),
            TextListValue("values")
        ))
        val structuredTextValue2b = StructuredTextValue(mutableListOf(
            TextListValue("some", "other", "test"),
            TextListValue("values")
        ))
        val structuredTextValue2c = StructuredTextValue(mutableListOf(
            TextListValue("some"),
            TextListValue("test", "values")
        ))

        assertFalse(structuredTextValue1a.equals("some,test;value"))
        assertFalse(structuredTextValue1a.equals(null))

        assertEquals(structuredTextValue1a, structuredTextValue1b)
        assertEquals(structuredTextValue1b, structuredTextValue1a)

        assertNotEquals(structuredTextValue1a, structuredTextValue2a)
        assertNotEquals(structuredTextValue1a, structuredTextValue2b)
        assertNotEquals(structuredTextValue1a, structuredTextValue2c)

        assertNotEquals(structuredTextValue2a, structuredTextValue1a)
        assertNotEquals(structuredTextValue2b, structuredTextValue1a)
        assertNotEquals(structuredTextValue2c, structuredTextValue1a)

    }

    @Test
    fun testHashCode() {

        val structuredTextValue1a = StructuredTextValue(mutableListOf(
            TextListValue("some", "test"),
            TextListValue("values")
        ))
        val structuredTextValue1b = StructuredTextValue(mutableListOf(
            TextListValue("some", "test"),
            TextListValue("values")
        ))
        val structuredTextValue2a = StructuredTextValue(mutableListOf(
            TextListValue("other", "test"),
            TextListValue("values")
        ))
        val structuredTextValue2b = StructuredTextValue(mutableListOf(
            TextListValue("some", "other", "test"),
            TextListValue("values")
        ))
        val structuredTextValue2c = StructuredTextValue(mutableListOf(
            TextListValue("some"),
            TextListValue("test", "values")
        ))

        assertEquals(structuredTextValue1a.hashCode(), structuredTextValue1b.hashCode())
        assertEquals(structuredTextValue1b.hashCode(), structuredTextValue1a.hashCode())

        assertNotEquals(structuredTextValue1a.hashCode(), structuredTextValue2a.hashCode())
        assertNotEquals(structuredTextValue1a.hashCode(), structuredTextValue2b.hashCode())
        assertNotEquals(structuredTextValue1a.hashCode(), structuredTextValue2c.hashCode())

        assertNotEquals(structuredTextValue2a.hashCode(), structuredTextValue1a.hashCode())
        assertNotEquals(structuredTextValue2b.hashCode(), structuredTextValue1a.hashCode())
        assertNotEquals(structuredTextValue2c.hashCode(), structuredTextValue1a.hashCode())

    }

    @Test
    fun mergeWith() {

        val structuredTextValue1 = StructuredTextValue(mutableListOf(
            TextListValue(),
            TextListValue("values")
        ))
        val structuredTextValue2 = StructuredTextValue(mutableListOf(
            TextListValue("some", "test"),
            TextListValue()
        ))
        val structuredTextValue3 = StructuredTextValue(mutableListOf(
            TextListValue("some", "test"),
            TextListValue("values")
        ))
        val structuredTextValue4 = StructuredTextValue(mutableListOf(
            TextListValue(),
            TextListValue(),
            TextListValue("additional entry")
        ))

        assertNotEquals(structuredTextValue1, structuredTextValue3)

        structuredTextValue1.mergeWith(structuredTextValue2)
        assertEquals(structuredTextValue1, structuredTextValue3)

        structuredTextValue1.mergeWith(structuredTextValue4)
        assertEquals(structuredTextValue1, structuredTextValue3)

    }
}