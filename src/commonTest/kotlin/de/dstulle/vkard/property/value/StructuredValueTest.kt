/*
 * GNU LESSER GENERAL PUBLIC LICENSE
 * Copyright (C) 2021 - 2024 Daniel Sturm
 * mail@DanielSturm.de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.dstulle.vkard.property.value

import kotlin.test.*

internal class StructuredValueTest {

    data class BasicTestValue(private val value: String = ""): BasicValue<String>() {
        override fun toVCardString() = value
        override fun get() = value
        override fun isEmpty() = value.isEmpty()
    }

    class StructuredTestValue(value: MutableList<BasicTestValue>): StructuredValue<BasicTestValue>(value) {

        constructor(structuredTestValue: StructuredTestValue): this(structuredTestValue.value)
        constructor(): this(mutableListOf())

        override fun isEmpty(): Boolean {
            return value.map{ it.isEmpty()}.reduceOrNull { acc, isEmpty -> acc && isEmpty }?:true
        }

        override fun equals(other: Any?): Boolean {
            return when {
                this === other -> true
                other is StructuredTestValue -> (
                        toVCardString() == other.toVCardString()
                        )
                else -> false
            }
        }

        override fun hashCode(): Int {
            return value.hashCode()
        }

    }

    @Test
    fun testCopyConstructor() {

        val originalValue = StructuredTestValue(mutableListOf(BasicTestValue("some"), BasicTestValue("test"), BasicTestValue("values")))

        val copiedValue = StructuredTestValue(originalValue)

        assertEquals(originalValue, copiedValue)
        assertEquals("some;test;values", copiedValue.toVCardString())

    }

    @Test
    fun toVCardString() {

        assertEquals("", StructuredTestValue().toVCardString())
        assertEquals("", StructuredTestValue(mutableListOf()).toVCardString())
        assertEquals("", StructuredTestValue(mutableListOf(BasicTestValue(""))).toVCardString())

        assertEquals(
            "value",
            StructuredTestValue(mutableListOf(BasicTestValue("value"))).toVCardString()
        )

        assertEquals(
            "first;second;third",
            StructuredTestValue(
                mutableListOf(
                    BasicTestValue("first"),
                    BasicTestValue("second"),
                    BasicTestValue("third")
                )
            ).toVCardString()
        )

    }

    @Test
    fun get() {

        assertEquals(listOf(), StructuredTestValue(mutableListOf()).get())
        assertEquals(listOf(BasicTestValue("")), StructuredTestValue(mutableListOf(BasicTestValue(""))).get())

        assertEquals(
            listOf(BasicTestValue("value")),
            StructuredTestValue(mutableListOf(BasicTestValue("value"))).get()
        )

        assertEquals(
            listOf(
                BasicTestValue("first"),
                BasicTestValue("second"),
                BasicTestValue("third")
            ),
            StructuredTestValue(
                mutableListOf(
                    BasicTestValue("first"),
                    BasicTestValue("second"),
                    BasicTestValue("third")
                )
            ).get()
        )

        val someTestValues = StructuredTestValue(mutableListOf(BasicTestValue("some"), BasicTestValue("test"), BasicTestValue("values")))

        assertEquals(BasicTestValue("some"), someTestValues.get().first())
        assertEquals(BasicTestValue("test"), someTestValues.get()[1])
        assertEquals(BasicTestValue("values"), someTestValues.get().last())

    }


    @Test
    fun isEmpty() {

        assertTrue(StructuredTestValue(mutableListOf()).isEmpty())
        assertTrue(StructuredTestValue(mutableListOf(BasicTestValue(""))).isEmpty())
        assertTrue(StructuredTestValue(mutableListOf(BasicTestValue(""), BasicTestValue(""))).isEmpty())
        assertFalse(StructuredTestValue(mutableListOf(BasicTestValue("not empty"))).isEmpty())

    }

    @Test
    fun testEquals() {

        val structuredValue1a = StructuredTestValue(mutableListOf(
            BasicTestValue("some"),
            BasicTestValue("test"),
            BasicTestValue("values")
        ))
        val structuredValue1b = StructuredTestValue(mutableListOf(
            BasicTestValue("some"),
            BasicTestValue("test"),
            BasicTestValue("values")
        ))

        val structuredValue2a = StructuredTestValue(mutableListOf(
            BasicTestValue("other"),
            BasicTestValue("test"),
            BasicTestValue("values")
        ))
        val structuredValue2b = StructuredTestValue(mutableListOf(
            BasicTestValue("some"),
            BasicTestValue("other"),
            BasicTestValue("test"),
            BasicTestValue("values")
        ))
        val structuredValue2c = StructuredTestValue(mutableListOf(
            BasicTestValue("some"),
            BasicTestValue("values")
        ))

        assertFalse(structuredValue1a.equals("some;test;value"))
        assertFalse(structuredValue1a.equals(null))

        assertEquals(structuredValue1a, structuredValue1b)
        assertEquals(structuredValue1b, structuredValue1a)

        assertNotEquals(structuredValue1a, structuredValue2a)
        assertNotEquals(structuredValue1a, structuredValue2b)
        assertNotEquals(structuredValue1a, structuredValue2c)

        assertNotEquals(structuredValue2a, structuredValue1a)
        assertNotEquals(structuredValue2b, structuredValue1a)
        assertNotEquals(structuredValue2c, structuredValue1a)

    }

    @Test
    fun testHashCode() {

        val structuredValue1a = StructuredTestValue(mutableListOf(
            BasicTestValue("some"),
            BasicTestValue("test"),
            BasicTestValue("values")
        ))
        val structuredValue1b = StructuredTestValue(mutableListOf(
            BasicTestValue("some"),
            BasicTestValue("test"),
            BasicTestValue("values")
        ))

        val structuredValue2a = StructuredTestValue(mutableListOf(
            BasicTestValue("other"),
            BasicTestValue("test"),
            BasicTestValue("values")
        ))
        val structuredValue2b = StructuredTestValue(mutableListOf(
            BasicTestValue("some"),
            BasicTestValue("other"),
            BasicTestValue("test"),
            BasicTestValue("values")
        ))
        val structuredValue2c = StructuredTestValue(mutableListOf(
            BasicTestValue("some"),
            BasicTestValue("values")
        ))

        assertEquals(structuredValue1a.hashCode(), structuredValue1b.hashCode())
        assertEquals(structuredValue1b.hashCode(), structuredValue1a.hashCode())

        assertNotEquals(structuredValue1a.hashCode(), structuredValue2a.hashCode())
        assertNotEquals(structuredValue1a.hashCode(), structuredValue2b.hashCode())
        assertNotEquals(structuredValue1a.hashCode(), structuredValue2c.hashCode())

        assertNotEquals(structuredValue2a.hashCode(), structuredValue1a.hashCode())
        assertNotEquals(structuredValue2b.hashCode(), structuredValue1a.hashCode())
        assertNotEquals(structuredValue2c.hashCode(), structuredValue1a.hashCode())

    }

    @Test
    fun mergeWith() {

        val structuredValue1 = StructuredTestValue(mutableListOf(BasicTestValue(), BasicTestValue(), BasicTestValue("values")))
        val structuredValue2 = StructuredTestValue(mutableListOf(BasicTestValue("some"), BasicTestValue("test"), BasicTestValue()))
        val structuredValue3 = StructuredTestValue(mutableListOf(BasicTestValue("some"), BasicTestValue("test"), BasicTestValue("values")))
        val structuredValue4 = StructuredTestValue(mutableListOf(BasicTestValue(), BasicTestValue(), BasicTestValue(), BasicTestValue("additional entry")))

        assertNotEquals(structuredValue1, structuredValue3)

        assertNotEquals(structuredValue1, structuredValue3)

        structuredValue1.mergeWith(structuredValue2)
        assertEquals(structuredValue1, structuredValue3)

        assertEquals(structuredValue1, structuredValue3)

        structuredValue1.mergeWith(structuredValue4)
        assertEquals(structuredValue1, structuredValue3)

    }
}