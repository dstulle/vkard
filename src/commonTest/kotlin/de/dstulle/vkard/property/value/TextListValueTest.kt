/*
 * GNU LESSER GENERAL PUBLIC LICENSE
 * Copyright (C) 2021 - 2024 Daniel Sturm
 * mail@DanielSturm.de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.dstulle.vkard.property.value

import kotlin.test.*

internal class TextListValueTest {


    @Test
    fun testCopyConstructor() {

        val originalValue = TextListValue(mutableListOf(TextValue("some"), TextValue("test"), TextValue("values")))

        val copiedValue = TextListValue(originalValue)

        assertEquals(originalValue, copiedValue)
        assertEquals("some,test,values", copiedValue.toVCardString())

    }

    @Test
    fun toVCardString() {

        assertEquals("", TextListValue().toVCardString())
        assertEquals("", TextListValue(mutableListOf()).toVCardString())
        assertEquals("", TextListValue(mutableListOf(TextValue(""))).toVCardString())

        assertEquals(
            "value",
            TextListValue(mutableListOf(TextValue("value"))).toVCardString()
        )

        assertEquals(
            "first,second,third",
            TextListValue(mutableListOf(
                TextValue("first"),
                TextValue("second"),
                TextValue("third")
            )).toVCardString()
        )

    }


    @Test
    fun get() {

        assertTrue(TextListValue().get().isEmpty())
        assertTrue(TextListValue(mutableListOf()).get().isEmpty())
        assertEquals(listOf(""), TextListValue(mutableListOf(TextValue(""))).get())

        assertEquals(
            listOf("value"),
            TextListValue(mutableListOf(TextValue("value"))).get()
        )

        assertEquals(
            listOf("first", "second", "third"),
            TextListValue(mutableListOf(
                TextValue("first"),
                TextValue("second"),
                TextValue("third")
            )).get()
        )

    }


    @Test
    fun isEmpty() {

        assertTrue(TextListValue(mutableListOf()).isEmpty())
        assertTrue(TextListValue(mutableListOf(TextValue(""))).isEmpty())
        assertTrue(TextListValue(mutableListOf(TextValue(""), TextValue(""))).isEmpty())
        assertFalse(TextListValue(mutableListOf(TextValue("not empty"))).isEmpty())
        assertFalse(TextListValue(mutableListOf(TextValue("also not empty"))).isEmpty())
        assertFalse(TextListValue(mutableListOf(TextValue("not"), TextValue("empty"))).isEmpty())

    }

    @Test
    fun testEquals() {

        val structuredValue1a = TextListValue(mutableListOf(TextValue("some"), TextValue("test"), TextValue("values")))
        val structuredValue1b = TextListValue(mutableListOf(TextValue("some"), TextValue("test"), TextValue("values")))

        val structuredValue2a = TextListValue(mutableListOf(TextValue("other"), TextValue("test"), TextValue("values")))
        val structuredValue2b = TextListValue(mutableListOf(TextValue("some"), TextValue("other"), TextValue("test"), TextValue("values")))
        val structuredValue2c = TextListValue(mutableListOf(TextValue("some"), TextValue("values")))

        assertFalse(structuredValue1a.equals("some;test;value"))
        assertFalse(structuredValue1a.equals(null))

        assertEquals(structuredValue1a, structuredValue1b)
        assertEquals(structuredValue1b, structuredValue1a)

        assertNotEquals(structuredValue1a, structuredValue2a)
        assertNotEquals(structuredValue1a, structuredValue2b)
        assertNotEquals(structuredValue1a, structuredValue2c)

        assertNotEquals(structuredValue2a, structuredValue1a)
        assertNotEquals(structuredValue2b, structuredValue1a)
        assertNotEquals(structuredValue2c, structuredValue1a)

    }

    @Test
    fun testHashCode() {

        val structuredValue1a = TextListValue(mutableListOf(TextValue("some"), TextValue("test"), TextValue("values")))
        val structuredValue1b = TextListValue(mutableListOf(TextValue("some"), TextValue("test"), TextValue("values")))

        val structuredValue2a = TextListValue(mutableListOf(TextValue("other"), TextValue("test"), TextValue("values")))
        val structuredValue2b = TextListValue(mutableListOf(TextValue("some"), TextValue("other"), TextValue("test"), TextValue("values")))
        val structuredValue2c = TextListValue(mutableListOf(TextValue("some"), TextValue("values")))

        assertEquals(structuredValue1a.hashCode(), structuredValue1b.hashCode())
        assertEquals(structuredValue1b.hashCode(), structuredValue1a.hashCode())

        assertNotEquals(structuredValue1a.hashCode(), structuredValue2a.hashCode())
        assertNotEquals(structuredValue1a.hashCode(), structuredValue2b.hashCode())
        assertNotEquals(structuredValue1a.hashCode(), structuredValue2c.hashCode())

        assertNotEquals(structuredValue2a.hashCode(), structuredValue1a.hashCode())
        assertNotEquals(structuredValue2b.hashCode(), structuredValue1a.hashCode())
        assertNotEquals(structuredValue2c.hashCode(), structuredValue1a.hashCode())

    }
}