/*
 * GNU LESSER GENERAL PUBLIC LICENSE
 * Copyright (C) 2021 - 2024 Daniel Sturm
 * mail@DanielSturm.de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.dstulle.vkard.property.value

import kotlin.test.*

internal class TextValueTest {

    @Test
    fun testCopyConstructor() {

        val originalValue = TextValue("test value")

        val copiedValue = TextValue(originalValue)

        assertEquals(originalValue, copiedValue)
        assertEquals("test value", copiedValue.toVCardString())

    }

    @Test
    fun get() {

        assertEquals("test value", TextValue("test value").get())
        assertEquals("other value", TextValue("other value").get())

    }

    @Test
    fun isEmpty() {

        assertTrue(TextValue().isEmpty())
        assertTrue(TextValue("").isEmpty())
        assertFalse(TextValue("not empty").isEmpty())
        assertFalse(TextValue("also not empty").isEmpty())

    }

    @Test
    fun toVCardString() {

        assertEquals("", TextValue().toVCardString())
        assertEquals("", TextValue("").toVCardString())
        assertEquals("test", TextValue("test").toVCardString())
        assertEquals("other test", TextValue("other test").toVCardString())

    }

    @Test
    fun testEquals() {

        val textValue1a = TextValue("same test string")
        val textValue1b = TextValue("same test string")
        val textValue2 = TextValue("other test string")

        assertFalse(textValue1a.equals("same test string"))
        assertFalse(textValue1a.equals(null))

        assertEquals(textValue1a, textValue1a)
        assertEquals(textValue1a, textValue1b)

        assertNotEquals(textValue1a, textValue2)
        assertNotEquals(textValue2, textValue1a)

    }

    @Test
    fun testHashCode() {

        val textValue1a = TextValue("same test string")
        val textValue1b = TextValue("same test string")
        val textValue2 = TextValue("other test string")

        assertEquals(textValue1a.hashCode(), textValue1a.hashCode())
        assertEquals(textValue1a.hashCode(), textValue1b.hashCode())

        assertNotEquals(textValue1a.hashCode(), textValue2.hashCode())
        assertNotEquals(textValue2.hashCode(), textValue1a.hashCode())

    }
}