/*
 * GNU LESSER GENERAL PUBLIC LICENSE
 * Copyright (C) 2021 - 2024 Daniel Sturm
 * mail@DanielSturm.de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.dstulle.vkard.property.value

import kotlin.test.*

internal class UriValueTest {

    @Test
    fun testCopyConstructor() {

        val originalValue = UriValue("https://example.com/")

        val copiedValue = UriValue(originalValue)

        assertEquals(originalValue, copiedValue)
        assertEquals("https://example.com/", copiedValue.toUriString())

    }

    @Test
    fun get() {

        assertEquals("mailto:arthur.dent@example.com", UriValue("mailto", path = "arthur.dent@example.com").get())

        assertEquals("", UriValue("", "", "", "", "").get())

        assertEquals("https://example.com/", UriValue("https://example.com/").get())
        assertEquals("https://example.com/", UriValue(scheme = "https", host = "example.com", path = "/").get())
        assertEquals("scheme:", UriValue("scheme", "", "", "", "").get())
        assertEquals("", UriValue("", "userinfo", "", "", "").get())
        assertEquals("//userinfo@host", UriValue("", "userinfo", "host", "", "").get())
        assertEquals("//host", UriValue("", "", "host", "", "").get())
        assertEquals("//host:port", UriValue("", "", "host", "port", "").get())
        assertEquals("", UriValue("", "", "", "port", "").get())
        assertEquals("path", UriValue("", "", "", "", "path").get())
        assertEquals("?query", UriValue("", "", "", "", "", "query").get())
        assertEquals("#fragment", UriValue("", "", "", "", "", "", "fragment").get())

    }

    @Test
    fun isEmpty() {

        assertTrue(UriValue("", "", "", "", "").isEmpty())

        assertFalse(UriValue("https://example.com/").isEmpty())
        assertFalse(UriValue("scheme", "", "", "", "").isEmpty())
        assertFalse(UriValue("", "userinfo", "", "", "").isEmpty())
        assertFalse(UriValue("", "", "hist", "", "").isEmpty())
        assertFalse(UriValue("", "", "", "port", "").isEmpty())
        assertFalse(UriValue("", "", "", "", "path").isEmpty())
        assertFalse(UriValue("", "", "", "", "", "query").isEmpty())
        assertFalse(UriValue("", "", "", "", "", "", "fragment").isEmpty())

    }

    @Test
    fun toVCardString() {

        assertEquals("mailto:arthur.dent@example.com", UriValue("mailto", path = "arthur.dent@example.com").toVCardString())

        assertEquals("", UriValue("", "", "", "", "").toVCardString())

        assertEquals("https://example.com/", UriValue("https://example.com/").toVCardString())
        assertEquals("https://example.com/", UriValue(scheme = "https", host = "example.com", path = "/").toVCardString())
        assertEquals("scheme:", UriValue("scheme", "", "", "", "").toVCardString())
        assertEquals("", UriValue("", "userinfo", "", "", "").toVCardString())
        assertEquals("//userinfo@host", UriValue("", "userinfo", "host", "", "").toVCardString())
        assertEquals("//host", UriValue("", "", "host", "", "").toVCardString())
        assertEquals("//host:port", UriValue("", "", "host", "port", "").toVCardString())
        assertEquals("", UriValue("", "", "", "port", "").toVCardString())
        assertEquals("path", UriValue("", "", "", "", "path").toVCardString())
        assertEquals("?query", UriValue("", "", "", "", "", "query").toVCardString())
        assertEquals("#fragment", UriValue("", "", "", "", "", "", "fragment").toVCardString())

    }

    @Test
    fun toUriString() {

        assertEquals("mailto:arthur.dent@example.com", UriValue("mailto", path = "arthur.dent@example.com").toUriString())

        assertEquals("", UriValue("", "", "", "", "").toUriString())

        assertEquals("https://example.com/", UriValue("https://example.com/").toUriString())
        assertEquals("https://example.com/", UriValue(scheme = "https", host = "example.com", path = "/").toUriString())
        assertEquals("scheme:", UriValue("scheme", "", "", "", "").toUriString())
        assertEquals("", UriValue("", "userinfo", "", "", "").toUriString())
        assertEquals("//userinfo@host", UriValue("", "userinfo", "host", "", "").toUriString())
        assertEquals("//host", UriValue("", "", "host", "", "").toUriString())
        assertEquals("//host:port", UriValue("", "", "host", "port", "").toUriString())
        assertEquals("", UriValue("", "", "", "port", "").toUriString())
        assertEquals("path", UriValue("", "", "", "", "path").toUriString())
        assertEquals("?query", UriValue("", "", "", "", "", "query").toUriString())
        assertEquals("#fragment", UriValue("", "", "", "", "", "", "fragment").toUriString())

    }

    @Test
    fun testEquals() {

        val uriValue1a = UriValue("mailto", path = "arthur.dent@example.com")
        val uriValue1b = UriValue("mailto", path = "arthur.dent@example.com")
        val uriValue2 = UriValue("mailto", path = "adent@example.com")

        assertFalse(uriValue1a.equals("same test string"))
        assertFalse(uriValue1a.equals(null))

        assertEquals(uriValue1a, uriValue1a)
        assertEquals(uriValue1a, uriValue1b)

        assertNotEquals(uriValue1a, uriValue2)
        assertNotEquals(uriValue2, uriValue1a)

    }

    @Test
    fun testHashCode() {

        val uriValue1a = UriValue("mailto", path = "arthur.dent@example.com")
        val uriValue1b = UriValue("mailto", path = "arthur.dent@example.com")
        val uriValue2 = UriValue("mailto", path = "adent@example.com")

        assertEquals(uriValue1a.hashCode(), uriValue1a.hashCode())
        assertEquals(uriValue1a.hashCode(), uriValue1b.hashCode())

        assertNotEquals(uriValue1a.hashCode(), uriValue2.hashCode())
        assertNotEquals(uriValue2.hashCode(), uriValue1a.hashCode())

    }

    @Test
    fun parseUri() {

        assertEquals(
            "https://user:password@example.com/path?query=string#fragmet",
            UriValue.parseUri("https://user:password@example.com/path?query=string#fragmet").toVCardString())

        assertEquals("scheme:path", UriValue.parseUri("scheme:path").toVCardString())
        assertEquals("scheme:path?query", UriValue.parseUri("scheme:path?query").toVCardString())
        assertEquals("scheme:path?query#fragmet", UriValue.parseUri("scheme:path?query#fragmet").toVCardString())

        assertEquals("scheme://host", UriValue.parseUri("scheme://host").toVCardString())
        assertEquals("scheme://host/path", UriValue.parseUri("scheme://host/path").toVCardString())
        assertEquals("scheme://host:1234", UriValue.parseUri("scheme://host:1234").toVCardString())
        assertEquals("scheme://host:1234/path", UriValue.parseUri("scheme://host:1234/path").toVCardString())

        assertEquals("scheme://userinfo@host", UriValue.parseUri("scheme://userinfo@host").toVCardString())
        assertEquals("scheme://userinfo@host/path", UriValue.parseUri("scheme://userinfo@host/path").toVCardString())
        assertEquals("scheme://userinfo@host:1234", UriValue.parseUri("scheme://userinfo@host:1234").toVCardString())
        assertEquals("scheme://userinfo@host:1234/path", UriValue.parseUri("scheme://userinfo@host:1234/path").toVCardString())

        assertEquals("scheme://host?query", UriValue.parseUri("scheme://host?query").toVCardString())
        assertEquals("scheme://host/path?query", UriValue.parseUri("scheme://host/path?query").toVCardString())
        assertEquals("scheme://host:1234?query", UriValue.parseUri("scheme://host:1234?query").toVCardString())
        assertEquals("scheme://host:1234/path?query", UriValue.parseUri("scheme://host:1234/path?query").toVCardString())

        assertEquals("scheme://userinfo@host?query", UriValue.parseUri("scheme://userinfo@host?query").toVCardString())
        assertEquals("scheme://userinfo@host/path?query", UriValue.parseUri("scheme://userinfo@host/path?query").toVCardString())
        assertEquals("scheme://userinfo@host:1234?query", UriValue.parseUri("scheme://userinfo@host:1234?query").toVCardString())
        assertEquals("scheme://userinfo@host:1234/path?query", UriValue.parseUri("scheme://userinfo@host:1234/path?query").toVCardString())

        assertEquals("scheme://host#fragment", UriValue.parseUri("scheme://host#fragment").toVCardString())
        assertEquals("scheme://host/path#fragment", UriValue.parseUri("scheme://host/path#fragment").toVCardString())
        assertEquals("scheme://host:1234#fragment", UriValue.parseUri("scheme://host:1234#fragment").toVCardString())
        assertEquals("scheme://host:1234/path#fragment", UriValue.parseUri("scheme://host:1234/path#fragment").toVCardString())

        assertEquals("scheme://userinfo@host#fragment", UriValue.parseUri("scheme://userinfo@host#fragment").toVCardString())
        assertEquals("scheme://userinfo@host/path#fragment", UriValue.parseUri("scheme://userinfo@host/path#fragment").toVCardString())
        assertEquals("scheme://userinfo@host:1234#fragment", UriValue.parseUri("scheme://userinfo@host:1234#fragment").toVCardString())
        assertEquals("scheme://userinfo@host:1234/path#fragment", UriValue.parseUri("scheme://userinfo@host:1234/path#fragment").toVCardString())

        assertEquals("scheme://host?query#fragment", UriValue.parseUri("scheme://host?query#fragment").toVCardString())
        assertEquals("scheme://host/path?query#fragment", UriValue.parseUri("scheme://host/path?query#fragment").toVCardString())
        assertEquals("scheme://host:1234?query#fragment", UriValue.parseUri("scheme://host:1234?query#fragment").toVCardString())
        assertEquals("scheme://host:1234/path?query#fragment", UriValue.parseUri("scheme://host:1234/path?query#fragment").toVCardString())

        assertEquals("scheme://userinfo@host?query#fragment", UriValue.parseUri("scheme://userinfo@host?query#fragment").toVCardString())
        assertEquals("scheme://userinfo@host/path?query#fragment", UriValue.parseUri("scheme://userinfo@host/path?query#fragment").toVCardString())
        assertEquals("scheme://userinfo@host:1234?query#fragment", UriValue.parseUri("scheme://userinfo@host:1234?query#fragment").toVCardString())
        assertEquals("scheme://userinfo@host:1234/path?query#fragment", UriValue.parseUri("scheme://userinfo@host:1234/path?query#fragment").toVCardString())

    }

}