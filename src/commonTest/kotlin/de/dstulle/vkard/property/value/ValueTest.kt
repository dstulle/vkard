/*
 * GNU LESSER GENERAL PUBLIC LICENSE
 * Copyright (C) 2021 - 2024 Daniel Sturm
 * mail@DanielSturm.de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.dstulle.vkard.property.value

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

/**
 * @author Daniel Sturm &lt;daniel.sturm@microwood.games&gt;
 */
internal class ValueTest {

    data class TestValue(private val value: String = ""): Value<String>() {

        override fun isEmpty() = value.isEmpty()
        override fun get() = value
        override fun toVCardString() = value

        companion object {
            fun escape(value: String): String = TestValue().escapeValue(value)
        }
    }

    @Test
    fun escapeValue() {

        assertEquals("", TestValue.escape(""))
        assertEquals("some plain value", TestValue.escape("some plain value"))
        assertEquals("value\\, that has a comma.", TestValue.escape("value, that has a comma."))
        assertEquals("text\\,value\\;structured", TestValue.escape("text,value;structured"))
        assertEquals("new value\\nnew line", TestValue.escape("new value\nnew line"))
        assertEquals("Special chars: !\"§\$%&/()=?", TestValue.escape("Special chars: !\"§\$%&/()=?"))
        assertEquals("\\,\\;\\\\\\n", TestValue.escape(",;\\\n"))

    }

    @Test
    fun isNotEmpty() {

        val emptyValue = TestValue()
        val notEmptyValue = TestValue("not empty")

        assertTrue(emptyValue.isEmpty())
        assertFalse(notEmptyValue.isEmpty())

        assertFalse(emptyValue.isNotEmpty())
        assertTrue(notEmptyValue.isNotEmpty())

    }
}