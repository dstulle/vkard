/*
 * GNU LESSER GENERAL PUBLIC LICENSE
 * Copyright (C) 2021 - 2024 Daniel Sturm
 * mail@DanielSturm.de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.dstulle.vkard.property.value

import org.w3c.dom.url.URL
import kotlin.test.Test
import kotlin.test.assertEquals

internal class UriValueKtTest {

    @Test
    fun toUrl() {

        val uriPropertyA = UriValue(scheme = "https", userinfo = "john.doe", host = "www.example.com", port = "123", path = "/forum/questions/", query = "tag=networking&order=newest", fragment = "top")
        val uriPropertyB = UriValue(scheme = "ldap", host = "[2001:db8::7]", path = "/c=GB", query = "objectClass?one")
        val uriPropertyC = UriValue(scheme = "mailto", path = "John.Doe@example.com")
        val uriPropertyD = UriValue(scheme = "news", path = "comp.infosystems.www.servers.unix")
        val uriPropertyE = UriValue(scheme = "tel", path = "+1-816-555-1212")
        val uriPropertyF = UriValue(scheme = "telnet", host = "192.0.2.16", port = "80", path = "/")
        val uriPropertyG = UriValue(scheme = "urn", path = "oasis:names:specification:docbook:dtd:xml:4.1.2")

        assertEquals(URL("https://john.doe@www.example.com:123/forum/questions/?tag=networking&order=newest#top").href, uriPropertyA.toUrl().href)
        assertEquals(URL("ldap://[2001:db8::7]/c=GB?objectClass?one").href, uriPropertyB.toUrl().href)
        assertEquals(URL("mailto:John.Doe@example.com").href, uriPropertyC.toUrl().href)
        assertEquals(URL("news:comp.infosystems.www.servers.unix").href, uriPropertyD.toUrl().href)
        assertEquals(URL("tel:+1-816-555-1212").href, uriPropertyE.toUrl().href)
        assertEquals(URL("telnet://192.0.2.16:80/").href, uriPropertyF.toUrl().href)
        assertEquals(URL("urn:oasis:names:specification:docbook:dtd:xml:4.1.2").href, uriPropertyG.toUrl().href)

    }

}