/*
 * GNU LESSER GENERAL PUBLIC LICENSE
 * Copyright (C) 2021 - 2024 Daniel Sturm
 * mail@DanielSturm.de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.dstulle.vkard.property.parameter

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import java.util.*

/**
 * @author Daniel Sturm &lt;mail@danielsturm.de&gt;
 */
internal class LanguageKtTest {

    @Test
    fun getLocale() {

        val languageParameterEn = Language("en")
        val languageParameterEnUs = Language("en-US")
        val languageParameterZhCn = Language("zh-CN")
        val languageParameterSrLatnRs = Language("sr-Latn-RS")
        val languageParameterAzArabIr = Language("az-Arab-IR")

        assertEquals(Locale("en"), languageParameterEn.getLocale())
        assertEquals(Locale("en","US"), languageParameterEnUs.getLocale())
        assertEquals(Locale("zh","CN"), languageParameterZhCn.getLocale())

        val localeSrLatnRs = Locale.Builder().setLanguage("sr").setRegion("RS").setScript("Latn").build()
        assertEquals(localeSrLatnRs, languageParameterSrLatnRs.getLocale())

        val localeAzArabIr = Locale.Builder().setLanguage("az").setRegion("IR").setScript("Arab").build()
        assertEquals(localeAzArabIr, languageParameterAzArabIr.getLocale())
    }
}