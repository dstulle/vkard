/*
 * GNU LESSER GENERAL PUBLIC LICENSE
 * Copyright (C) 2021 - 2024 Daniel Sturm
 * mail@DanielSturm.de
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser Public License for more details.
 *
 * You should have received a copy of the GNU Lesser Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package de.dstulle.vkard.property.value

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.net.URI
import java.net.URL

internal class UriValueKtTest {

    @Test
    fun toUri() {

        val uriPropertyA = UriValue(scheme = "https", userinfo = "john.doe", host = "www.example.com", port = "123", path = "/forum/questions/", query = "tag=networking&order=newest", fragment = "top")
        val uriPropertyB = UriValue(scheme = "ldap", host = "[2001:db8::7]", path = "/c=GB", query = "objectClass?one")
        val uriPropertyC = UriValue(scheme = "mailto", path = "John.Doe@example.com")
        val uriPropertyD = UriValue(scheme = "news", path = "comp.infosystems.www.servers.unix")
        val uriPropertyE = UriValue(scheme = "tel", path = "+1-816-555-1212")
        val uriPropertyF = UriValue(scheme = "telnet", host = "192.0.2.16", port = "80", path = "/")
        val uriPropertyG = UriValue(scheme = "urn", path = "oasis:names:specification:docbook:dtd:xml:4.1.2")

        assertEquals(URI("https://john.doe@www.example.com:123/forum/questions/?tag=networking&order=newest#top"), uriPropertyA.toUri())
        assertEquals(URI("ldap://[2001:db8::7]/c=GB?objectClass?one"), uriPropertyB.toUri())
        assertEquals(URI("mailto:John.Doe@example.com"), uriPropertyC.toUri())
        assertEquals(URI("news:comp.infosystems.www.servers.unix"), uriPropertyD.toUri())
        assertEquals(URI("tel:+1-816-555-1212"), uriPropertyE.toUri())
        assertEquals(URI("telnet://192.0.2.16:80/"), uriPropertyF.toUri())
        assertEquals(URI("urn:oasis:names:specification:docbook:dtd:xml:4.1.2"), uriPropertyG.toUri())

    }

    @Test
    fun toUrl() {

        val uriPropertyA = UriValue(scheme = "https", userinfo = "john.doe", host = "www.example.com", port = "123", path = "/forum/questions/", query = "tag=networking&order=newest", fragment = "top")
        val uriPropertyB = UriValue(scheme = "mailto", path = "John.Doe@example.com")

        assertEquals(URL("https://john.doe@www.example.com:123/forum/questions/?tag=networking&order=newest#top"), uriPropertyA.toUrl())
        assertEquals(URL("mailto:John.Doe@example.com"), uriPropertyB.toUrl())

    }
}